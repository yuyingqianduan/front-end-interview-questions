# JS高级红宝石书重学js

### 闭包

关键的两个点：1.是一个函数   2.可以访问到另外一个函数作用域中的变量

作用：1.隐藏变量，避免全局污染   2.可以读取函数内部的变量

闭包实现的思路：当前作用域 --> 局部作用域 --> 全局作用域 （一层一层往上找，直到undefined or null结束继而又有了原型链的概念）

```javascript
<script type="text/javascript">
	// 访问到当前函数外的变量
	function init(){
		var str = 'abc'
		function test(){
			console.log(str) // abc 
		}
		return test
	}
	var getStr = init()
	getStr() // 已经执行完了
	getStr() // 再次执行 一样打印 abc
	
	// 闭包可以更新外部函数的值
	function changStr(){
		var str = 'abc'
		function updateStr(newStr){
			console.log(str) // abc 
			str = newStr
			console.log(str) // def
		}
		return updateStr
	}
	var updateStr = changStr()
	updateStr('def')
	
	// 从作用域链理解闭包
	var scope = '全局的变量'
	function inScope(){
		var scope = '局部的变量'
		function fn(){
			return scope
		}
		return fn
	}
	let fn = inScope()
	console.log(fn()) // 局部的变量

	// 闭包循环demo
	let data = []
	for(var i=0; i<3; i++){
		data[i] = function(){
			console.log(i) // 3 3 3 
		}
	}
	
	for(var i=0; i<3; i++){
		(function(counter){
			setTimeout(()=>{
				console.log(counter) // 0 1 2 
			},1000)
		})(i)
	}
</script>

```

闭包的问题：js规定在一个函数作用域内程序执行完变量就会被销毁，但是使用闭包时按照作用域的特点，闭包（函数）外面的变量不会被销毁，因为函数会被一直调用一直存在，过度使用闭包会使内存溢出（泄漏）。。。一般使用条件判断把不用的变量置 null

### 作用域链

当在函数作用域中操作一个变量的时候，会先在自身作用域中查找，如果有就直接使用，如果没有就向上级作用域中寻找。如果全局作用域中也没有，那么就报错。

根据内部函数可以访问可以访问外部函数变量的这种机制，用链式查找决定哪些数据能被内部函数访问，就称为**函数作用域链**

#### **全局变量**

全局变量是整个代码中都可以调用的变量

在任何一个地方都可以使用，全局变量只有在浏览器关闭的时候才会销毁，比较占用内存资源

#### **局部变量**

函数内部 定义的变量称为局部变量

只能在函数内部使用，当其所在代码块被执行时，会被初始化；当代码块执行完毕就会销毁，因此更节省节约内存空间；

#### **全局变量** or 局部变量小结

- 全局变量：函数体外声明的变量，生命周期与浏览器页面使用时间相同
- 局部变量：函数体内声明的变量，生命周期与函数执行时间相同
- 特殊情况：变量声明时没有使用var，变量无论在哪声明都是全局变量

**JS作用域**：就是代码名字（变量）作用的范围

**作用域的目的：**是为了提高程序的可靠性，更重要的是减少命名冲突

作用域链:内部函数访问外部函数的变量，采取的是链式查找的方式来决定取哪个值，这种结构我们称为作用域链

#### 全局作用域

全局作用域是指变量可以在当前脚本的任意位置访问，拥有全局作用域的变量也被称为“全局变量”，一般情况下拥有以下特征的变量具有全局作用域：

- 最外层的函数和在最外层函数外面定义的变量拥有全局作用域；
- 所有未定义直接赋值的变量拥有全局作用域；
- 所有 window 对象的属性拥有全局作用域

直接编写在 script 标签之中的JS代码，都是全局作用域；

或者是一个单独的 JS 文件中的。

全局作用域在页面打开时创建，页面关闭时销毁；

在全局作用域中有一个全局对象 window（代表的是一个浏览器的窗口，由浏览器创建），可以直接使用

1. 在全局作用域中，所有创建的变量都会作为 window 对象的属性保存
2. 所有创建的函数都会作为 window 对象的方法保存

#### 局部作用域

在函数内部声明的变量具有局部作用域，拥有局部作用域的变量也被称为“局部变量”，局部变量只能在其作用域中（函数内部）使用

在函数内定义的局部变量只有在函数被调用时才会生成，当函数执行完毕后会被立即销毁

- 在函数内部就是局部作用域，这个代码的名字只在函数的内部起作用
- 调用函数时创建函数作用域，函数执行完毕之后，函数作用域销毁；
- 每调用一次函数就会创建一个新的函数作用域，它们之间是相互独立的

#### 块级作用域（let、const声明）

块级作用域是ES6新推出的概念，用let和const关键字声明变量。**所有{}都会形成独立的块级作用域**，例如if、for，注意对象的{}不包括在内

#### 特殊场景

```javascript
// 函数参数——属于函数（局部）作用域
function setName(age){
    console.log(age) // 20
}
setName(20)  
console.log(age) // 报错
```

```javascript
// for 循环 使用var—全局作用域，外面也可以访问
for (var i=0; i<5; i++){
	console.log(i,'内') // 0 1 2 3 4 内
}
console.log(i,'外') // 5 外
```

```javascript
// 使用let—块级作用域，只能在{}范围内被访问，外面访问不到
for (let i=0; i<5; i++){
	console.log(i,'内') // 0 1 2 3 4 内
}
console.log(i,'外') // 报错 外
```

```javascript
// try...catch中的err—不属于任何作用域，只能被catch访问
try{
	throw new Error()
}catch(err){
	var str = 'abc'
	const str1 = 'edf'
	console.log(err) // 正常打印 Error
}
console.log(str) // 正常打印 abc
console.log(str1) // 报错
console.log(err) // 报错
```

### 执行上下文

执行上下文是当前JavaScript代码被解析和执行时所在环境的抽象概念

#### 全局执行上下文

只有一个浏览器中的全局对象是window对象，this指向这个对象

#### 函数执行上下文

存在多个，函数被调用的时候才会创建，每次调用函数都会创建一个新的上下文

#### Eval函数执行上下文

eval函数中执行的代码（好像已经没什么人用了不用过多关注吧）

#### this指向

```html
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>上下文</title>
	</head>
	<body>
		<button id="btn1">事件按钮1</button>
		<button id="btn2">事件按钮2</button>
		<!-- BUTTON -->
		<button onclick="console.log(this.tagName)">事件按钮3</button>
		<!-- Window 对象 -->
		<button onclick="console.log(function(){return this}())">事件按钮4</button>
	<script type="text/javascript">
		// 全局上下文
		// 浏览器中
		console.log(this === window) // true
		num = 10
		console.log(window.num) // 10
		this.aa = 'abc'
		console.log(window.aa) // abc
		console.log(aa) // abc
		
        // 函数上下文
		function fn(){
			console.log(this) // Window
		}
		fn()
		
        // 函数体内使用严格模式是 undefined
		function fn1(){
			"use strict";
			console.log(this) // undefined
		}
		fn1()
		
		// 类上下文
		class Person {
			constructor(arg) {
				console.log(this) // Person {}
				let proto = Object.getPrototypeOf(this) // 指向当前类 Person
				console.log(Object.getOwnPropertyNames(proto)) // ['constructor', 'setAge', 'setName']
				
			}
			setAge(){}
			setName(){}
		}
		let eg = new Person()
		
        // 作为对象的方法
		var age = 10
		let ob = {
			age: 20,
			fn: function(){
				console.log(this.age)
				return this.age
			}
		}
		ob.fn() // 20
		let getName = ob.fn
		console.log(getName()) // Window.age 10
		
        // dom 事件处理函数
	document.getElementById('btn1').addEventListener('click',function(e){
			console.log(this) // 指向的绑定的元素
		})
		// 箭头函数写法
		document.getElementById('btn2').addEventListener('click',(e) => {
			console.log(this) // 指向的 Window 箭头函数是继承上层 this
		})
		
		// 箭头函数下的 this
		var f = () => this
		console.log(f() === window) // true
		// Eval函数执行上下文 现在的代码基本上无相关也不建议使用也不解释了
	</script>
	</body>
</html>
```



### 对象

对象是一个抽象的概念，js里万事万物都可称为对象，对象是一个实体里面封装了属性和方法

OOP（面向对象编程）是一种编程开发思想

面向对象的特性

- 封装性
- 继承性
- 多态性（同一个方法运行产生不同的结果）

#### 创建对象的方法

```javascript
<script type="text/javascript">
// 第一种 new Object
var person = new Object()
person.name = 'bob'
person.age = 20
person.setName = function (){
	console.log(this.name)
}
console.log(person) // {name: 'bob', age: 20, setName: ƒ}

// 第二种 对象字面量
var person1 = {
	name: 'lala',
	age: 22,
	setName:function(){
		console.log(this.name)
	}
}
console.log(person1) // {name: 'lala', age: 22, setName: ƒ}

// 第三种 工厂函数
function createPerson(name,age){
	return {
		name: name,
		age: age,
		setName:function(){
			console.log(this.name)
		}
	}
}
let person2 = createPerson('wow',18) 
console.log(person2) // {name: 'wow', age: 18, setName: ƒ}
// 第四种 构造函数
// 构造函数和普通函数的区别在于使用new生成实例的就是构造函数直接调用的就是普通函数
function Person(name,age){
	this.name = name;
	this.age = age;
	this.setName = function(){
		console.log(this.name)
	}
}
let person3 = new Person('kol', 28)
person3.setName() // kol
console.log(person3) // Person {name: 'kol', age: 28, setName: ƒ}

let person4 = new Person('sal', 29)
person4.setName() // sal
console.log(person4) // Person {name: 'sal', age: 29, setName: ƒ}
// 构造函数和实例之间的关系 [每一个对象实例都有一个constructor属性指向创建该实例的构造函数]
console.log(person3.constructor === Person) // true
console.log(person4.constructor === Person) // true
console.log(person3.constructor === person4.constructor) // true

// constructor prototype 以及实例之间的关系
function Animal(color,shap){
	this.color = color;
	this.shap = shap;
}
// 原型
Animal.prototype.setName = function(){
	console.log('setName')
}
// 实例
let Animal1 = new Animal('blue','round')
console.log(Animal1.__proto__ === Animal.prototype) // true
console.log(Object.getPrototypeOf(Animal1) === Animal.prototype) // true
console.log(Animal.prototype.constructor === Animal) // true
let Animal2 = new Animal('yellow','lang')
// 原型链的查找过程
function Foo(){}
Foo.prototype.str = 'abc'
let fn = new Foo()
console.log(fn) // Foo {}
console.log(fn.str) // abc
console.log(fn.toString()) // [object Object]
console.log(fn.aa) // undefined

// 第五种 Object.create() 方法
let user = {
	name: 'bob',
	age: 22,
	setName: function(){
		return `姓名：${this.name},年龄：${this.age}`
	}
}
let newUser = Object.create(user)
newUser.name = 'lala'
newUser.age = 26
console.log(newUser) // {name: 'lala', age: 26}
console.log(newUser.setName()) // 姓名：lala,年龄：26

// 第六种 ES6类
class Tool {
	constructor(shap,amount) {
		this.shap = shap
		this.amount = amount
		this.setAmount = function(){
			return `形状是：${this.shap}，多少钱${this.amount}`
		}
	}	
}
let tool = new Tool('round',100)
console.log(tool) // Tool {shap: 'round', amount: 100, setAmount: ƒ}

// 如果只是拷贝 自身可枚举属性，就可以只用 Object.assign 方法；
// 如果是要拷贝原型上的属性，就需要 Object.assign , Object.create, Object.getPrototypeOf 方法结合使用
// 如果是拷贝get /set 属性，就需要 结合 Ojbect.getOwnPropertyDescriptors 方法
</script>

```

#### 原型

概念：每一个构造函数都有一个`prototype`属性，指向另一个对象。这个对象的所有属性和方法，都会被构造函数的实例继承

JS中的对象包含了一个prototype的内部属性，这个属性所对应的就是该对象的原型

所有引用类型（函数，数组，对象）都拥有__proto__属性（隐式原型)所有函数除了有_proto_属性之外还拥有prototype属性（显式原型）

原型对象：每创建一个函数，该函数会自动带有一个prototype属性，该属性是一个指针，指向了一个对象，我们称之为原型对象

函数除了有_proto_属性之外还拥有prototype属性

原型和实例的关系：每个构造函数都有一个原型对象，原型对象都包含一个指向构造函数的指针，而实例都包含一个指向原型对象的内部指针

#### 原型链

概念：

```javascript
在JavaScript 中，每个对象通过__proto__属性指向它的原型对象，这个原型对象又有自己的原型，直到某个对象的原型为 null 为止，这种一级一级的链结构就称为原型链
```

MDN专业术语解释

每个实例对象（object）都有一个私有属性（称之为 __proto__ ）指向它的构造函数的原型对象（**prototype**）。该原型对象也有一个自己的原型对象（__proto__），层层向上直到一个对象的原型对象为 `null`。根据定义，`null` 没有原型，并作为这个**原型链**中的最后一个环节。

所以 Object.prototype.__proto__ 的值为 null 跟 Object.prototype 没有原型，其实表达了一个意思。

所以查找属性的时候查到 Object.prototype 就可以停止查找了

总结就是：

1.每个函数都有一个 prototype 属性指向它的原型；
2.每一个JavaScript对象(除了 null )都具有的一个属性，叫proto，这个属性会指向该对象的原型；
3.每个原型都有一个 constructor 属性指向关联的构造函数 实例原型指向构造函数；
4.实例原型本身也是一个JS对象，是对象就会有 proto属性，它的指向是 Object.prototype,Object.prototype指向null；

那通过构造函数创建的对象 => 函数原型 => Object.prototype => null 的指向（查找过程），就是所谓的原型链；

### 浏览器的渲染过程

![img](https://img2022.cnblogs.com/blog/1186521/202204/1186521-20220406201717815-526252133.png)

1. 解析HTML，生成DOM树，解析CSS，生成CSSOM树
2. 将DOM树和CSSOM树结合，生成渲染树(Render Tree)
3. Layout(回流):根据生成的渲染树，进行回流(Layout)，得到节点的几何信息（位置，大小）
4. Painting(重绘):根据渲染树以及回流得到的几何信息，得到节点的绝对像素
5. Display:将像素发送给GPU，展示在页面上 （这一步其实还有很多内容，比较复杂）

<u>渲染树只包含可见的节点</u>

### 回流（也叫重排）

将可见DOM节点以及它对应的样式结合起来，可是我们还需要计算它们在设备视口(viewport)内的确切位置和大小，这个计算的阶段就是回流。

### 重绘

通过构造渲染树和回流阶段，我们知道了哪些节点是可见的，以及可见节点的样式和具体的几何信息(位置、大小)，那么我们就可以将渲染树的每个节点都转换为屏幕上的实际像素，这个阶段就叫做重绘节点

### 何时发生回流（重排）重绘

当页面布局和几何信息发生变化的时候，就需要回流。

<u>回流常见情景</u>

- 添加或删除可见的DOM元素
- 元素的位置发生变化
- 元素的尺寸发生变化（包括外边距、内边框、边框大小、高度和宽度等）
- 内容发生变化，比如文本变化或图片被另一个不同尺寸的图片所替代。
- 页面一开始渲染的时候（这肯定避免不了）
- 浏览器的窗口尺寸变化（因为回流是根据视口的大小来计算元素的位置和大小的）

<u>重绘常见情景</u>

某个div标签节点的背景颜色、

字体颜色等等发生改变，但是该div标签节点的宽、高、内外边距并不发生变化，此时触发浏览器重绘（repaint）

### 重绘回流（重排）关系

重绘(repaint) 不会引起Dom树变化及页面布局变化，只重新渲染页面样式回流与重绘两者之间的联系在于: 触发回流一定会触发重绘, 而触发重绘却不一定会触发回流

### 浏览器的优化机制

现代浏览器大多都是通过队列机制来批量更新布局，浏览器会把修改操作放在队列中，至少一个浏览器刷新（即16.67ms）才会清空队列，但当你**获取布局信息的时候，队列中可能有会影响这些属性或方法返回值的操作，即使没有，浏览器也会强制清空队列，触发回流与重绘来确保返回正确的值**

现代的浏览器都是很聪明的，由于每次重排都会造成额外的计算消耗，因此大多数浏览器都会通过队列化修改并批量执行来优化**重排过程**。浏览器会将修改操作放入到队列里，直到过了一段时间或者操作达到了一个阈值，才清空队列。但是！**当你获取布局信息的操作的时候，会强制队列刷新**，比如当你访问以下属性或者使用以下方法：

- offsetTop、offsetLeft、offsetWidth、offsetHeight
- scrollTop、scrollLeft、scrollWidth、scrollHeight
- clientTop、clientLeft、clientWidth、clientHeight
- getComputedStyle()
- getBoundingClientRect

### 如何减少回流

- 使用 transform 替代 top
- 不要把节点的属性值放在一个循环里当成循环里的变量。
- 避免使用 table 布局，可能table 中每个元素的大小以及内容的改动会造成整个 table 的重新布局。
- 对于那些复杂的动画，对其设置 position: fixed/absolute，尽可能地使元素脱离文档流，从而减少对其他元素的影响
- 把 DOM 离线后修改。如：使用 documentFragment 对象在内存里操作 DOM。我们还可以通过通过设置元素属性display: none，将其从页面上去掉，然后再进行后续操作，这些后续操作也不会触发回流与重绘，这个过程称为离线操作。
- 如果想设定元素的样式，通过改变元素的 class 类名。不要一条一条地修改 DOM 的样式。与其这样，还不如预先定义好 css 的 class，然后修改 DOM 的 className
- css3硬件加速（GPU加速）使用css3硬件加速，可以让transform、opacity、filters这些动画不会引起回流重绘 。但是对于动画的其它属性，比如background-color这些，还是会引起回流重绘的，不过它还是可以提升这些动画的性能。css3硬件加速的坑，如果你为太多元素使用css3硬件加速，会导致内存占用较大，会有性能问题，在GPU渲染字体会导致抗锯齿无效。这是因为GPU和CPU的算法不同。因此如果你不在动画结束的时候关闭硬件加速，会产生字体模糊。

### 防抖（debounce）

自我理解：一个函数**连续多次**触发，我们只执行最后**一次**

```javascript
// fn是要节流的函数，wait是时间间隔，默认500毫秒
const Debounce = (fn, wait = 500) => {
  // 缓存一个定时器
  let timer = null
  // 使用闭包（这样节流函数复用时，不会相互影响，且不污染全局变量）
  return function( ...args ) { // ES6剩余参数收集参数
    // 如果在500毫秒内再次触发，即timer存在，此时清除掉这个timer
    // 这里实现了执行只最后一次
    if (timer) clearTimeout(timer)
    timer = setTimeout(() => {
      fn.apply(this, args) // 利用apply绑定this，同时展开args数组并传参
    }, wait)
    // 上面一句 也可以替换成这样的写法
    // timer = setTimeout(fn.bind(this), wait, ...args)
  } 
}
```

### 节流（Throttle）

自我理解：一个函数**连续多次**触发，我们按照一定的时间**间隔多次**执行

```javascript
// fn是要节流的函数，wait是时间间隔，默认500毫秒
const Throttle = (fn, wait = 500) => {
  // 缓存一个定时器
  let timer
  // 使用闭包（这样节流函数复用时，不会相互影响）
  return function( ...args ) {
    // 如果在500毫秒内再次触发，即timer存在，此时return，等待这个timer执行完毕。
    // 这里实现了时间间隔
    if (timer) return
    // 这里表示第一次触发，或是上一个timer执行完毕。就可以重新开启一个定时器。
    timer = setTimeout(() => {
      fn.apply(this, args)
      timer = null
    }, wait)
  } 
}
```

实际项目中 节流（`Throttle`）和防抖（`Debounce`）两个函数都内置到 [Lodash](https://lodash.com/) 脚本库中，无需自己实现 Lodash 里面有很多js方便实用处理的方法。

### 防抖和节流的应用场景

**<u>防抖</u>**

- 登录、发短信等按钮避免用户点击太快，以致于发送了多次请求，需要防抖
- 调整浏览器窗口大小时，resize 次数过于频繁，造成计算过多
- 文本编辑器实时保存，当无任何更改操作一秒后进行保存
- DOM 元素的拖拽功能实现
- 计算鼠标移动的距离
- Canvas 模拟画板功能
- 搜索联想

**<u>节流</u>**

- scroll 事件，每隔一秒计算一次位置信息等
- 浏览器播放事件，每个一秒计算一次进度信息等
- input框实时搜索并发送请求展示下拉列表，每隔一秒发送一次请求 (也可做防抖)
- 滚动加载；mousemove ,mousehover

**<u>总结</u>**

防抖和节流都是为了减低浏览器触发事件的频率，提高利用率的方式

防抖和节流都是应用在高频事件触发场景中

无论是防抖还是节流最终的目的都是`避免回调函数中的处理每次都执行`

节流和防抖都是浏览器处理事件的一种机制

