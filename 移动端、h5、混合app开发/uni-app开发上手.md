# uni-app开发上手

### 下载（APP版本即可）

官网下载地址：https://www.dcloud.io/hbuilderx.html

### 配置预览

运行到浏览器

运行到开发者工具以微信小程序的为例【配置开发者工具路径】

运行到终端

真机运行：连接手机，开启USB调试，进入hello-uniapp项目，点击工具栏的运行 -> 真机运行 -> 选择运行的设备，即可在该设备里面体验uni-app

![image-20211016140653866](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20211016140653866.png)

### 生命周期

生命周期介绍：https://uniapp.dcloud.io/collocation/frame/lifecycle?id=%e9%a1%b5%e9%9d%a2%e7%94%9f%e5%91%bd%e5%91%a8%e6%9c%9f

1. **应用生命周期**（应用生命周期仅可在`App.vue`中监听，在其它页面监听无效）
2. **页面生命周期**（pages.json  pages字段配置过的组件）
3. **组件生命周期**（等同于vue生命周期）

### less scss处理

安装less scss编译的插件 在工具/插件安装

### tabbar配置（pages.json中配置）

```javascript
	"tabBar": {
	    "color": "#7A7E83",
	    "selectedColor": "#3192F2",
	    "borderStyle": "black",
	    "backgroundColor": "#ffffff",
	    "list": [{
	        "pagePath": "pages/index/index",
	        "iconPath": "static/home.png",
	        "selectedIconPath": "static/home_select.png",
	        "text": "首页"
	    }, {
	        "pagePath": "pages/card/card",
	        "iconPath": "static/card_list.png",
	        "selectedIconPath": "static/card_list_select.png",
	        "text": "卡列表"
	    }, {
	        "pagePath": "pages/log/log",
	        "iconPath": "static/log.png",
	        "selectedIconPath": "static/log_select.png",
	        "text": "用户日志"
	    }, {
	        "pagePath": "pages/my/my",
	        "iconPath": "static/my.png",
	        "selectedIconPath": "static/my_select.png",
	        "text": "个人中心"
	    }]
	},
```

### 效果

微信开发者工具（unpackage/dist/dev/mp-weixin）

uni-app默认把项目编译到根目录的unpackage目录

![image-20211016212056339](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20211016212056339.png)

h5谷歌浏览器预览

![image-20211016212120980](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20211016212120980.png)

### 打包发布

点击发布（发布对应的应用如h5 or 微信小程序）即可

注意事项：https://uniapp.dcloud.io/quickstart-hx?id=%e5%8f%91%e5%b8%83uni-app

特别的h5：history 模式发行需要后台配置支持，详见：history 模式的后端配置

### 开发环境和生产环境区分

```javascript
if(process.env.NODE_ENV === 'development'){
    console.log('开发环境')
}else{
    console.log('生产环境')
}
```

### 条件编译

```javascript
// #ifdef H5
    alert("只有h5平台才有alert方法")
// #endif
```

详细链接：https://uniapp.dcloud.io/platform
