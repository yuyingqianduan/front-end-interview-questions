# react深入浅出

### JSX 的本质

JavaScript 的语法扩展

JSX 会被编译为 React.createElement()， React.createElement() 将返回一个叫作“React Element”的 JS 对象

“编译”这个动作，是由 **Babel** 来完成的，Babel 也具备将 JSX 语法转换为 JavaScript 代码的能力。

**JSX 的本质是React.createElement这个 JavaScript 调用的语法糖**

```javascript
// Babel 输入： ES6 箭头函数
[1, 2, 3].map((n) => n + 1);

// Babel 输出： ES5 语法实现的同等功能
[1, 2, 3].map(function(n) {
  return n + 1;
});
```

### 为什么用jsx

用 JavaScript 写起来太长了，结构看起来又不清晰，用 HTML 的方式写起来就方便很多了

JSX 代码层次分明、嵌套关系清晰；而 React.createElement 代码则给人一种非常混乱的“杂糅感”

### **React.createElement()**

```javascript
createElement(type, config, children)

// type：用于标识节点的类型。它可以是类似“h1”“div”这样的标准 HTML 标签字符串，也可以是 React 组件类型或 React fragment 类型。

// config：以对象形式传入，组件所有的属性都会以键值对的形式存储在 config 对象中。

// children：以对象形式传入，它记录的是组件标签之间嵌套的内容，也就是所谓的“子节点”“子元素”。
```

```javascript
/**
 React的创建元素方法
 */
export function createElement(type, config, children) {
  // propName 变量用于储存后面需要用到的元素属性
  let propName; 
  // props 变量用于储存元素属性的键值对集合
  const props = {}; 
  // key、ref、self、source 均为 React 元素的属性，此处不必深究
  let key = null;
  let ref = null; 
  let self = null; 
  let source = null; 
  // config 对象中存储的是元素的属性
  if (config != null) { 
    // 进来之后做的第一件事，是依次对 ref、key、self 和 source 属性赋值
    if (hasValidRef(config)) {
      ref = config.ref;
    }
    // 此处将 key 值字符串化
    if (hasValidKey(config)) {
      key = '' + config.key; 
    }
    self = config.__self === undefined ? null : config.__self;
    source = config.__source === undefined ? null : config.__source;
    // 接着就是要把 config 里面的属性都一个一个挪到 props 这个之前声明好的对象里面
    for (propName in config) {
      if (
        // 筛选出可以提进 props 对象里的属性
        hasOwnProperty.call(config, propName) &&
        !RESERVED_PROPS.hasOwnProperty(propName) 
      ) {
        props[propName] = config[propName]; 
      }
    }
  }
  // childrenLength 指的是当前元素的子元素的个数，减去的 2 是 type 和 config 两个参数占用的长度
  const childrenLength = arguments.length - 2; 
  // 如果抛去type和config，就只剩下一个参数，一般意味着文本节点出现了
  if (childrenLength === 1) { 
    // 直接把这个参数的值赋给props.children
    props.children = children; 
    // 处理嵌套多个子元素的情况
  } else if (childrenLength > 1) { 
    // 声明一个子元素数组
    const childArray = Array(childrenLength); 
    // 把子元素推进数组里
    for (let i = 0; i < childrenLength; i++) { 
      childArray[i] = arguments[i + 2];
    }
    // 最后把这个数组赋值给props.children
    props.children = childArray; 
  } 
  // 处理 defaultProps
  if (type && type.defaultProps) {
    const defaultProps = type.defaultProps;
    for (propName in defaultProps) { 
      if (props[propName] === undefined) {
        props[propName] = defaultProps[propName];
      }
    }
  }
  // 最后返回一个调用ReactElement执行方法，并传入刚才处理过的参数
  return ReactElement(
    type,
    key,
    ref,
    self,
    source,
    ReactCurrentOwner.current,
    props,
  );
}
```

```javascript
// jsx创建元素
const element = (
  <h1 className="react">
    Hello, react!
  </h1>
);

// React.createElement 创建元素
const element = React.createElement(
  'h1',
  {className: 'react'},
  'Hello, react!'
);
```

### **ReactDOM.render() 方法**

```javascript
ReactDOM.render(
    // 需要渲染的元素（ReactElement）
    element, 
    // 元素挂载的目标容器（一个真实DOM）
    container,
    // 回调函数，可选参数，可以用来处理渲染结束后的逻辑
    [callback]
)
```

### 虚拟 DOM: 核心算法的基石

组件在初始化时，会通过调用生命周期中的 render 方法，**生成虚拟 DOM**，然后再通过调用 ReactDOM.render 方法，实现虚拟 DOM 到真实 DOM 的转换。

当组件更新时，会再次通过调用 render 方法**生成新的虚拟 DOM**，然后借助 diff 对比定位出两次虚拟 DOM 的差异，从而针对发生变化的真实 DOM 作定向更新

### react生命周期钩子

#### react15生命周期

|        constructor()        |
| :-------------------------: |
| componentWillReceiveProps() |
|   shouldComponentUpdate()   |
|    componentWillMount()     |
|    componentWillUpdate()    |
|    componentDidUpdate()     |
|     componentDidMount()     |
|          render()           |
|   componentWillUnmount()    |

```javascript
// 初始化渲染时调用
  componentWillMount() {
    console.log("componentWillMount方法执行");
  }
  // 初始化渲染时调用
  componentDidMount() {
    console.log("componentDidMount方法执行");
  }
  // 父组件修改组件的 props 时会调用
  componentWillReceiveProps(nextProps) {
    console.log("componentWillReceiveProps方法执行");
  }
  // 组件更新时调用
  shouldComponentUpdate(nextProps, nextState) {
    console.log("shouldComponentUpdate方法执行");
    return true;
  }
  // 组件更新时调用
  componentWillUpdate(nextProps, nextState) {
    console.log("componentWillUpdate方法执行");
  }
  // 组件更新后调用
  componentDidUpdate(preProps, preState) {
    console.log("componentDidUpdate方法执行");
  }
  // 组件卸载时调用
  componentWillUnmount() {
    console.log("子组件的componentWillUnmount方法执行");
  }
```

#### Mounting 阶段：组件的初始化渲染（挂载）

初始化渲染 -> constructor() -> componentWillMount() -> render() -> componentDidMount()

constructor() 该方法仅仅在挂载的时候被调用一次，对 this.state 进行初始化，componentWillMount、componentDidMount 方法同样只会在挂载阶段被调用一次。其中 componentWillMount 会在执行 render 方法前被触发，真实 DOM 的渲染工作，在挂载阶段是由 ReactDOM.render 来承接的

componentDidMount 方法在渲染结束后被触发，此时因为真实 DOM 已经挂载到了页面上，我们可以在这个生命周期里执行真实 DOM 相关的操作（比如异步请求、数据初始化）

#### Updating 阶段：组件的更新

组件的更新分为两种：一种是由父组件更新触发的更新；另一种是组件自身调用自己的 setState 触发的更新

父组件触发 -> componentWillReceiveProps() -> shouldComponentUpdate() -> componentWillUpdate() -> render() -> componentDidUpdate()

组件自身触发 -> shouldComponentUpdate() -> componentWillUpdate() -> render() -> componentDidUpdate()

<u>componentReceiveProps 并不是由 props 的变化触发的，而是由父组件的更新触发的</u>

#### Unmounting 阶段：组件的卸载

组件卸载 -> componentWillUnmount()

组件销毁常见的两种情况

1. 组件在父组件中被移除了
2. 组件中设置了 key 属性，父组件在 render 的过程中，发现 key 值和上一次不一致

#### react16生命周期

```javascript
// 初始化/更新时调用
  static getDerivedStateFromProps(props, state) {
    console.log("getDerivedStateFromProps方法执行");
    return {
      fatherText: props.text
    }
  }
  // 初始化渲染时调用
  componentDidMount() {
    console.log("componentDidMount方法执行");
  }
  // 组件更新时调用
  shouldComponentUpdate(prevProps, nextState) {
    console.log("shouldComponentUpdate方法执行");
    return true;
  }
  // 组件更新时调用
  getSnapshotBeforeUpdate(prevProps, prevState) {
    console.log("getSnapshotBeforeUpdate方法执行");
    return "haha";
  }
  // 组件更新后调用
  componentDidUpdate(preProps, preState, valueFromSnapshot) {
    console.log("componentDidUpdate方法执行");
    console.log("从 getSnapshotBeforeUpdate 获取到的值是", valueFromSnapshot);
  }
  // 组件卸载时调用
  componentWillUnmount() {
    console.log("子组件的componentWillUnmount方法执行");
  }
```

#### Mounting 阶段：组件的初始化渲染（挂载）

初始化渲染 -> constructor() -> getDerivedStateFromProps() -> render() -> componentDidMount()

<u>消失的 componentWillMount，新增的 getDerivedStateFromProps</u>

**getDerivedStateFromProps 是一个静态方法**。静态方法不依赖组件实例而存在，因此你在这个方法内部是**访问不到 this** 的，其设计的初衷不是试图替换掉 **componentWillMount**，而是试图替换掉 **componentWillReceiveProps**，因此它有且仅有一个用途：**使用 props 来派生/更新 state**

#### Updating 阶段：组件的更新

组件的更新分为两种：一种是由父组件更新触发的更新；另一种是组件自身调用自己的 setState 触发的更新

父组件触发 -> getDerivedStateFromProps() -> shouldComponentUpdate() -> render() -> getSnapshotBeforeUpdate() -> componentDidUpdate()

组件自身触发 -> shouldComponentUpdate() -> render() -> getSnapshotBeforeUpdate() -> componentDidUpdate()

- 在 React 16.4 中，**任何因素触发的组件更新流程**（包括由 this.setState 和 forceUpdate 触发的更新流程）都会触发 getDerivedStateFromProps；
- 而在 v 16.3 版本时，**只有父组件的更新**会触发该生命周期

为什么要用 getDerivedStateFromProps 代替 componentWillReceiveProps？

确保生命周期函数的行为更加可控可预测，从根源上帮开发者避免不合理的编程方式，避免生命周期的滥用；同时，也是在为新的 Fiber 架构铺路（**Fiber 会使原本同步的渲染过程变成异步的**）

#### Unmounting 阶段：组件的卸载

组件卸载 -> componentWillUnmount()

### Fiber 架构简析

同步渲染一旦开始，便会牢牢抓住主线程不放，直到递归彻底完成。在这个过程中，浏览器没有办法处理任何渲染之外的事情，会进入一种无法处理用户交互的状态。因此若渲染时间稍微长一点，页面就会面临卡顿甚至卡死的风险

Fiber 会将一个大的更新任务拆解为许多个小任务。每当执行完一个小任务时，渲染线程都会把主线程交回去，看看有没有优先级更高的工作要处理，确保不会出现其他任务被“饿死”的情况，进而避免同步渲染带来的卡顿。在这个过程中，渲染线程不再“一去不回头”，而是可以被打断的，这就是所谓的“异步渲染”

- render 阶段：纯净且没有副作用，可能会被 React 暂停、终止或重新启动。

- pre-commit 阶段：可以读取 DOM。

- commit 阶段：可以使用 DOM，运行副作用，安排更新。


总的来说，render 阶段在执行过程中允许被打断，而 commit 阶段则总是同步执行的

### 基于 props 的单向数据流（自上而下）

**事件的发布-订阅机制**

通过调用 addEventListener 方法，我们可以创建一个事件监听器，这个动作就是“订阅”

```javascript
el.addEventListener("click", func, false);
```

当 click 事件被触发时，事件会被“发布”出去，进而触发监听这个事件的 func 函数。这就是一个最简单的发布-订阅案例

监听事件的位置和触发事件的位置是不受限的

**发布-订阅模型 API 设计思路**（订阅操作是一个“写”操作，相应的，发布操作就是一个“读”操作）

- on()：负责注册事件的监听器，指定事件触发时的回调函数。

- emit()：负责触发事件，可以通过传参使其在触发的时候携带数据
- off()：负责监听器的删除

```javascript
class myEventEmitter {
  constructor() {
    // eventMap 用来存储事件和监听函数之间的关系
    this.eventMap = {};
  }
  // type 这里就代表事件的名称
  on(type, handler) {
    // hanlder 必须是一个函数，如果不是直接报错
    if (!(handler instanceof Function)) {
      throw new Error("请传一个函数");
    }
    // 判断 type 事件对应的队列是否存在
    if (!this.eventMap[type]) {
      // 若不存在，新建该队列
      this.eventMap[type] = [];
    }
    // 若存在，直接往队列里推入 handler
    this.eventMap[type].push(handler);
  }
  // 别忘了我们前面说过触发时是可以携带数据的，params 就是数据的载体
  emit(type, params) {
    // 假设该事件是有订阅的（对应的事件队列存在）
    if (this.eventMap[type]) {
      // 将事件队列里的 handler 依次执行出队
      this.eventMap[type].forEach((handler, index) => {
        // 注意别忘了读取 params
        handler(params);
      });
    }
  }
  // 负责监听器的删除
  off(type, handler) {
    if (this.eventMap[type]) {
      this.eventMap[type].splice(this.eventMap[type].indexOf(handler) >>> 0, 1);
    }
  }
}
```

用例

```javascript
// 实例化 myEventEmitter
const myEvent = new myEventEmitter();
// 编写一个简单的 handler
const testHandler = function (params) {
  console.log(`test事件被触发了，testHandler 接收到的入参是${params}`);
};
// 监听 test 事件
myEvent.on("test", testHandler);
// 在触发 test 事件的同时，传入希望 testHandler 感知的参数
myEvent.emit("test", "newState");
```

window.myEvent = new myEventEmitter();

#### 父传子 props

#### 子传父 使用 props 回调

```javascript
class Father extends React.Component {
      handleGetMsg = (value) => {
        console.log(value)
        this.setState({
          test: value
        })
      }
      state = {
          
        }
      render() {
        return (
          <div>
            <p>从子组件中接收到得数据: {this.state.test}</p>
            <Child getMsg={this.handleGetMsg}/>
          </div>
        )
      }
    }
```

```javascript
class Child extends React.Component {
      state = {
        val: 'haha'
      }
      handleClick = () => {
        this.props.getMsg(this.state.val)
      }
      render() {
        return (
          <div>
            <button onClick={this.handleClick}>子组件</button>
          </div>
        )
      }
    }
```

#### 跨级组件通信

- 中间组件层层传递 props（不推荐）
- 使用 context 对象

- 非嵌套组件间通信：使用事件订阅（兄弟组件以及不在同一个父级中的非兄弟组件）

```javascript
npm install events --save
```

```javascript
// emitter.js
import { EventEmitter } from "events";
export default new EventEmitter();
```

自己实现组件间的通信还是太难以管理了，因此出现了很多状态管理工具，如 Redux、Mobx、Flux 等状态管理了

#### Redux 的设计思想/工作流

**在 Redux 的整个工作过程中，数据流是严格单向的**

Redux 是为**JavaScript**应用而生的，也就是说它不是 React 的专利，React 可以用，Vue 可以用，原生 JavaScript 也可以用

Redux 是一个**状态容器**

Redux 主要由三部分组成：store、reducer 和 action

- store  它是一个**单一的数据源**，而且是只读的；

- action 人如其名，是“动作”的意思，它是**对变化的描述**  （**要想让 state 发生改变，就必须用正确的 action 来驱动这个改变**。）

- ```javascript
  const action = {
    type: "ADD", // 只有 type 是必传的
    payload: '123'
  }
  // 派发 action，靠的是 dispatch.
  // 使用 dispatch 派发 action，action 会进入到 reducer 里触发对应的更新
   store.dispatch(action)
  ```

- reducer 是一个纯函数，它负责**对变化进行分发和处理，** 最终将新的数据返回给 store。(**将新的 state 返回给 store**)

对数据进行修改只有一种途径：派发 action。action 会被 reducer 读取，进而根据 action 内容的不同对数据进行修改、生成新的 state（状态），这个新的 state 会更新到 store 对象里，进而驱动视图层面做出对应的改变

**Redux 通过提供一个统一的状态容器，使得数据能够自由而有序地在任意组件之间穿梭**，这就是 Redux 实现组件间通信的思路

```javascript
// 引入 redux
import { createStore } from 'redux'

// 创建 store
const store = createStore(
    reducer, // reducer 是你不得不传的
    initial_state, // 初始state
    applyMiddleware(middleware1, middleware2, ...) // 中间件 如 redux-sage redux-thunk

);
```

### react hooks （React 16.8后）

#### 类组件（Class Component）

基于 ES6 Class 这种写法，通过继承 React.Component 得来的 React 组件

#### 函数组件/无状态组件（Function Component/Stateless Component）

以函数的形态存在的 React 组件。早期（16.8之前）并没有 React-Hooks 的加持，函数组件内部无法定义和维护 state，因此它还有一个别名叫“无状态组件”

#### 类组件与函数组件对比

类组件需要继承 class，函数组件不需要；

类组件可以访问生命周期方法，函数组件不能；

类组件中可以获取到实例化后的 this，并基于这个 this 做各种各样的事情，而函数组件不可以；

类组件中可以定义并维护 state（状态），而函数组件不可以；

**同样逻辑的函数组件相比类组件而言，复杂度要低得多得多**

**函数组件会捕获 render 内部的状态，这是两类组件最大的不同**

....

**封装：**将一类属性和方法，“聚拢”到一个 Class 里去。

**继承：**新的 Class 可以通过继承现有 Class，实现对某一类属性和方法的复用。

**Hooks 的本质：**一套能够使函数组件更强大、更灵活的“钩子”，帮助函数组件补齐这些（相对于类组件来说）缺失的能力【比如生命周期、对 state 的管理】**Hooks 的本质其实是链表**

#### useState()：为函数组件引入状态

```javascript
// 语法糖  状态和修改状态的 API 名都是可以自定义的 “set + 具体变量名”这种命名形式
const [state, setState] = useState(initialState);
setState(newState)

// 类组件
this.state = {initialState}
this.setState(newState)
```

```javascript
// 案例
import React, { useState } from "react";
export default function Button() {
  const [text, setText] = useState("初始state");
  function changeText() {
    return setText("修改后的state");
  }
  return (
    <div className="txt">
      <p>{text}</p>
      <button onClick={changeText}>点击修改state</button>
    </div>
  );
}
```

#### **useEffect()：允许函数组件执行副作用操作**

useEffect 则在一定程度上弥补了函数组件生命周期的缺失

**只要你在 useEffect 回调中返回了一个函数，它就会被作为清除函数来处理**

```javascript
// 语法糖
useEffect(callBack, [])

// 每一次渲染后都执行的副作用：传入回调函数，不传依赖数组。
useEffect(callBack)

// 仅在挂载阶段执行一次的副作用：传入回调函数，且这个函数的返回值不是一个函数，同时传入一个空数组
useEffect(()=>{
  // 这里是业务逻辑 
}, [])

// 仅在挂载阶段和卸载阶段执行的副作用：传入回调函数，且这个函数的返回值是一个函数，同时传入一个空数组。
useEffect(()=>{
  // 挂载阶段执行  这里是xxx业务逻辑 

  // 卸载阶段执行  useEffect 回调中返回的函数被称为“清除函数”，
  return ()=>{
      
  }
}, [])

// 每一次渲染都触发，且卸载阶段也会被触发的副作用：传入回调函数，且这个函数的返回值是一个函数，同时不传第二个参数
useEffect(()=>{
  // 挂载阶段执行

  // 卸载阶段执行
  return ()=>{
  }
})

// 根据一定的依赖条件来触发的副作用：传入回调函数，同时传入一个非空的数组
useEffect(()=>{
  // 这是回调函数的业务逻辑 

  // 若 xxx 是一个函数，则 xxx 会在组件每次因 effect1,effect2,effect3 的改变而重新渲染时被触发
  return xxx
}, [effect1,effect2,effect3]) // 数组中的变量一般都是来源于组件本身的数据（props 或者 state）
```

#### 为什么使用 hooks

- 告别难以理解的 Class；
- 解决业务逻辑难以拆分的问题；
- 使状态逻辑复用变得简单可行；
- 函数组件从设计思想上来看，更加契合 React 的理念

难以理解 Class：**this 和生命周期**

hooks出现前 复用状态逻辑，靠的是 HOC（高阶组件）和 Render Props 这些组件设计模式，会出现最常见的问题就是“嵌套地狱”现象

####  hooks使用原则

1. 只在 React 函数中调用 Hook；
2. 不要在循环、条件或嵌套函数中调用 Hook。

**约束的目的就是要确保 Hooks 在每次渲染时都保持同样的执行顺序**

### setstate是同步还是异步

```javascript
test = () => {
  console.log('循环100次 setState前的count', this.state.count)
  for(let i=0;i<100;i++) {
    this.setState({
      count: this.state.count + 1
    })
  }
  console.log('循环100次 setState后的count', this.state.count)
}
// 只是会增加 state 任务入队的次数，并不会带来频繁的 re-render。当 100 次调用结束后，仅仅是 state 的任务队列内容发生了变化， state 本身并不会立刻改变
```

setState 并不是单纯同步/异步的，它的表现会因调用场景的不同而不同：在 **React 钩子函数**及**合成事件**中，它表现为异步；而在 **setTimeout、setInterval** 等函数中，包括在 **DOM 原生事件**中，它都表现为同步。这种差异，**<u>本质上是由 React 事务机制和批量更新机制的工作方式来决定的</u>**

### React 事件与 DOM 事件对比

DOM 事件流下的性能优化思路：事件委托（也叫事件代理）是一种重要的性能优化手段

```javascript
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>事件委托</title>
</head>
<body>
  <ul id="poem">
    <li>1</li>
    <li>2</li>
    <li>3</li>
    <li>4</li>
  </ul>
</body>
</html>
<script>
  // 获取 li 列表
  var liList = document.getElementsByTagName('li')
  // 逐个 监听
  for (var i = 0; i < liList.length; i++) {
    liList[i].addEventListener('click', function (e) {
      console.log(e.target.innerHTML)
    })
  }
</script>

// 时间委托
<script>
// 事件冒泡
var ul = document.getElementById('poem')
ul.addEventListener('click', function(e){
  console.log(e.target.innerHTML)
})
</script>
```

React 的事件系统沿袭了事件委托的思想。在 React 中，除了少数特殊的不可冒泡的事件（比如媒体类型的事件）无法被事件系统处理外，绝大部分的事件都不会被绑定在具体的元素上，而是统一被绑定在页面的 document 上。**当事件在具体的 DOM 节点上被触发后，最终都会冒泡到 document 上，document 上所绑定的统一事件处理程序会将事件分发到具体的组件实例**。

在分发事件之前，React 首先会对事件进行包装，把原生 DOM 事件包装成**合成事件**

### 虚拟 DOM（Virtual DOM）本质

1. 虚拟 DOM 是 JS 对象
2. 虚拟 DOM 是对真实 DOM 的描述

虚拟 DOM（Virtual DOM）本质上是**JS 和 DOM 之间的一个映射缓存**，它在形态上表现为一个能够描述 DOM 结构及其属性信息的 **JS 对象**

### 虚拟 DOM 在 React 组件的挂载阶段和更新阶段

- **挂载阶段**，React 将结合 JSX 的描述，构建出虚拟 DOM 树，然后通过 ReactDOM.render 实现虚拟 DOM 到真实 DOM 的映射（触发渲染流水线）
- **更新阶段**，页面的变化在作用于真实 DOM 之前，会先作用于虚拟 DOM，虚拟 DOM 将在 JS 层借助算法先对比出具体有哪些真实 DOM 需要被改变，然后再将这些改变作用于真实 DOM。

### 调和（Reconciliation）过程与 Diff 算法

**调和：**通过如 ReactDOM 等类库使虚拟 DOM 与“真实的” DOM 同步，这一过程叫作协调（调和）

调和指的是将虚拟 DOM**映射**到真实 DOM 的过程，**调和过程并不能和 Diff 画等号**。调和是“使一致”的过程，而 Diff 是“找不同”的过程，它只是“使一致”过程中的一个环节

#### React 15 为代表的 “栈调和” (Diff 算法)

Stack Reconciler 需要的调和时间会很长，这就意味着 JavaScript 线程将长时间地霸占主线程，进而导致我们上文中所描述的渲染卡顿/卡死、交互长时间无响应等问题  它是同步的，不可以被打断

Reconciler （**找不同**）-> Renderer （**渲染不同**）这个过程是严格同步

#### React 16 为代表的 “Fiber 调和" (Diff 算法)

增量渲染其实也只是一种手段【把一个渲染任务分解为多个渲染任务，而后将其分散到多个帧里面】，实现增量渲染的目的，是为了实现任务的可中断、可恢复，并给不同的任务赋予不同的优先级，最终达成更加顺滑的用户体验。

Scheduler（调度器）-> Reconciler （**找不同**）->  Renderer （**渲染不同**）

#### **将 O (n3) 复杂度转换成 O (n) 复杂度前提** 

- 若两个组件属于同一个类型，那么它们将拥有相同的 DOM 树形结构；
- 处于同一层级的一组子节点，可用通过设置 key 作为唯一标识，从而维持各个节点在不同渲染过程中的稳定性。

1. Diff 算法性能突破的关键点在于“**分层对比**”；【**只针对相同层级的节点作对比**】
2. 类型一致的节点才有继续 Diff 的必要性；
3. key 属性的设置，可以帮我们尽可能重用同一层级内的节点

#### ReactDOM.render 的调用栈划分为三个阶段：

1. 初始化阶段
2. render 阶段
3. commit 阶段

同步的 ReactDOM.render，异步的 ReactDOM.createRoot

**浏览器的刷新频率为 60Hz，也就是说每 16.6ms 就会刷新一次**

### react周边生态 【主要数据管理方案以及UI】

#### Flux 

- **View**（**视图层**）：用户界面。该用户界面可以是以任何形式实现出来的，React 组件是一种形式，Vue、Angular 也完全 OK。**Flux 架构与 React 之间并不存在耦合关系**。
- **Action**（**动作**）：也可以理解为视图层发出的“消息”，它会触发应用状态的改变。
- **Dispatcher**（派发器）：它负责对 action 进行分发。
- **Store**（数据层）：它是存储应用状态的“仓库”，此外还会定义修改状态的逻辑。store 的变化最终会映射到 view 层上去

Flux 架构最核心的一个特点——**单向数据流**

#### Redux

- **Store：**它是一个单一的数据源，而且是只读的。
- **Action** 人如其名，是“动作”的意思，它是对变化的描述。
- **Reducer** 是一个纯函数，它负责**对变化进行分发和处理**，最终将新的数据返回给 Store。

Redux 架构最核心的一个特点——**单向数据流**  

如果你想对数据进行修改，只有一种途径：**派发 Action**

```javascript
// 引入 redux
import { createStore } from 'redux'

// 创建 store
const store = createStore(
    reducer,
    initial_state,
    applyMiddleware(middleware1, middleware2, ...)
);

// applyMiddleware 是中间件模块
// bindActionCreators（用于将传入的 actionCreator 与 dispatch 方法相结合，揉成一个新的方法
// combineReducers（用于将多个  reducer 合并起来）
// compose（用于把接收到的函数从右向左进行组合）
```

action -> middleware1 -> middleware2 -> middleware2 ....-> dispatch  -> reducer -> nextState

### React-Router [react-router-dom.js]

#### 路由器：BrowserRouter 和 HashRouter

BrowserRouter 是使用 HTML 5 的 history API 来控制路由跳转的

HashRouter 是通过 URL 的 hash 属性来控制路由跳转的

#### hash 模式

```javascript
// 监听hash变化，点击浏览器的前进后退会触发
window.addEventListener('hashchange', function(event){ 
    // 根据 hash 的变化更新内容
},false)
```

#### history 模式

```javascript
window.addEventListener('popstate', function(e) {
  console.log(e)
});

// history.pushState(data[,title][,url]); // 向浏览历史中追加一条记录
// history.replaceState(data[,title][,url]); // 修改（替换）当前页在浏览历史中的信息
```

go、forward 和 back 等方法的调用确实会触发 popstate，但是**pushState 和 replaceState 不会**。不过这一点问题不大，我们可以通过自定义事件和全局事件总线来手动触发事件

#### 前端路由——SPA“定位”解决方案

**拦截用户的刷新操作，避免服务端盲目响应、返回不符合预期的资源内容**

**感知 URL 的变化**

### React 设计模式

- **高阶组件（HOC）**
- **Render Props**
- **剥离有状态组件与无状态组件**
