# vue框架常见面试题

## 基础知识

### 1.vue的优缺点

优点：渐进式【想用也行不用也可以比如vuex、component，**VUE不强求你一次性接受并使用它的全部功能特性**】，组件化，轻量级，虚拟dom、响应式，数据与视图分开、双向数据绑定、简单易学、运行速度快，vue是单页面应用，使页面局部刷新，**不用每次跳转页面都要请求所有数据和dom**，这样大大加快了访问速度和提升用户体验。而且他的第三方ui库很多节省开发时间；

缺点：VUE不支持IE9以下，首屏加载时间长

### 2.vue和react的异同点

相同点：

- 都使用了虚拟dom
- 组件化开发
- 都是单向数据流(父子组件之间，不建议子修改父传下来的数据)
- 都支持服务端渲染 【vue --> nuxt.js   react ---> next.js】

不同点：

- React的JSX，Vue的template
- 数据变化，React手动(setState)，Vue自动(初始化已响应式处理，vue2 Object.defineProperty  vue3  proxy)
- React单向绑定，Vue双向绑定
- 数据管理React的Redux，Vue的Vuex

### 3.讲一讲MVVM

MVVM是`Model-View-ViewModel`缩写，也就是把`MVC`中的`Controller`演变成`ViewModel`。Model层代表数据模型，View代表UI组件，ViewModel是View和Model层的桥梁，数据会绑定到viewModel层并自动将数据渲染到页面中，视图变化的时候会通知viewModel层更新数据。

Vue是MVVM框架，但是不是严格符合MVVM，因为MVVM规定Model和View不能直接通信，而Vue的`ref`可以做到这点。

### 4.为什么data是个函数

data之所以只一个函数，是因为一个组件可能会多处调用，而每一次调用就会执行data函数并返回新的数据对象，这样，可以避免多处调用之间的数据污染

### 5.Vue常用的修饰符，常用指令

![image-20210731141513519](C:\Users\Administrator.WIN-MHB8ELOJK80\AppData\Roaming\Typora\typora-user-images\image-20210731141513519.png)

![image-20210731141640548](C:\Users\Administrator.WIN-MHB8ELOJK80\AppData\Roaming\Typora\typora-user-images\image-20210731141640548.png)

### 6.v-if 和v-show 有何区别

- `v-if`是通过控制dom元素的删除和生成来实现显隐，每一次显隐都会使组件重新跑一遍生命周期，因为显隐决定了组件的生成和销毁
- `v-show`是通过控制dom元素的css样式来实现显隐，不会销毁
- 频繁或者大数量显隐使用`v-show`，否则使用`v-if`

### 7.v-if 和v-for 那个优先级高为什么不建议用在同一标签

`v-for`优先级是高于`v-if`的

```javascript
<div v-for="item in [1, 2, 3]" v-if="item !== 3">
    {{item}}
</div>
```

```javascript
<div v-for="item in list">
    {{item}}
</div>

computed() {
    list() {
        return [1, 2, 3].filter(item => item !== 3)
    }
  }
```

`v-for`和`v-if`同时存在，会先把3个元素都遍历出来，然后再一个个判断是否为3，并把3给隐藏掉，这样的坏处就是，渲染了无用的3节点，增加无用的dom操作，建议使用computed来解决这个问题

### 8.computed和watch有何区别

- `computed`是依赖已有的变量来计算一个目标变量，大多数情况都是`多个变量`凑在一起计算出`一个变量`，并且`computed`具有`**缓存机制**`，依赖值不变的情况下其会直接读取缓存进行复用，`computed`不能进行`异步操作`
- `watch`是监听某一个变量的变化，并执行相应的回调函数，通常是`一个变量`的变决定`多个变量`的变化，`watch`可以进行`异步操作`
- 一般情况下`computed`是`多对一`，`watch`是`一对多`

### 9.watch有哪些属性，分别有什么用

监听基本数据类型

```javascript
watch: {
    value (oldval,newVal) {
        // do something
    }
}
```

监听引用数据类型 如 object

```javascript
watch: {
    obj: {
       handler () { // 执行回调
           // do something
       },
       deep: true, // 是否进行深度监听
       immediate: true // 是否初始执行handler函数
    }
}
```

### 10.computed如何实现传参

```javascript
// html
<div>{{ total(3) }}
// js
computed: {
    total() {
      return function(n) {
          return n * this.num
         }
    },
  }
```

### 11.动态指令和参数

```javascript
<template>
    ...
    <Button @[someEvent]="handleSomeEvent()" :[someProps]="1000" />...
</template>
<script>
  ...
  data(){
    return{
      ...
      someEvent: someCondition ? "click" : "dbclick",
      someProps: someCondition ? "num" : "price"
    }
  },
  methods: {
    handleSomeEvent(){
      // handle some event
    }
  }  
</script>
```

### 12.自定义指令（自定义指令控制按钮权限）

  自定义指令钩子函数
    bind：指令第一次绑定到元素时调用，只执行一次。这里可以进行一次性的初始化设置
    inserted：被绑定的元素，插入到父节点的DOM中时调用（仅保证父节点存在）
    update：组件更新时调用
    componentUpdated：组件与子组件更新时调用
    unbind：指令与元素解绑时调用，只执行一次，除了update和componentUpdated钩子函数之外，每个钩子函数都含有el、binding、vnode这三个参数
      el：在每个函数中，第一个参数永远是el，表示被绑定的指令的那个dom元素，这个el元素，是一个原生的js对象，所以Vue自定义指令可以用来直接和DOM打交道
      binding：是一个对象，它包含以下属性：name、value、oldValue、expression、arg、modifiers ，除了el之外，binding、vnode属性都是只读的

```javascript
// 只有code码对应时候  使用时候  v-permission="['100000801']"
Vue.directive('permission', {
  // inserted函数：当被绑定的元素插入到 DOM 中时……
  inserted(el, binding, vnode) {
    // console.log(el, binding, vnode)
    const { value } = binding // 获取指令绑定的值;
    const btncodeList = store.getters && store.getters.getBtnCodeList.map(e => {return e.buttonCode}); // 权限code码 后端数据返回的数据
    // console.log(value, 'value', btncodeList,'btncodeList--指令')
    if (value && value instanceof Array && value.length > 0) {
      const permissionCode = value;
      
      const hasPermission = btncodeList.some(btncode => { // 只要有一个满足即返回true
        return permissionCode.includes(btncode)
      })
      // 没有该指令,直接删除掉该指令元素;即页面不显示没有指令权限的按钮;
      if (!hasPermission) {
        el.parentNode && el.parentNode.removeChild(el)
        // 因项目需要，本指令remove其父元素;一般情况下，只隐藏其本身;
      }
    } else {
      throw new Error(`need roles! Like v-permission="			['100000801','100000802']"`)
    }
  }
});
```

```javascript
// 组件定义指令
export default {
	directives: {
        focus: {
            bind () {
                el.focus()
                console.log('bind')
			}
			inserted () {
				console.log('inserted')
			}
		}
	}
}
// 全局指令
Vue.directive('focus', {
	inserted (el, binding) {
        el.focus()
	}
})
```

### 13.插槽的使用以及原理

1. 匿名插槽

   ```javascript
   <slot><slot/>
   ```

2. 具名插槽

   ```javascript
   // 定义：<slot name="header"></slot> 或者使用简单缩写的定义#header使用：要用一个template标签包裹
   // 父组件
   <template v-slot:hasName>
       <p>具名插槽里面放置内容</p>
   </template>
   // 子组件
   <slot name="hasName"></slot>
   ```

3. 作用域插槽

   为了让子组件中的数据，在父级的插槽内容中可用我们可以将数据作为元素的一个特性绑定上去：v-bind:text="text"

   匿名的作用域插槽和具名的作用域插槽区别在v-slot:default="接受的名称"（default(匿名的可以不写，具名的要写对应的name)）

   v-slot可以解构接收，解构接收的字段要和传的字段一样才可以，例如：one对应 v-slot="{one}"

   ```javascript
   <template v-slot:default="slotProps">
   	{{ slotProps.user.firstName }}
   </template>
   ```

   ```javascript
   <slot v-bind:user="user">
       {{ user.lastName }}
   </slot>
   ```

### 14.SSR的了解

SSR也就是服务端渲染，`也就是将Vue在客户端把标签渲染成HTML的工作放在服务端完成，然后再把html直接返回给客户端`。

SSR有着更好的SEO、并且首屏加载速度更快等优点。不过它也有一些缺点，比如我们的开发条件会受到限制，服务器端渲染只支持`beforeCreate`和`created`两个钩子，当我们需要一些外部扩展库时需要特殊处理，服务端渲染应用程序也需要处于Node.js的运行环境。还有就是服务器会有更大的负载需求

## vue的组件之间的通信方式

- 父组件传值给子组件，子组件使用`props`进行接收
- 子组件传值给父组件，子组件使用`$emit+事件`对父组件进行传值
- 组件中可以使用`$parent`和`$children`获取到父组件实例和子组件实例，进而获取数据
- 使用`$attrs`和`$listeners`，在对一些组件进行二次封装时可以方便传值，例如A->B->C
- 使用`$refs`获取组件实例，进而获取数据
- 使用`Vuex`进行状态管理
- 使用`eventBus`进行跨组件触发事件，进而传递数据
- 使用`provide`和`inject`，官方建议我们不要用这个方式，但ElementUI`源码大量使用
- 使用浏览器本地缓存，sessionStorage （会话存储） 和 localStorage（本地存储）

## vue2 vue3生命周期对比

|     Vue2      |      vue3       |
| :-----------: | :-------------: |
| beforeCreate  |     setup()     |
|    created    |     setup()     |
|  beforeMount  |  onBeforeMount  |
|    mounted    |    onMounted    |
| beforeUpdate  | onBeforeUpdate  |
|    updated    |    onUpdated    |
| beforeDestroy | onBeforeUnmount |
|   destroyed   |   onUnmounted   |
|   activated   |   onActivated   |
|  deactivated  |  onDeactivated  |
| errorCaptured | onErrorCaptured |

![实例的生命周期](https://v3.cn.vuejs.org/images/lifecycle.svg)

![image-20210731142604402](C:\Users\Administrator.WIN-MHB8ELOJK80\AppData\Roaming\Typora\typora-user-images\image-20210731142604402.png)

### 1.父子组件生命周期顺序

组件的调用顺序都是`先父后子`,渲染完成的顺序是`先子后父`。

组件的销毁操作是`先父后子`，销毁完成的顺序是`先子后父`。

**<u>加载渲染过程</u>**

```
父beforeCreate->父created->父beforeMount->子beforeCreate->子created->子beforeMount- >子mounted->父mounted
```

**<u>子组件更新过程</u>**

```
父beforeUpdate->子beforeUpdate->子updated->父updated
```

**<u>父组件更新过程</u>**

```
父 beforeUpdate -> 父 updated
```

**<u>销毁过程</u>**

```
父beforeDestroy->子beforeDestroy->子destroyed->父destroyed
```

### 2.vue的hook的使用

清除定时器

```javascript
export default{
  data(){
    timer:null  
  },
  mounted(){
      this.timer = setInterval(()=>{
      //具体执行内容
      console.log('1');
    },1000);
  }
  beforeDestory(){
    clearInterval(this.timer);
    this.timer = null;
  }
}
```

```javascript
export default{
  methods:{
    fn(){
      const timer = setInterval(()=>{
        //具体执行代码
        console.log('1');
      },1000);
      this.$once('hook:beforeDestroy',()=>{
        clearInterval(timer);
        timer = null;
      })
    }
  }
}
```

如果子组件需要在mounted时触发父组件的某一个函数

```javascript
//父组件
<my-child @childMounted="childMountedHandle"
/>
method () {
  childMountedHandle() {
  // do something...
  }
},

// 子组件
mounted () {
  this.$emit('childMounted')
},
```

```javascript
//父组件
<my-child @hook:mounted="childMountedHandle" />
method () {
  childMountedHandle() {
  // do something...
  }
},
```

### 3.Vue的el属性和$mount优先级

```javascript
new Vue({
  router,
  store,
  el: '#app',
  render: h => h(App)
}).$mount('#example')
```

![image-20210731152140646](C:\Users\Administrator.WIN-MHB8ELOJK80\AppData\Roaming\Typora\typora-user-images\image-20210731152140646.png)

结论：`el`和`$mount`同时存在时，`el优先级` > `$mount`

## vuex相关

### 1.Vuex 是什么？vuex的有哪些属性？用处是什么？什么情况下我应该使用 Vuex？

![img](https://vuex.vuejs.org/flow.png)

![vuex](https://vuex.vuejs.org/vuex.png)

Vuex 是一个专为 Vue.js 应用程序开发的**状态管理模式**。它采用集中式存储管理应用的所有组件的状态，并以相应的规则保证状态以一种可预测的方式发生变化。

核心概念

- State：定义了应用状态的数据结构，可以在这里设置默认的初始状态。
- Getter：允许组件从 Store 中获取数据，mapGetters 辅助函数仅仅是将 store 中的 getter 映射到局部计算属性。
- Mutation：是唯一更改 store 中状态的方法，且必须是同步函数。
- Action：用于提交 mutation，而不是直接变更状态，可以包含任意异步操作。
- Module：允许将单一的 Store 拆分为多个 store 且同时保存在单一的状态树中。

如果你的应用足够简单，既可以不使用vuex，如果你的项目很复杂那么请你带上它一起玩耍！

vuex 数据持久化方案：

npm i -S vuex-persistedstate 链接：https://www.npmjs.com/package/vuex-persistedstate

npm i -S vuex-persist  链接：https://www.npmjs.com/package/vuex-persist

使用案例请参考官方文档

## vue-router相关

### 1.相同的路由组件如何重新渲染

```javascript
<template>
    <router-view :key="$route.path"></router-view>
</template>
```

### 2.hash路由和history路由实现原理说一下

**hash**（支持刷新）

`location.hash`的值实际就是URL中`#`后面的东西原理是onhashchage事件，可以在window对象上监听这个事件

```javascript
window.onhashchange = function(event){
  console.log(event.oldURL, event.newURL)
  let hash = location.hash.slice(1)
}
```

history (需要后端支持)

history实际采用了HTML5中提供的API来实现，主要有`history.pushState()`和`history.replaceState()`，需要后台配置支持，如果刷新时，服务器没有响应响应的资源，会刷出404【让他配置一下apache或是nginx的url重定向，重定向到你的首页路由】。

### 3.Vue路由 导航守卫（全局守卫、路由独享守卫、组件内守卫）

在登录页面中，登录请求成功之后，需要判断一下用户角色类型， 根据不同的用户角色来跳转到不同的页面。

**全局守卫**

```javascript
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'

// 设置白名单，指不需要登录就可以直接进入的页面
const whiteList = ["/login","/welcome"]
// 这里使用cookie.js，登录成功后将后台返回的token保存在cookie中
// Cookies.set('token','token_value');
const hasToken = Cookies.get('token');
router.beforeEach((to, from, next) => {
  // 进度
  NProgress.start()
 
  /* 路由发生变化修改页面title */
  if (to.meta.title) {
    document.title = to.meta.title
  }
  // 登录拦截
  if (hasToken) {
    if (to.path === '/login') {
      next('/') // 系统根路由
    } else {
      // 其他鉴权操作，比如资源是否可访问
      let menus = store.getters['user/menus']
      if (menus.includes(to.path)) {
        next()
      } else {
        next({
          path: '/403',
          replace: true // 替换当前路由，避免用户后退等操作导致重复跳转
        })
      }
    }
  } else {
    if (whiteList.includes(to.path)) {
      next()
    } else {
      next(`/login?redirect=${to.path}`)
    }
  }
})
```

```javascript
router.afterEach((to,form) => {
    NProgress.done()
})
```

**组件内守卫**

```javascript
// 组件内的路由导航守卫
beforeRouteEnter(to, from, next) {
  //不能获取当前组件实例this,因为在守卫执行之前，实例还没有被创建
  // 确认导航
  next(vm => {
    
  })
}
```

```javascript
// 路由更新时
beforeRouteUpdate(to, from, next) {
    //在当前路由改变，但是该组件被复用时调用
    //举例来说：对于一个带有动态参数的路径 /foo/:id, 在/foo/1 和 /foo/2  之间跳转的时候，由于会渲染同样的foo组件， 因此组件实例会被重复利用。 此时这个钩子就可以在这个时候调用
  //在这里可以获取到当前的this
}
```

```javascript
beforeRouteLeave(to, from, next){
 // 离开当前组件时调用
	// 确定离开当前页面么
}
```

**路由独享的守卫**

```javascript
beforeEnter: (to, from, next) => {
   	//  to 要进入的目标，路由对象
	//  from 当前导航正要离开的路由
	//  next 初步认为是展示页面；（是否显示跳转页面）
}
```

**监听路由，组件内导航钩子函数**  可以响应路由参数变化 **<u>如果参数改变视图不更新</u>**

```javascript
// 监听当前路由发生变化的时候执行
watch: {
  $route(to, from){
    console.log(to.path)
    // 对路由变化做出响应
  }
}
```

```javascript
beforeRouteUpdate(to, from, next){
  // to do somethings
}
```

### 4.vue-router传参的方式

Params

只能使用name，不能使用path
参数不会显示在路径上
浏览器强制刷新参数会被清空

```javascript
// 传递参数 /detail/xxx
this.$router.push({
    name: Home,
    params: {
     number: 1 ,
     code: '999'
    }
})
// 接收参数
const parmas = this.$route.params
```

Query:

参数会显示在路径上，刷新不会被清空
name 可以使用path路径

```javascript
// 传递参数  /detail?id=xxx
this.$router.push({
  name: Home，
  query: {
   number: 1 ,
   code: '999'
  }
})
// 接收参数
const query = this.$route.query
```



## vue优化处理

### 1.不需要响应式的数据应该怎么处理

项目中有一些数据，从始至终都`未曾改变过`，这种`死数据`，既然`不改变`，那也就`不需要对他做响应式处理`了，不然只会做一些无用功消耗性能，比如一些写死的下拉框，写死的表格数据；

```javascript
// 方法一：将数据定义在data之外
data () {
    this.list = { xxx }
    return {}
 }
    
// 方法二：Object.freeze()
data () {
    return {
        list: Object.freeze({xxx}),
    }
 }
```

### 2.为什么不建议用index做key，为什么不建议用随机数做key

用组件唯一的 id(一般由后端返回)作为它的 key，实在没有的情况下，可以在获取到列表的时候通过某种规则为它们创建一个 key，并保证这个 key 在组件整个生命周期中都保持稳定。

别用 index 作为 key，和没写基本上没区别，因为不管你数组的顺序怎么颠倒，index 都是 0, 1, 2 这样排列，导致 Vue 会复用错误的旧子节点，做很多额外的工作

千万别用随机数作为 key，不然旧节点会被全部删掉，新节点重新创建

```javascript
<div v-for="(item, index) in list" :key="index">{{item.name}}</div>

list: [
    { name: '西瓜', id: '123' },
    { name: '葡萄', id: '124' },
    { name: '芒果', id: '125' }
]

渲染为
<div key="0">西瓜</div>
<div key="1">葡萄</div>
<div key="2">芒果</div>

现在我执行 list.unshift({ name: '橘子', id: '122' })

渲染为
<div key="0">橘子</div>
<div key="1">西瓜</div>
<div key="2">葡萄</div>
<div key="3">芒果</div>

新旧对比

<div key="0">西瓜</div>  <div key="0">橘子</div>
<div key="1">葡萄</div>  <div key="1">西瓜</div>
<div key="2">芒果</div>  <div key="2">葡萄</div>
                         <div key="3">芒果</div>

可以看出，如果用index做key的话，其实是更新了原有的三项，并新增了芒果，虽然达到了渲染目的，但是损耗性能

现在我们使用id来做key，渲染为

<div key="123">西瓜</div>
<div key="124">葡萄</div>
<div key="125">芒果</div>

现在我执行 list.unshift({ name: '橘子', id: '122' })，渲染为

<div key="122">橘子</div>
<div key="123">西瓜</div>
<div key="124">葡萄</div>
<div key="125">芒果</div>

新旧对比

                           <div key="122">橘子</div>
<div key="123">西瓜</div>  <div key="123">西瓜</div>
<div key="124">葡萄</div>  <div key="124">葡萄</div>
<div key="125">芒果</div>  <div key="125">芒果</div>

可以看出，原有的三项都不变，只是新增了橘子，这才是最理想的结果
```

### 3.你都做过哪些Vue的性能优化

**<u>编码阶段</u>**

- 尽量减少data中的数据，data中的数据都会增加getter和setter，会收集对应的watcher
- v-if 和v-for 不能连用
- 如果需要使用v-for给每项元素绑定事件时使用事件代理
- SPA 页面采用keep-alive缓存组件
- 在更多的情况下，使用 v-if 替代v-show
- key保证唯一
- 使用路由懒加载、异步组件
- 防抖、节流
- 第三方模块按需导入
- 长列表滚动到可视区域动态加载（虚拟列表 vue-virtual-scroll-list）
- 图片懒加载

长列表概念：前端的业务开发中会遇到一些数据量较大且无法使用分页方式来加载的列表，我们一般把这种列表叫做长列表。
完整渲染的长列表基本上很难达到业务上的要求的，非完整渲染的长列表一般有两种方式：

**<u>懒渲染：</u>**这个就是常见的无线滚动的，每次只渲染一部分（比如10条），等剩余部分滚到可见区域，就再渲染一部分。
**可视区域渲染：**只渲染可见部分，不可见部分不渲染     `虚拟列表就是采用的可视区渲染方式优化`

- `可滚动区域`：假设有1000条数据，每个列表项的高度是30，那么可滚动的区域的高度就是`1000*30`。当用户改变列表的滚动条的当前滚动值的时候，会造成可见区域的内容的变更。
- `可见区域`：比如列表的高度是300，右侧有纵向滚动条可以滚动，那么视觉可见的区域就是可见区域

**虚拟列表原理：**用数组保存所有列表元素的位置，只渲染可视区内的列表元素，当可视区滚动时，根据滚动的offset大小以及所有列表元素的位置，计算在可视区应该渲染哪些元素

**<u>SEO优化</u>**

- 预渲染
- 服务端渲染SSR

**<u>打包优化</u>**

- 压缩代码

- Tree Shaking/Scope Hoisting

- 使用cdn加载第三方模块

- 多线程打包 happypack

- splitChunks抽离公共文件 （分包）【在 `webpack4` 之前，都是利用 `CommonsChunkPlugin` 插件来进行公共模块抽取】

  ```javascript
  module.exports = {
      optimization: {
          splitChunks: {
              cacheGroups: {
                  vendor: {
                      name: "vendor",
                      test: /[\\/]node_modules[\\/]/,
                      chunks: "all",
                      priority: 10 // 优先级
                  },
                  common: {
                      name: "common",
                      test: /[\\/]src[\\/]/,
                      minSize: 1024,
                      chunks: "all",
                      priority: 5
                  }
              }
          }
      },
  }
  ```

- sourceMap优化：开发环境: eval-source-map 或者 cheap-module-source-map
  生产环境: source-map 或者 cheap-module-source-map

```javascript
// 模块 happypack 可以时间多线程（进程）来打包 启用前和启用后的对比
// 安装：npm install happypack
const happypack = require('happypack');
```

```javascript
module:{
    rules:[
        {test:/\.js$/,
         use:{
                loader:'babel-loader',
                options:{
                    presets:[
                        //解析ES6和react语法
                        '@babel/preset-env',
                        '@babel/preset-react'
                    ]
                } 
            }
        }
     },
        {
            test:/\.css$/,
            use:['style-loader','css-loader']
        }
    ]
 }
```

```javascript
module:{
    rules:[
            {test:/\.js$/,
             use:'happypack/loader?id=js'
            },
        {
            test:/\.css$/,
            use:'happypack/loader?id=css'
        }
    ]
    },
    plugins: [
        new happypack({
            id:'css',
            use:['style-loader','css-loader']
        }),
        new happypack({
            id:'js',
            use:[{
                loader:'babel-loader',
                options:{
                    presets:[
                        //解析ES6和react语法
                        '@babel/preset-env',
                        '@babel/preset-react'
                    ]
                } 
            }]
        })
}
```

**<u>用户体验</u>**

- 骨架屏
- PWA

还可以使用缓存(客户端缓存、服务端缓存)优化、服务端开启gzip压缩等

## vue原理

### 1.Vue响应式是怎么实现的 (vue2 vue3)

Object.defineProperty(obj, prop, descriptor)

proxy(target, handler)

**区别：**

1. 对象上定义新属性时，Proxy可以监听到，Object.defineProperty监听不到。
2. proxy支持数组:不需要对数组的方法进行重载，省去了众多 hack 嵌套支持（本质也是不支持嵌套的）: get 里面递归调用 Proxy 并返回。
3. Proxy不兼容IE，Object.defineProperty不兼容IE8及以下
4. Proxy代理整个对象，Object.defineProperty只代理对象上的某个属性
5. 如果对象内部要全部递归代理，Object.defineProperty需要在一开始就全部递归，Proxy代理对象时只会在调用时递归，不会一开始就全部递归，优化了性能
6. Proxy有多达13种拦截方法,不限于apply、ownKeys、deleteProperty、has等等，这是Object.defineProperty不具备的

```javascript
let obj={a:1,b:{c:2}};
let handler={
  get:function(obj,prop){
    const v = Reflect.get(obj,prop);
    if(v !== null && typeof v === 'object'){
      return new Proxy(v,handler);//代理内层
    }else{
      return v; // 返回obj[prop]
    }
  },
  set(obj,prop,value){
    return Reflect.set(obj,prop,value);//设置成功返回true
  }
}
let p=new Proxy(obj,handler);

p.a // 会触发get方法
p.b.c // 会先触发get方法获取p.b，然后触发返回的新代理对象的.c的set。

```

整体思路是数据劫持+观察者模式，对象内部通过 `defineReactive` 方法，使用 `Object.defineProperty` 将属性进行劫持（只会劫持已经存在的属性），数组则是通过重写数组方法来实现。当页面使用对应属性时，每个属性都拥有自己的`dep`属性，存放他所依赖的 `watcher`（依赖收集），当属性变化后会通知自己对应的 `watcher` 去更新(派发更新)

**<u>监测数组变化</u>**

使用了函数劫持的方式，重写了数组的方法，Vue将data中的数组进行了原型链重写，指向了自己定义的数组原型方法。这样当调用数组api时，可以通知依赖更新。如果数组中包含着引用类型，会对数组中的引用类型再次递归遍历进行监控。这样就实现了监测数组变化

```javascript
const { arrayMethods } = require('./array')

class Observer {
    constructor(value) {
        Object.defineProperty(value, '__ob__', {
            value: this,
            enumerable: false,
            writable: true,
            configurable: true
        })
        if(Array.isArray(value)) {
            value.__proto__ = arrayMethods
            this.observeArray(value)
        } else {
            this.walk(value)
        }
    }
    walk(data) {
        let keys = Object.keys(data)
        for(let i = 0; i < keys.length; i++) {
            const key = keys[i]
            const value = data[key]
            defineReactive(data, key, value)
        }
    }
    observeArray(items) {
        for(let i = 0; i < items.length; i++) {
            observe(items[i])
        }
    }
}

function defineReactive(data, key, value) {
    const childOb = observe(value)
    const dep = new Dep()
    Object.defineProperty(data, key, {
        get() {
            console.log('获取值')
            if (Dep.target) {
                dep.depend()
                if (childOb) {
                    childOb.dep.depend()
                    if (Array.isArray(value)) {
                        dependArray(value)
                    }
                }
            }
            return value
        },
        set(newVal) {
            if (newVal === value) return
            observe(newVal)
            value = newVal
            dep.notify()
        }
    })
}

function observe(value) {
    if (Object.prototype.toString.call(value) === '[object Object]' || Array.isArray(value)) {
        return new Observer(value)
    }
}

function dependArray(value) {
    for(let e, i = 0, l = value.length; i < l; i++) {
        e = value[i]
        e && e.__ob__ && e.__ob__.dep.depend()
        if (Array.isArray(e)) {
            dependArray(e)
        }
    }
}

// array.js
const arrayProto = Array.prototype

const arrayMethods = Object.create(arrayProto)

const methodsToPatch = [
    'push',
    'pop',
    'shift',
    'unshift',
    'splice',
    'reverse',
    'sort'
]

methodsToPatch.forEach(method => {
    arrayMethods[method] = function (...args) {
        const result = arrayProto[method].apply(this, args)
        const ob = this.__ob__
        var inserted
        switch (method) {
            case 'push':
            case 'unshift':
                inserted = args
                break;
            case 'splice':
                inserted = args.slice(2)
            default:
                break;
        }
        if (inserted) ob.observeArray(inserted)
        	ob.dep.notify()
        return result
    }
})
```

Vue3 的响应式系统被放到了一个单独的**@vue/reactivity**模块中，其提供了**reactive**、**effect**、**computed**等方法，其中reactive用于**定义响应式的数据**，effect相当于是Vue2中的**watcher**，computed用于定义**计算属性**。

```javascript
export function reactive(target) {
    return new Proxy(target, {
        get() {

        },
        set() {
            
        }
    });
}
```

### 2.为什么只对对象劫持，而要对数组进行方法重写

因为对象最多也就几十个属性，拦截起来数量不多，但是数组可能会有几百几千项，拦截起来非常耗性能，所以直接重写数组原型上的方法，是比较节省性能的方案

### 3.Vue的computed和watch的原理

vue中属性计算利用函数闭包和对象属性set，get函数 完美实现     ( 测试打印 console.log(obj.b)   obj.a += 1;   console.log(obj.b))

```javascript
var Dep = null
    function defineReactive(obj,key,val){
        var deps = [] // deps 收集依赖用
        Object.defineProperty(obj,key,{
            get:function(){
                if(Dep){
                    deps.push(Dep)
                }
                return val
            },
            set:function(newVal){
                val = newVal;
                deps.forEach(func=>func()) // deps 在set函数中被引用 形成闭包
            }
        })
    }

    function defineComputed(obj,key,func){
        func = func.bind(obj)
        var value
        Dep = function(){
            value = func() // value 在此函数中被引用 形成闭包
        }
        value = func() // 执行一次 属性计算函数，计算属性函数中的this.a(36行)的执行,会执行a的get函数(5行),将计算属性函数放到依赖项中(第7行)
        Dep = null
        Object.defineProperty(obj,key,{
            get:function(){
                return value // value 在set函数中被引用 形成闭包
            }
        })
    }

    var obj = {}
    defineReactive(obj,"a",0)
    defineComputed(obj,"b",function(){
        var a = this.a
        return a + 1
    })
```

watch监听原理的关键在于：watch下key必须是被数据拦截的属性

- 每一个key都会对应一个watcher对象
- watcher对象的getter函数就是获取当前key对应的值，从而建立key对应Dep对象与当前watcher对象建立联系
- 当key改变时就会触发视图更新，从而执行key对应的回调函数

$watch实例方法

- Watcher对象生成
- immediate配置：实际上就是立即调用对应的回调函数
- unwatch逻辑处理即取消watch监听

### 4.Vue的模板编译原理（简单说个主要流程）

Vue的编译过程就是将`template`转化为`render`函数的过程

- 生成AST树
- 优化
- codegen

首先解析模版，生成`AST语法树`(一种用JavaScript对象的形式来描述整个模板)。使用大量的正则表达式对模板进行解析，遇到标签、文本的时候都会执行对应的钩子进行相关处理。

Vue的数据是响应式的，但其实模板中并不是所有的数据都是响应式的。有一些数据首次渲染后就不会再变化，对应的DOM也不会变化。那么优化过程就是深度遍历AST树，按照相关条件对树节点进行标记。这些被标记的节点(静态节点)我们就可以`跳过对它们的比对`，对运行时的模板起到很大的优化作用

### 5.Vue.set方法的原理

```javascript
function set(target, key, val) {
    // 判断是否是数组
    if (Array.isArray(target)) {
        // 判断谁大谁小
        target.length = Math.max(target.length, key)
        // 执行splice
        target.splice(key, 1, val)
        return val
    }
    const ob = target.__ob__
    // 如果此对象没有不是响应式对象，直接设置并返回
    if (key in target && !(key in target.prototype) || !ob) {
        target[key] = val
        return val
    }
    // 否则，新增属性，并响应式处理
    defineReactive(target, key, val)
    return val
}
```

### 6.Vue.delete方法的原理

```javascript
function del (target, key) {
    // 判断是否为数组
    if (Array.isArray(target)) {
        // 执行splice
        target.splice(key, 1)
        return
    }
    const ob = target.__ob__
    // 对象本身就没有这个属性，直接返回
    if (!(key in target)) return
    // 否则，删除这个属性
    delete target[key]
    // 判断是否是响应式对象，不是的话，直接返回
    if (!ob) return
    // 是的话，删除后要通知视图更新
    ob.dep.notify()
}
```

### 7.nextTick的原理

```javascript
let callbacks = []; //回调函数
let pending = false;
function flushCallbacks() {
  pending = false; //把标志还原为false
  // 依次执行回调
  for (let i = 0; i < callbacks.length; i++) {
    callbacks[i]();
  }
}
let timerFunc; //先采用微任务并按照优先级优雅降级的方式实现异步刷新
if (typeof Promise !== "undefined") {
  // 如果支持promise
  const p = Promise.resolve();
  timerFunc = () => {
    p.then(flushCallbacks);
  };
} else if (typeof MutationObserver !== "undefined") {
  // MutationObserver 主要是监听dom变化 也是一个异步方法
  let counter = 1;
  const observer = new MutationObserver(flushCallbacks);
  const textNode = document.createTextNode(String(counter));
  observer.observe(textNode, {
    characterData: true,
  });
  timerFunc = () => {
    counter = (counter + 1) % 2;
    textNode.data = String(counter);
  };
} else if (typeof setImmediate !== "undefined") {
  // 如果前面都不支持 判断setImmediate
  timerFunc = () => {
    setImmediate(flushCallbacks);
  };
} else {
  // 最后降级采用setTimeout
  timerFunc = () => {
    setTimeout(flushCallbacks, 0);
  };
}

export function nextTick(cb) {
  callbacks.push(cb);
  if (!pending) {
    pending = true;
    timerFunc();
  }
}
```

### 8.如果子组件改变props里的数据会发生什么

改变的props数据基本类型，则会报错

```javascript
props: {
  num: Number,
}
created() {
  this.num = 666
}
```

![image-20210731150746318](C:\Users\Administrator.WIN-MHB8ELOJK80\AppData\Roaming\Typora\typora-user-images\image-20210731150746318.png)

改变的props数据是引用类型

```javascript
props: {
    item: {
      default: () => {},
    }
  }
created() {
    // 不报错，并且父级数据会跟着变
    this.item.name = 'xxx';
    // 会报错，跟基础类型报错一样
    this.item = 'xxx'
},
```

props 自定义验证

```javascript
props: {
    num: {
      default: 1,
          validator: function (value) {
              // 返回值为true则验证不通过，报错
              return [ 1, 2, 3, 4, 5 ].indexOf(value) !== -1
          }
    }
}
```

### 9.watch监听一个对象时，如何排除某些属性的监听

```javascript
mounted() {
    Object.keys(this.params)
    	.filter((_) => !["c", "d"].includes(_)) // 排除对c，d属性的监听
    	.forEach((_) => {
   		 	this.$watch((vm) => vm.params[_], handler, {
    		deep: true,
    	});
    });
},
data() {
	return {
		params: {
			a: 1,
			b: 2,
			c: 3,
			d: 4
		},
	};
},
watch: {
	params: {
		deep: true,
		handler() {
			this.getList;
		},
	},
}

```

### 10.自定义v-model（2.2.0+ 新增）

默认情况下，v-model 是 @input 事件侦听器和 :value 属性上的语法糖。但是，你可以在你的Vue组件中指定一个模型属性来定义使用什么事件和value属性

```javascript
export default: {
  model: {
    event: 'change',
    prop: 'checked'  
  }
}
```

官方文档：https://cn.vuejs.org/v2/guide/components-custom-events.html#%E8%87%AA%E5%AE%9A%E4%B9%89%E7%BB%84%E4%BB%B6%E7%9A%84-v-model

this.$options.data().xx   可以获取data中某一个数据的初始状态

### 11.对象新属性无法更新视图，删除属性无法更新视图，为什么？怎么办？

- 原因：`Object.defineProperty`没有对对象的新属性进行属性劫持
- 对象新属性无法更新视图：使用`Vue.$set(obj, key, value)`，组件中`this.$set(obj, key, value)`
- 删除属性无法更新视图：使用`Vue.$delete(obj, key)`，组件中`this.$delete(obj, key)`

直接arr[index] = xxx无法更新视图

- 原因：Vue没有对数组进行`Object.defineProperty`的属性劫持，所以直接arr[index] = xxx是无法更新视图的
- 使用数组的splice方法，`arr.splice(index, 1, item)`
- 使用`Vue.$set(arr, index, value)`

### 12.Vue事件绑定原理

原生事件绑定是通过`addEventListener`绑定给真实元素的，组件事件绑定是通过Vue自定义的`$on`实现的

$on也是采用了经典的发布订阅者设计模式，首先定义一个事件中心，通过`$on`订阅事件，将事件存储在事件中心里面，然后通过`$emit`触发事件中心里面存储的订阅事件

```javascript
Vue.prototype.$on = function (event, fn) {
    const vm: Component = this
    if (Array.isArray(event)) {
        for (let i = 0, l = event.length; i < l; i++) {
            this.$on(event[i], fn)
        }
    } else {
        (vm._events[event] || (vm._events[event] = [])).push(fn)
    }
    return vm
}
```

### 13.Vue2.x和Vue3.x diff算法分别说一下

简单来说，diff算法有以下过程

- 同级比较，再比较子节点
- 先判断一方有子节点一方没有子节点的情况(如果新的children没有子节点，将旧的子节点移除)
- 比较都有子节点的情况(核心diff)
- 递归比较子节点

正常Diff两个树的时间复杂度是`O(n^3)`，但实际情况下我们很少会进行`跨层级的移动DOM`，所以Vue将Diff进行了优化，从`O(n^3) -> O(n)`，只有当新旧children都为多个子节点时才需要用核心的Diff算法进行同层级比较。

Vue2的核心Diff算法采用了`双端比较`的算法，同时从新旧children的两端开始进行比较，借助key值找到可复用的节点，再进行相关操作。相比React的Diff算法，同样情况下可以减少移动节点次数，减少不必要的性能损耗，更加的优雅。

Vue3.x借鉴了ivi算法和 inferno算法

在创建VNode时就确定其类型，以及在`mount/patch`的过程中采用`位运算`来判断一个VNode的类型，在这个基础之上再配合核心的Diff算法，使得性能上较Vue2.x有了提升。(实际的实现可以结合Vue3.x源码看。)

该算法中还运用了`动态规划`的思想求解最长递归子序列。

### 14.虚拟Dom以及key属性的作用

由于在浏览器中操作DOM是很昂贵的。频繁的操作DOM，会产生一定的性能问题。这就是虚拟Dom的`产生原因`。

Vue2 的Virtual DOM借鉴了开源库`snabbdom`的实现。

`Virtual DOM本质就是用一个原生的JS对象去描述一个DOM节点。是对真实DOM的一层抽象。`(也就是源码中的VNode类，它定义在src/core/vdom/vnode.js中。)

VirtualDOM映射到真实DOM要经历VNode的create、diff、patch等阶段。

**「key的作用是尽可能的复用 DOM 元素。」**

新旧 children 中的节点只有顺序是不同的时候，最佳的操作应该是通过移动元素的位置来达到更新的目的。

需要在新旧 children 的节点中保存映射关系，以便能够在旧 children 的节点中找到可复用的节点。key也就是children中节点的唯一标识

### 15.keep-alive了解吗

`keep-alive`可以实现组件缓存，当组件切换时不会对当前组件进行卸载。

常用的两个属性`include/exclude`，允许组件有条件的进行缓存。

两个生命周期`activated/deactivated`，用来得知当前组件是否处于活跃状态。

keep-alive的中还运用了`LRU(Least Recently Used)`算法

**<u>LRU</u>**

LRU（Least recently used）算法根据数据的历史访问记录来进行淘汰数据，其核心思想是“如果数据最近被访问过，那么将来被访问的几率也更高”。

最常见的实现是使用一个链表保存缓存数据，详细算法实现如下：

1. 新数据插入到链表头部；
2. 每当缓存命中（即缓存数据被访问），则将数据移到链表头部；
3. 当链表满的时候，将链表尾部的数据丢弃。

当存在热点数据时，LRU的效率很好，但偶发性的、周期性的批量操作会导致LRU命中率急剧下降，缓存污染情况比较严重。复杂度比较简单，代价则是命中时需要遍历链表，找到命中的数据块索引，然后需要将数据移到头部。

### 16.style scoped 原理

会在DOM结构及css样式上加上唯一性的标记【data-v-something】属性，即CSS带属性选择器，以此完成类似作用域的选择方式，从而达到样式私有化，不污染全局的作用。

### 17.为什么vuex中要通过mutations修改state，而不是直接修改state

因为state是实时更新的，mutations无法进行异步操作，当你异步对state进行操作时，还没执行完，这时候如果state已经在其他地方被修改了，这样就会导致程序出现问题了，所以state要同步操作，通过mutations的方式限制了不允许异步。

