<!--
 * @Description: 项目备注
 * @Version: 2.0
 * @Autor: lhl
 * @Date: 2021-09-05 14:07:54
 * @LastEditors: lhl
 * @LastEditTime: 2021-09-05 14:17:06
-->

### 克隆代码仓时候报错

unable to access 'https://github.com/vuejs/vue.git/': OpenSSL SSL_read: Connection
was reset, errno 10054

# git bash 解决进入桌面 配置下面命令即可关闭 解除 ssl 验证后，再次 git 即可

cd ..
git config --global http.sslVerify "false"

### vue2.6 版本源码 （2.6.11）

https://github.com/vuejs/vue

### 源码分析

1、拉取源码
2、安装依赖 cnpm/npm i
3、拉取源码 git clone https://github.com/vuejs/vue.git
4、修改 dev 脚本，添加 sourcemap，package.json
5、安装 rollup： cnpm/npm i -g rollup 或者安装在 package.json cnpm/npm i -D rollup
5、跑起来 cnpm/npm run dev
