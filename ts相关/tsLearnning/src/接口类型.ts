/*
 * @Description: 接口类型
 * @Version: 2.0
 * @Autor: lhl
 * @Date: 2020-11-07 19:32:03
 * @LastEditors: lhl
 * @LastEditTime: 2020-11-07 19:40:20
 */
interface Point {
    x:number,
    y:number,
    color?: string // 可选属性
    readonly z: string // 只读熟悉
    [prop: number]:number, // 任意属性 - 索引熟悉 1.字符串索引 2.数字索引
}

let p1:Point = {
    x:1,
    y:1,
    z: '2'
}
// Point.z = '更改' 错误
