# vue专项面试题

**vue的两个核心点**
数据驱动：ViewModel，保证数据和视图的一致性。
组件系统：应用类UI可以看作全部是由组件树构成的

### 1、v-if与v-for按个优先级高？如果两个同时出现应该怎么样优化得到更的性能？

源码中找答案：compiler/codegen/index.js  

- v-for优先【高于】于v-if被解析 
- 如果同时出现，每次渲染都会先执行循环再判断条件，无论如何循环都不可避免，浪费了性能 
- 要避免出现这种情况，则在外层嵌套template，在这一层进行v-if判断，然后在内部进行v-for循环 
- 如果条件出现在循环内部，可通过计算属性【computed】提前过滤掉【filter】那些不需要显示的项 

### 2、Vue组件data为什么必须是个函数而Vue的根实例则没有此限制？ 

源码中找答案：src\core\instance\state.js - initData() 

函数每次执行都会返回全新data对象实例 

Vue组件可能存在多个实例，如果使用对象形式定义data，则会导致它们共用一个data对象，那么状态 变更将会影响所有组件实例，这是不合理的；采用函数形式定义，在initData时会将其作为工厂函数返 回全新data对象，有效规避多实例之间状态污染问题。而在Vue根实例创建过程中则不存在该限制，也 是因为根实例只能有一个，不需要担心这种情况。 

### 3、你知道vue中key的作用和工作原理吗？说说你对它的理解

源码中找答案：src\core\vdom\patch.js - updateChildren()

key的作用主要是为了高效的更新虚拟DOM，其原理是vue在patch过程中通过key可以精准判断两个节点是否是同一个，从而避免频繁更新不同元素，使得整个patch过程更加高效，减少DOM操 作量，提高性能。 另外，若不设置key还可能在列表更新时引发一些隐蔽的bug vue中在使用相同标签名元素的过渡切换时，也会使用到key属性，其目的也是为了让vue可以区分 它们，否则vue只会替换其内部属性而不会触发过渡效果。 

### 4、你怎么理解vue中的diff算法？

snabbdom.js【vue2】

- diff算法是虚拟DOM技术的必然产物：通过新旧虚拟DOM作对比（即diff），将变化的地方更新在真 实DOM上；另外，也需要diff高效的执行对比过程，从而降低时间复杂度为O(n)。 
- vue 2.x中为了降低Watcher粒度，每个组件只有一个Watcher与之对应，只有引入diff才能精确找到 发生变化的地方。 
  vue中diff执行的时刻是组件实例执行其更新函数时，它会比对上一次渲染结果oldVnode和新的渲染 结果newVnode，此过程称为patch。
- diff过程整体遵循深度优先、同层比较的策略；两个节点之间比较会根据它们是否拥有子节点或者文 本节点做不同操作；比较两组子节点是算法的重点，首先假设头尾节点可能相同做4次比对尝试，如果 没有找到相同节点才按照通用方式遍历查找，查找结束再按情况处理剩下的节点；借助key通常可以非 常精确找到相同节点，因此整个patch过程非常高效

```javascript
// Vue描述一个节点
{
  el:  div  //对真实的节点的引用，本例中就是document.querySelector('#id.classA')
  tagName: 'DIV',   //节点的标签
  sel: 'div#v.classA'  //节点的选择器
  data: null,       // 一个存储节点属性的对象，对应节点的el[prop]属性，例如onclick , style
  children: [], //存储子节点的数组，每个子节点也是vnode结构
  text: null,    //如果是文本节点，对应文本节点的textContent，否则为null
}
```

```javascript
// patch diff时调用 patch 函数，patch接收两个参数 vnode，oldVnode，分别代表新旧节点
function patch (oldVnode, vnode) {
    if (sameVnode(oldVnode, vnode)) {
        patchVnode(oldVnode, vnode)
    } else {
        const oEl = oldVnode.el
        let parentEle = api.parentNode(oEl)
        createEle(vnode)
        if (parentEle !== null) {
            api.insertBefore(parentEle, vnode.el, api.nextSibling(oEl))
            api.removeChild(parentEle, oldVnode.el)
            oldVnode = null
        }
    }
    return vnode
}
// patch函数内第一个if判断sameVnode(oldVnode, vnode)就是判断这两个节点是否为同一类型节点
function sameVnode(oldVnode, vnode){
  //两节点key值相同，并且sel属性值相同，即认为两节点属同一类型，可进行下一步比较
    return vnode.key === oldVnode.key && vnode.sel === oldVnode.sel
}
// 即便同一个节点元素比如div，它的className不同，Vue就认为是两个不同类型的节点，执行删除旧节点、插入新节点操作。这与react diff实现是不同的，react对于同一个节点元素认为是同一类型节点，只更新其节点上的属性
```

```javascript
// patchVnode 对于同类型节点调用 patchVnode(oldVnode, vnode)进一步比较
patchVnode (oldVnode, vnode) {
    const el = vnode.el = oldVnode.el  //让vnode.el引用到现在的真实dom，当el修改时，vnode.el会同步变化。
    let i, oldCh = oldVnode.children, ch = vnode.children
    if (oldVnode === vnode) return  //新旧节点引用一致，认为没有变化
    //文本节点的比较
    if (oldVnode.text !== null && vnode.text !== null && oldVnode.text !== vnode.text) {
        api.setTextContent(el, vnode.text)
    } else {
        updateEle(el, vnode, oldVnode)
        //对于拥有子节点(两者的子节点不同)的两个节点，调用updateChildren
        if (oldCh && ch && oldCh !== ch) {
            updateChildren(el, oldCh, ch)
        }else if (ch){  //只有新节点有子节点，添加新的子节点
            createEle(vnode) //create el's children dom
        }else if (oldCh){  //只有旧节点内存在子节点，执行删除子节点操作
            api.removeChildren(el)
        }
    }
}
```

```javascript
// updateChildren patchVnode中有一个重要的概念updateChildren，这是Vue diff实现的核心
function updateChildren (parentElm, oldCh, newCh, insertedVnodeQueue, removeOnly) {
    var oldStartIdx = 0;
    var newStartIdx = 0;
    var oldEndIdx = oldCh.length - 1;
    var oldStartVnode = oldCh[0];
    var oldEndVnode = oldCh[oldEndIdx];
    var newEndIdx = newCh.length - 1;
    var newStartVnode = newCh[0];
    var newEndVnode = newCh[newEndIdx];
    var oldKeyToIdx, idxInOld, vnodeToMove, refElm;

    // removeOnly is a special flag used only by <transition-group>
    // to ensure removed elements stay in correct relative positions
    // during leaving transitions
    var canMove = !removeOnly;

    {
      checkDuplicateKeys(newCh);
    }
    // 如果索引正常
    while (oldStartIdx <= oldEndIdx && newStartIdx <= newEndIdx) {
        // 当前的开始旧节点没有定义，进入下一个节点
      if (isUndef(oldStartVnode)) {
        oldStartVnode = oldCh[++oldStartIdx]; // Vnode has been moved left
        // 当前的结束旧节点没有定义，进入上一个节点
      } else if (isUndef(oldEndVnode)) {
        oldEndVnode = oldCh[--oldEndIdx];
        // 如果旧的开始节点与新的开始节点相同，则开始更新该节点，然后进入下一个节点
      } else if (sameVnode(oldStartVnode, newStartVnode)) {
　　　　　// 更新节点
        patchVnode(oldStartVnode, newStartVnode, insertedVnodeQueue);
        oldStartVnode = oldCh[++oldStartIdx];
        newStartVnode = newCh[++newStartIdx];
        // 如果旧的结束节点与新的结束节点相同，则开始更新该节点，然后进入下一个节点
      } else if (sameVnode(oldEndVnode, newEndVnode)) {
        patchVnode(oldEndVnode, newEndVnode, insertedVnodeQueue);
        oldEndVnode = oldCh[--oldEndIdx];
        newEndVnode = newCh[--newEndIdx];
        // 如果旧的开始节点与新的结束节点相同，更新节点后把旧的开始节点移置节点末尾
      } else if (sameVnode(oldStartVnode, newEndVnode)) { // Vnode moved right
        patchVnode(oldStartVnode, newEndVnode, insertedVnodeQueue);
        canMove && nodeOps.insertBefore(parentElm, oldStartVnode.elm, nodeOps.nextSibling(oldEndVnode.elm));
        oldStartVnode = oldCh[++oldStartIdx];
        newEndVnode = newCh[--newEndIdx];
        // 如果旧的结束节点与新的开始节点相同，更新节点后把旧的结束节点移置节点开头
      } else if (sameVnode(oldEndVnode, newStartVnode)) { // Vnode moved left
        patchVnode(oldEndVnode, newStartVnode, insertedVnodeQueue);
        canMove && nodeOps.insertBefore(parentElm, oldEndVnode.elm, oldStartVnode.elm);
        oldEndVnode = oldCh[--oldEndIdx];
        newStartVnode = newCh[++newStartIdx];
      } else {
          // 如果旧的节点没有定义key，则创建key
        if (isUndef(oldKeyToIdx)) { oldKeyToIdx = createKeyToOldIdx(oldCh, oldStartIdx, oldEndIdx); }
        idxInOld = isDef(newStartVnode.key)
          ? oldKeyToIdx[newStartVnode.key]
          : findIdxInOld(newStartVnode, oldCh, oldStartIdx, oldEndIdx);
        // 如果没有定义index,则创建新的新的节点元素
        if (isUndef(idxInOld)) { // New element
          createElm(newStartVnode, insertedVnodeQueue, parentElm, oldStartVnode.elm);
        } else {
          vnodeToMove = oldCh[idxInOld];
          if (sameVnode(vnodeToMove, newStartVnode)) {
            patchVnode(vnodeToMove, newStartVnode, insertedVnodeQueue);
            oldCh[idxInOld] = undefined;
            canMove && nodeOps.insertBefore(parentElm, vnodeToMove.elm, oldStartVnode.elm);
          } else {
            // same key but different element. treat as new element
            createElm(newStartVnode, insertedVnodeQueue, parentElm, oldStartVnode.elm);
          }
        }
        newStartVnode = newCh[++newStartIdx];
      }
    }
    // 如果旧节点的开始index大于结束index,则创建新的节点  如果新的开始节点index大于新的结束节点则删除旧的节点
    if (oldStartIdx > oldEndIdx) {
      refElm = isUndef(newCh[newEndIdx + 1]) ? null : newCh[newEndIdx + 1].elm;
      addVnodes(parentElm, refElm, newCh, newStartIdx, newEndIdx, insertedVnodeQueue);
    } else if (newStartIdx > newEndIdx) {
      removeVnodes(parentElm, oldCh, oldStartIdx, oldEndIdx);
    }
  }
// 过程总结：oldCh和newCh各有两个头尾的变量StartIdx和EndIdx，它们的2个变量相互比较，一共有4种比较方式。如果4种比较都没匹配，如果设置了key，就会用key进行比较，在比较的过程中，变量会往中间靠，一旦StartIdx>EndIdx表明oldCh和newCh至少有一个已经遍历完了，就会结束比较
```

patch.js - patchVnode() 
patchVnode是diff发生的地方，整体策略：深度优先，同层比

- diff算法是虚拟DOM技术的必然产物：通过新旧虚拟DOM作对比（即diff），将变化的地方更新在真 实DOM上；另外，也需要diff高效的执行对比过程，从而降低时间复杂度为O(n) 
- vue 2.x中为了降低Watcher粒度，每个组件只有一个Watcher与之对应，只有引入diff才能精确找到 发生变化的地方
- vue中diff执行的时刻是组件实例执行其更新函数时，它会比对上一次渲染结果oldVnode和新的渲染 结果newVnode，此过程称为patch
- diff过程整体遵循**<u>深度优先、同层比较</u>**的策略；两个节点之间比较会根据它们是否拥有子节点或者文 本节点做不同操作；比较两组子节点是算法的重点，首先假设头尾节点可能相同做4次比对尝试，如果 没有找到相同节点才按照通用方式遍历查找，查找结束再按情况处理剩下的节点；借助key通常可以非常精确找到相同节点，因此整个patch过程非常高效

### 5、谈一谈对vue组件化的理解？ 

- 组件是独立和可复用的代码组织单元。组件系统是 Vue 核心特性之一，它使开发者使用小型、独立和通常可复用的组件构建大型应用； 
- 组件化开发能大幅提高应用开发效率、测试性、复用性等； 
- 组件使用按分类有：页面组件、业务组件、通用组件； 
- vue的组件是基于配置的，我们通常编写的组件是组件配置而非组件，框架后续会生成其构造函数，它们基VueComponent，扩展于Vue； 
- vue中常见组件化技术有：属性prop，自定义事件，插槽等，它们主要用于组件通信、扩展等； 
- 合理的划分组件，有助于提升应用性能； 组件应该是高内聚、低耦合的； 遵循单向数据流的原则。 

### 6、谈一谈对vue设计原则的理解？ 

vue.js的两个核心

**数据驱动【数据的双向绑定】：**ViewModel，保证数据和视图的一致性。

**组件系统：**应用类UI可以看作全部是由组件树构成的

- 渐进式JavaScript框架 
- 易用、灵活和高效 

渐进式***\*JavaScript\****框架： 

Vue 被设计为可以自底向上逐层应用。Vue 的核心库只关注视图层，不仅易 于上手，还便于与第三方库或既有项目整合。另一方面，当与[现代化的工具链](https://cn.vuejs.org/v2/guide/single-file-components.html)以及各种[支持类库](#libraries--plugins)结合使 用时，Vue 也完全能够为复杂的单页应用提供驱动。   

**易用性** 

vue提供数据响应式、声明式模板语法和基于配置的组件系统等核心特性。这些使我们只需要关注应用 的核心业务即可，只要会写js、html和css就能轻松编写vue应用。 

**灵活性** 

渐进式框架的最大优点就是灵活性，如果应用足够小，我们可能仅需要vue核心特性即可完成功能；随 着应用规模不断扩大，我们才可能逐渐引入路由【vue-router】、状态管理【vuex】、vue-cli等库和工具，不管是应用体积还是 学习难度都是一个逐渐增加的平和曲线。 

**高效性** 

超快的虚拟 DOM 和 diff 算法使我们的应用拥有最佳的性能表现。 
追求高效的过程还在继续，vue3中引入Proxy对数据响应式改进以及编译器中对于静态内容编译的改进 都会让vue更加高效。

### 7、谈谈你对MVC、MVP和MVVM的理解？ 

前后端分离后的架构演变----MVC、MVP、MVVM

**MVC**
前端的MVC与后端类似，具备着View、Controller和Model。 
Model：负责保存应用数据，与后端数据进行同步 
Controller：负责业务逻辑，根据用户行为对Model数据进行修改 View：负责视图展示，将model中的数据可视化出来

**MVP**
MVP与MVC很接近，P指的是Presenter，presenter可以理解为一个中间人，它负责着View和Model之 间的数据流动，防止View和Model之间直接交流

presenter负责和Model进行双向交互，还和View进行双向交互。这种交互方式， 相对于MVC来说少了一些灵活，VIew变成了被动视图，并且本身变得很小。虽然它分离了View和 Model。但是应用逐渐变大之后，导致presenter的体积增大，难以维护。要解决这个问题，或许可以 从MVVM的思想中找到答案

**MVVM**

可以分解成(Model-View-VIewModel)

ViewModel通过实现一套数据响应式机制自动响应Model中数据变化； 
同时Viewmodel会实现一套更新策略自动将数据变化转换为视图更新； 
通过事件监听响应View中用户交互修改Model中数据。 

这样在ViewModel中就减少了大量DOM操作代码。 
MVVM在保持View和Model松耦合的同时，还减少了维护它们关系的代码，使用户专注于业务逻辑，兼 顾开发效率和可维护性

![image-20210919222941212](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210919222941212.png)

### 8、你了解哪些Vue性能优化方法？

- 路由懒加载 【三种方式】

1. vue 异步组件
2. es 提案的 import()
3. webpack的 require,ensure()

```javascript
// 第一种
component: resolve => require(['组件路径'],resolve)
// 第二种
// 没有指定webpackChunkName，每个组件打包成一个js文件
const 组件名 = () => import('组件路径');
// 指定了相同的webpackChunkName，会合并打包成一个js文件
const Home = () => import(/* webpackChunkName: 'index'*/ '@/components/home')
const Index = () => import(/* webpackChunkName: 'index' */ '@/components/index')
// 第三种 r就是resolve
// 多个路由指定相同的chunkName，会合并打包成一个js文件
component: r => require.ensure([], () => r(require('组件路径')), 'chunkName')
```

- keep-alive缓存页面： include 值为字符串或者正则表达式匹配的组件name会被缓存。exclude 值为字符串或正则表达式匹配的组件name不会被缓存，或者利用路由中的meta属性来控制如：meta:{ keepAlive:true }  结合路由守卫来实现需要缓存组件的缓存 比如 beforeRouteLeave 看看哪些是需要取消缓存的，keep-alive 运用了 LRU 算法，选择最近最久未使用的组件予以淘汰

- v-for 遍历避免同时使用 v-if 【v-for加key，key建议不用index or随机数，影响dom更新效率】
- 长列表性能优化：可以通过按需进行加载dom，即在可视区域内根据当前滚动的高度和列表的长度进行渲染响应dom页面中减少dom的结构数据【插件：vue-virtual-scroller】Object.freeze 方法来冻结一个对象
- v-if 【v-show区别使用】适用于在运行时很少改变条件，不需要频繁切换条件的场景；v-show 则适用于需要非常频繁切换条件的场景使用v-show复用DOM 
- computed【值有缓存，只有它依赖的属性值发生改变，下一次获取 computed 的值时才会重新计算 computed 的值】 和 watch【每当监听的数据变化时都会执行回调进行后续操作】 区分使用场景
- 图片资源懒加载【vue-lazyload】

```javascript
// main.js
import VueLazyLoad from 'vue-lazyload'
Vue.use(VueLazyLoad, {
  preLoad: 1,
  error: require('./assets/img/error.jpg'),
  loading: require('./assets/img/homePage_top.jpg'),
  attempt: 2,
})
// 组件里使用
<li v-for="(item,index) in imgList"> 
  <img v-lazy="item" alt="seo" /> 
</li>
```

- 第三方插件的按需引入：如UI组件【babel-plugin-component】

```javascript
// babel.config.js
plugins: [
    [
      'import',
      {
        libraryName: 'vant', // vant UI
        libraryDirectory: 'es',
        style: true
      },
      'vant'
    ]
 ]
```

- 服务端渲染 SSR【nuxt.js】 or 预渲染,

**服务端渲染的优点：**更好的 SEO、更快的内容到达时间（首屏加载更快）SSR 直接由服务端渲染好页面直接返回显示，无需等待下载 js 文件及再去渲染等

**服务端渲染的缺点：**更多的开发条件限制【只支持 beforCreate 和 created 两个钩子函数】服务端渲染应用程序，需要处于 Node.js server 运行环境，更多的服务器负载

**预渲染：**【插件prerender-spa-plugin  puppeteer】解决SEO首屏问题 , 页面较少，且预渲染相对于SSR比较简单，预渲染可以极大的提高网页访问速度。而且配合一些meat插件，完全可以满足SEO需求。Prerender服务需要有NodeJs环境支持，如果之前服务器环境没有NodeJs需要先进行安装

```javascript
$ npm install prerender-spa-plugin -S
// build/webpack.prod.conf.js 或 vue.config.js
const PrerenderSPAPlugin = require('prerender-spa-plugin'); 
// 预渲染
const Renderer= PrerenderSPAPlugin.PuppeteerRenderer
// plugins中配置PrerenderSPAPlugin
new PrerenderSPAPlugin({
  staticDir: path.join(__dirname, '../dist'),
  // 对应自己的路由文件，比如index有参数，就需要写成 /index/param1。
  routes: ['/'], // 因为该系统操作都是基于登录后的，所以只做登录页面的预渲染就行了
  renderer: new Renderer({
    inject: {
      foo: 'bar',
    },
    headless: false,
    // 在 main.js 中 document.dispatchEvent(new Event('render-event'))，两者的事件名称要对应上。
    renderAfterDocumentEvent: 'render-event',
  }),
})

// main.js vue 实例完成后执行
document.dispatchEvent(new Event('render-event')); 
```

```
# ect/nginx/nginx.conf 【如果路由配置的是history模式】
server {
        listen       8080; # 监听8080端口
        root         /www/vue-base-demo/dist/; // 静态资源目录
        index        index.html; // 首页
        location / {
                try_files $uri $uri/ /index.html; # 当找不到文件404的时候，转发的index.html上
        }
}
```

- 事件的销毁

```javascript
created() {
  addEventListener('click', this.click, false)
},
beforeDestroy() {
  removeEventListener('click', this.click, false)
}
```

- 无状态的组件标记为函数式组件

```javascript
<template functional> 
  <div class="box"> 
    <p>{{value}}</p>
  </div> 
</template> 

<script> 
export default { 
  props: ['value'] 
} 
</script> 
```

- 子组件分割 【按需按功能拆分更小颗粒度的组件】
- 防抖、节流的运用

### 9、你对Vue3.0的新特性有没有了解？ 

- 虚拟DOM重写 
- 优化slots的生成 
- 静态树提升 
- 静态属性提升 
- 基于Proxy的响应式系统 
- 通过摇树优化核心库体积 
- 更容易维护：TypeScript + 模块化 
- 更加友好 
- 跨平台：编译器核心和运行时核心与平台无关，使得Vue更容易与任何平台（Web、 Android、iOS）一起使用 
- 更容易使用 ：改进的TypeScript支持，编辑器能提供强有力的类型检查和错误及警告 
- 更好的调试支持 
- 独立的响应化模块 
- Composition API 

### 10、vue中组件之间的通信方式？

常见使用场景可以分为三类： 
父子组件通信 
兄弟组件通信 
跨层组件通信 

- props和$emit(常用)
- $attrs和$listeners
- eventBus（非父子组件间通信）new Vue()
- v-model  sync  都是语法糖 当子组件想要修改父组件数据时，只用使用 `this.$emit('update:propsName', dataValue)`这种方式去通知父组件，父组件上不需要做任何操作
- provide和inject
- $parent和$children
- vuex
- $refs

**Vue 的单项数据流**

数据总是从父组件传到子组件，**子组件没有权利修改父组件传过来的数据**，只能请求父组件对原始数据进行修改。这样会防止从子组件意外改变父组件的状态，从而导致你的应用的数据流向难以理解

**注意：**在子组件直接用 v-model 绑定父组件传过来的 props 这样是不规范的写法，开发环境会报警告。

如果实在要改变父组件的 props 值可以再data里面定义一个变量，并用 prop 的值初始化它，之后用$emit 通知父组件去修改

### 11、vue-router 中的导航钩子由那些？ 

- 全局的钩子函数 

router.beforeEach(to，from，next) 路由改变前调用

to ：即将要进入的目标路由对象 

from：当前正要离开的路由对象 

next：路由控制参数

router.afterEach (to，from) 路由改变后的钩子 

router.onError() 

- 路由配置中的导航钩子 

beforeEnter (to，from，next) 

- 组件内的钩子函数

beforeRouteEnter(to,from,next)  不能获取实例（this） 

beforeRouteUpdate(to,from,next)  可以获取组件实例（this） 

beforeRouteLeave(to,from,next)  可以获取组件实例（this）

watch 路由监测变化  '$route' (to,from) 

```javascript
watch: {
  propsValue: {
    handler: 'function/eventName/callback',
    deep: true, // 深度监听
    immediate: true // 初始化执行
  }
}
```

### 12、说一说vue响应式理解？ 

```kotlin
Vue.util.defineReactive  // 给一数据加响应式类似vue3 ref
```

响应式实现：数据劫持 + 观察者模式

1、Object.defineProperty 数据劫持
2、使用 getter 收集依赖 ，setter 通知 watcher派发更新。
3、watcher 发布订阅模式

- vue2：object.defineProperty 

**对象响应化**：递归遍历每个key，使用Object.defineproperty方法定义getter、setter
**数组响应化**：采用函数拦截方式，覆盖数组原型方法，额外增加通知逻辑

['push', 'pop', 'shift','unshift','splice','reverse','sort'] 引起副作用（改变原数组）拷贝一份数组原型方法 （AOP 切片思想）Object.create(Array.prototype)

因为object.defineProperty 没办法监听数组长度的变化

在生成vue实例时，为对传入的data进行遍历，使用`Object.defineProperty`把这些属性转为`getter/setter`

每个vue实例都有一个watcher实例，它会在实例渲染时记录这些属性，并在setter触发时重新渲染

Vue 无法检测到对象属性的添加或删除

Vue 不允许动态添加根级别的响应式属性。但是，可以使用 `Vue.set(object, propertyName, value)`方法向嵌套对象添加响应式属性

- vue3：proxy (兼容性不太好) 

effect：将回调函数保存起来备用，立即执行一次回调函数触发它里面一些响应数据的getter
track（依赖收集）：getter中调用track，把前面存储的回调函数和当前target,key之间建立映射关系
trigger（派发更新）：setter中调用trigger，把target,key对应的响应函数都执行一遍

![image-20210921151936046](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210921151936046.png)

### 13、vue为什么要求组件模版只能有一个根元素? 

- new Vue({el:'#app'}) 

- 单文件组件中，template下的元素div。其实就是"树"状数据结构中的"根"。 
- diff算法要求的，源码中，patch.js里patchVnode()

Vue其实并不知道哪一个才是我们的入口。如果同时设置了多个入口，那么vue就不知道哪一个才是这 个‘类。

### 14、你知道nextTick的原理吗? 

- vue用异步队列的方式来控制DOM更新和nextTick回调先后执行 
- microtask因为其高优先级特性，能确保队列中的微任务在一次事件循环前被执行完毕 
- 因为兼容性问题，vue不得不做了microtask【微任务】向macrotask【宏任务】的降级方案

监听DOM改动的API：MutationObserver 【HTML5新增的属性】

浏览器事件循环 【Event Loop】

**两个原则：**同一时间只能执行一个任务、任务一直执行到完成，不能被其他任务抢断

![image-20210920135626901](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210920135626901.png)

vue2.5的源码中，macrotask降级的方案依次是：setImmediate、MessageChannel、setTimeout. setImmediate是最理想的方案了，可惜的是只有IE和nodejs支持

**事件循环：**JS引擎遇到一个异步事件后并不会一直等待其返回结果，而是会将这个事件挂起，继续执行执行栈中的其他任务。当一个异步事件返回结果后，js会将这个事件加入与当前执行栈不同的另一个队列，我们称之为事件队列。被放入事件队列不会立刻执行其回调，而是等待当前执行栈中的所有任务都执行完毕， 主线程处于闲置状态时，主线程会去查找事件队列是否有任务。如果有，那么主线程会从中取出排在第一位的事件，并把这个事件对应的回调放入执行栈中，然后执行其中的同步代码...，如此反复，这样就形成了一个无限的循环。

**宏任务：**

- script(整体代码)
- setTimeout()
- setInterval()
- postMessage
- I/O
- UI交互事件

**微任务:**

- new Promise().then(回调)
- MutationObserver(html5 新特性)
- nodejs中的 process.nextTick.、setImmediate

**同步任务**：在主线程上排队执行的任务，只有前一个任务执行完毕，才能执行后一个任务

**异步任务**：不进入主线程、而进入"任务队列"（task queue）的任务，只有"任务队列"通知主线程，某个异步任务可以执行了，该任务才会进入主线程执行

**任务队列**："任务队列" 是一个事件的队列（也可以理解成消息的队列），IO设备完成一项任务，就在"任务队列"中添加一个事件，表示相关的异步任务可以进入"执行栈"了。主线程读取"任务队列"，就是读取里面有哪些事件，"任务队列"是一个先进先出的数据结构，排在前面的事件，优先被主线程读取。主线程的读取过程基本上是自动的，只要执行栈一清空，"任务队列"上第一位的事件就自动进入主线程。但是，由于存在后文提到的"定时器"功能，主线程首先要检查一下执行时间，某些事件只有到了规定的时间，才能返回主线程

**执行栈**：**堆（heap）**对象被分配在一个堆中，即用以表示一个大部分非结构化的内存区域。**执行栈（stack）**运行同步代码。执行栈中的代码（同步任务），总是在读取"任务队列"（异步任务）之前执行

**进程**：运行的程序就是一个进程，比如你正在运行的浏览器，它会有一个进程

**线程**：程序中独立运行的代码段。**一个进程** 由单个或多个 **线程** 组成，线程是负责执行代码的

**单线程：** 从头执行到尾，一行一行执行，如果其中一行代码报错，那么剩下代码将不再执行。同时容易代码阻塞。

**多线程：** 代码运行的环境不同，各线程独立，互不影响，避免阻塞

**JS的执行机制** ：【以下三步循环执行,就是Event Loop】

- 首先判断JS是同步还是异步,同步就进入主线程,异步就进入event table
- 异步任务在event table中注册函数,当满足触发条件后,被推入event queue
- 同步任务进入主线程后一直执行,直到主线程空闲时,才会去event queue中查看是否有可执行的异步任务,如果有就推入主线程中

**总结：**Javascript是单线程的，所有的同步任务都会在主线程中执行异步执行的运行机制如下（同步执行也是如此，因为它可以被视为没有异步任务的异步执行）：
（1）所有同步任务都在主线程上执行，形成一个"执行栈"
（2）主线程之外，还存在一个"任务队列"（task queue）。只要异步任务有了运行结果，就在"任务队列"之中放置一个事件；
（3）一旦"执行栈"中的所有同步任务执行完毕，系统就会取出"任务队列"中事件所对应的回调函数进入"执行栈"，开始执行；
（4）主线程不断重复上面的第三步

### 15、说说vue中的几种solt使用？

 插槽，其实就相当于占位符。它在组件中给你的HTML模板占了一个位置，让你来传入一些东西。

**匿名插槽**

```javascript
<slot></slot>
```

**具名插槽**

```javascript
<slot name="xxx"></slot>
```

**作用域插槽**

```javascript
<slot :propsName="propsValue"></slot>
```

### 16、vue常用的修饰符

.stop：等同于JavaScript中的 event.stopPropagation()，防止事件冒泡；

.prevent：等同于JavaScript中的event.preventDefault()，防止执行预设的行为（如果事件可取消，则取消该事件，而不停止事件的进一步传播）；

.capture：与事件冒泡的方向相反，事件捕获由外到内；

.self：只会触发自己范围内的事件，不包含子元素；

.once：只会触发一次

.lazy：输入框改变，这个数据就会改变，lazy这个修饰符会在光标离开input框才会更新数据

.trim：输入框过滤首尾的空格

.number：先输入数字就会限制输入只能是数字，先字符串就相当于没有加number，注意，不是输入框不能输入字符串，是这个数据是数字

.sync：对prop进行双向绑定

.keyCode：监听按键的指令，具体可以查看vue的键码对应表

### 17、Vue里面router-link在电脑上有用，在安卓上没反应怎么解决？

Vue路由在Android机上有问题，babel问题，安装babel polypill插件解决。

### 18、Vue2中注册在router-link上事件无效解决方法

使用@click.native。原因：router-link会阻止click事件，.native指直接监听一个原生事件。

### 19、router-link在IE和Firefox中不起作用（路由不跳转）的问题

方法一：只用a标签，不适用button标签；

方法二：使用button标签和Router.navigate方法

### 20、vue初始化页面闪动问题

使用vue开发时，在vue初始化之前，由于div是不归vue管的，所以我们写的代码在还没有解析的情况下会容易出现花屏现象，看到类似于{{message}}的字样，虽然一般情况下这个时间很短暂，但是我们还是有必要让解决这个问题的。首先：在css里加上

```css
[v-cloak] {
	display: none;
}
```

如果没有彻底解决问题，则在根元素加上style="display: none;" :style="{display: 'block'}"

### 21、Vue事件绑定原理

**原生事件绑定是通过 addEventListener 绑定给真实元素的**，**组件事件绑定是通过Vue自定义的$on实现的**。如果要在组件上使用原生事件，需要加.native修饰符，这样就相当于在父组件中把子组件当做普通的HTML标签，然后加上原生事件。
$on、$emit 是基于发布订阅模式的，维护一个事件中心，on的时候将事件按名称存在事件中心里，称之为订阅者，然后emit将对应的事件进行发布，去执行事件中心里的对应的监听器

### 22、谈一下对 vuex 的个人理解

![image-20210921214104370](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210921214104370.png)

<u>**vuex中核心概念**</u> 

**state：**vuex的唯一数据源，如果获取多个state,可以使用...mapState

**getter:** 可以将getter理解为计算属性，getter的返回值根据他的依赖缓存起来，依赖发生变 化才会被重新计算

**mutation：**更改vuex的state中唯一的方法是提交mutation都有一个字符串和一个回调函数。 回调函数就是使劲进行状态修改的地方。并且会接收state作为第一个参数payload为第二个参 数，payload为自定义函数，mutation必须是同步函数

**action：**action类似mutation都是修改状态，不同之处,  action提交的mutation不是直接修改状态 ,action可以包含异步操作，而mutation不行 ,action中的回调函数第一个参数是context，是一个与store实例具有相同属性的方法的 ,action通过store.dispatch触发，mutation通过store.commit提交

**module：**由于是使用单一状态树，应用的所有状态集中到比较大的对象，当应用变得非常复杂 ，store对象就有可能变得相当臃肿。为了解决以上问题，vuex允许我们将store分割成模块，每个模块拥有自己的state,mutation,action,getter,甚至是嵌套子模块从上至下进行同样方式分割。

**Vuex 页面刷新数据丢失解决**

需要做 vuex 数据持久化，一般使用本地储存的方案来保存数据，可以自己设计存储方案，也可以使用第三方插件。推荐使用 vuex-persist插件，它是为 Vuex 持久化储存而生的一个插件。不需要你手动存取 storage，而是直接将状态保存至 cookie 或者 localStorage中

**命名空间**

如果希望你的模块具有更高的封装度和复用性，你可以通过添加 namespaced:true 的方式使其成为带命名的模块

### 23、Vue 模板编译原理

Vue 的编译过程就是将 template 转化为 render 函数的过程，分为以下三步：
第一步 是将 模板字符串转换成 element ASTS（解析器）
第二步 是对 AST 进行静态节点标记，主要用来做虚拟 DOM 的渲染优化（优化器）
第三步 是 使用element ASTS生成 render 函数代码字符串（代码生成器）

### 24、谈谈vue生命周期（vue2、vue3）

|     Vue2      |      vue3       |
| :-----------: | :-------------: |
| beforeCreate  |      setup      |
|    created    |      setup      |
|  beforeMount  |  onBeforeMount  |
|    mounted    |    onMounted    |
| beforeUpdate  | onBeforeUpdate  |
|    updated    |    onUpdated    |
| beforeDestroy | onBeforeUnmount |
|   destroyed   |   onUnmounted   |
|   activated   |   onActivated   |
|  deactivated  |  onDeactivated  |
| errorCaptured | onErrorCaptured |

第一次页面加载会触发beforeCreate， created， beforeMount， mounted钩子，获取数据一般在在created，beforeMount，mounted 周期函数

created:在模板渲染成html前调用，即通常初始化某些属性值，然后再渲染成视图。
mounted:在模板渲染成html后调用，通常是初始化页面完成后，再对html的dom节点进行一些需要的操作。

**Vue的父子组件生命周期钩子函数执行顺序**

- 加载渲染过程
- 父beforeCreate -> 父created -> 父beforeMount -> 子beforeCreate -> 子created -> 子beforeMount -> 子mounted -> 父mounted

- 子组件更新过程
- 父beforeUpdate -> 子beforeUpdate -> 子updated -> 父updated

- 父组件更新过程
- 父beforeUpdate -> 父updated

- 销毁过程
- 父beforeDestroy -> 子beforeDestroy -> 子destroyed -> 父destroyed

### 25、v-model的实现原理和自定义组件v-model

v-model其实是个语法糖

1、绑定数据value
2、触发输入事件input并传递数据 (核心和重点)

核心就是，一方面modal层通过defineProperty来劫持每个属性，一旦监听到变化通过相关的页面元素更新。另一方面通过编译模板文件，为控件的v-model绑定input事件，从而页面输入能实时更新相关data属性值

```javascript
// 自定义组件v-model 父组件
<template>
  <div id="app">
    <p>{{ name }}</p>
    <!--自定义组件上使用 v-model，name 的值会传入到子组件 model-prop-text 中-->
    <Child v-model="name" />
  </div>
</template>
<script>
import Child from './components/Child'
export default {
  name: "App",
  components: { Child },
  data() {
    return {
      name: 'vue双向绑定v-model实现'
    }
  },
};
</script>

// 子组件 Child.vue
<template>
  <input
    type="text"
    :value="text"
    @input="$emit('change', $event.target.value)"
  />
</template>
<script>
export default {
  model: {
    prop: 'text', // 可任意定义的变量，用来接收父组件 v-model 中传递过来的值
    event: 'change'
  },
  props: {
    text: String, // 对自己定义的变量的类型和默认值进行声明，必须和自己定义的变量名相同
    default() {
      return ''
    }
  }
}
</script>
```

### 26、Vue-router 中hash模式和history模式的区别

**hash模式**

很丑的 hash 带  # 号

hash模式其实是利用了window.onhashchange事件

支持低版本浏览器和IE浏览器

hash模式下，build之后本地 index.html 打开正常！

**history模式**

不带  # 号

HTML5的新推出 的 API

利用history.pushState() 和 history.replaceState()事件

history.back() 等价于 history.go(-1) 【相当于后退】

history.forward() 则等价于 history.go(1) 【相当于前进】

history模式下，build之后本地 index.html 打开是无效的。

```nginx
### history模式需要 借助 ng 处理 或者 后台对应的配置
location / {
  try_files $uri $uri/ /index.html;
}
```

