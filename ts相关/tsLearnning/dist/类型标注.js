/*
 * @Description: vscode自带注释
 * @Version: 2.0
 * @Autor: lhl
 * @Date: 2020-11-07 14:55:15
 * @LastEditors: lhl
 * @LastEditTime: 2020-11-07 19:30:11
 */
var a;
a = 'hello,ts';
function add(a, b) {
    return a + b;
}
add(1, 2);
// 1.基础类型 string number 基类
var tsStr = '字符串类型';
var tsNum = 5;
var tsBoolean = false;
// 对象标注
var obj = {
    x: 1,
    y: 2
};
// 数组类型
var arr = [];
arr.push('只能push对应类型的值'); //正确
// arr.push(1) 错误
var arr2 = [];
arr2.push(1);
// 元祖类型 源目标个数限定 不能push或者操作不是定义的类型的
var arr3 = [1, 'ss'];
// arr3.push(false) 报错
// 数字类型枚举
var HTTP_CODE;
(function (HTTP_CODE) {
    HTTP_CODE[HTTP_CODE["OK"] = 200] = "OK";
    HTTP_CODE[HTTP_CODE["NOT_FOUN"] = 404] = "NOT_FOUN";
    HTTP_CODE[HTTP_CODE["NOT_LOGIN"] = 401] = "NOT_LOGIN";
})(HTTP_CODE || (HTTP_CODE = {}));
console.log(HTTP_CODE.OK);
// 无值类型 执行了return 但是没有具体的值返回
function fn() {
}
// Never类型 不会执行return
function fin() {
    throw new Error('Error Message');
}
var aa;
a = fin();
// 任何类型 不推荐实在不知道时候标注
var bb;
bb = '1';
bb = 1;
// ts3.0 unknow类型  any的安全模式
// unknow只能赋给unknow或者 any
// 函数类型 手写一个 forEach
function forEach(data, callback) {
    for (var i = 0; i < data.length; i++) {
        callback(i, data[i]);
    }
}
var testArr = [1, 2];
forEach(testArr, function (key, value) {
    console.log(key, value);
});
