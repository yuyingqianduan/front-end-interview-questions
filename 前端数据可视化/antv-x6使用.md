# antv-x6使用

官方文档：https://x6.antv.vision/zh/docs/tutorial/getting-started

安装： npm install @antv/x6 --save       或者   yarn add @antv/x6

### 画布基础使用

```javascript
import React, { Component } from "react";

import { Graph } from "@antv/x6";

class AntvView extends Component {
  componentDidMount() {
    // 第二步 准备数据
    const data = {
      // 节点
      nodes: [
        {
          id: "node1", // String，可选，节点的唯一标识
          x: 40, // Number，必选，节点位置的 x 值
          y: 40, // Number，必选，节点位置的 y 值
          width: 80, // Number，可选，节点大小的 width 值
          height: 40, // Number，可选，节点大小的 height 值
          label: "hello", // String，节点标签
        },
        {
          id: "node2", // String，节点的唯一标识
          x: 160, // Number，必选，节点位置的 x 值
          y: 180, // Number，必选，节点位置的 y 值
          width: 80, // Number，可选，节点大小的 width 值
          height: 40, // Number，可选，节点大小的 height 值
          label: "world", // String，节点标签
        },
      ],
      // 边
      edges: [
        {
          source: "node1", // String，必须，起始节点 id
          target: "node2", // String，必须，目标节点 id
        },
      ],
    };
	
     // 第三步 渲染画布
    const graph = new Graph({
      container: document.getElementById("container"),
      width: 800,
      height: 600,
    });

    // 渲染节点和边
    graph.fromJSON(data);
  }
  render() {
    return (
      <div>
        // 第一步 创建容器
        <div id="container"></div>
      </div>
    );
  }
}
export default AntvView;
```

### Grid网格 【画布的options（配置项）】

```javascript
getGridSize() // 获取网格大小
setGridSize() // 设置网格大小
showGrid() // 显示网格
hideGrid() // 隐藏网格
clearGrid() // 清除网格
drawGrid(opts) // 重绘网格
```

### Background 背景【画布的options（配置项）】

背景用于为画布指定背景颜色或背景图片，支持[水印背景](https://x6.antv.vision/zh/docs/api/graph/background#repeat)和[自定义背景图片的重复方式](https://x6.antv.vision/zh/docs/api/registry/background#registry)，背景层在 DOM 层级上位于画布的最底层。

```javascript
const graph = new Graph({
  background: false | BackgroundOptions
})

可以调用 graph.drawBackground(options?: BackgroundOptions) 方法来重绘背景

color // 背景颜色
支持所有 CSS background-color 属性的取值
image // 背景图片的 URL 地址
默认值为 undefined，表示没有背景图片
                          
position //  背景图片位置
'支持所有 CSS background-position 属性的取值'，默认为 'center'
                          
size // 背景图片大小
'支持所有 CSS background-size 属性的取值'，默认为: 'auto auto'   
                          
repeat // 背景图片重复方式
'watermark' 水印效果。
'flip-x' 水平翻转背景图片。
'flip-y' 垂直翻转背景图片。
'flip-xy' 水平和垂直翻转背景图片。    
                          
opacity // 背景透明度，取值范围 [0, 1]，默认值为 1。                   
quality // 背景图片质量，取值范围 [0, 1]，默认值为 1                     
angle // 水印旋转角度，仅当 repeat 为 'watermark' 时有效，默认值为 20                  
```

### Snapline 对齐线【画布的options（配置项）】

```javascript
const graph = new Graph({
  snapline: true,
})

// 等同于
const graph = new Graph({
  snapline: {
    enabled: true,
  },
})

// isSnaplineEnabled()返回是否启用对齐线
if (graph.isSnaplineEnabled()) {
  // disableSnapline() 禁用对齐线
  graph.disableSnapline()
} else {
  // enableSnapline() 启用对齐线
  graph.enableSnapline()
}

hideSnapline() // 隐藏对齐线
isSnaplineOnResizingEnabled() // 调整节点大小时，是否触发对齐线
enableSnaplineOnResizing() // 启用调整节点大小过程中触发对齐线
disableSnaplineOnResizing() // 禁用调整节点大小过程中触发对齐线
```

### Scroller 画布具备滚动、平移、居中、缩放等能力【画布的options（配置项）】

```javascript
const graph = new Graph({
  scroller: true,
})

// 等同于
const graph = new Graph({
  scroller: {
    enabled: true,
  },
})

width // Scroller 的宽度，默认为画布容器宽度。
height // Scroller 的高度，默认为画布容器高度。
pannable // 是否启用画布平移能力（在空白位置按下鼠标后拖动平移画布），默认为 false

// 创建画布后，也可以调用 graph.enablePanning() 和 graph.disablePanning() 来启用和禁用画布平移。
if (graph.isPannable()) {
  graph.disablePanning()
} else {
  graph.enablePanning()
}

padding // 设置画布四周的 padding 边距。默认根据 minVisibleWidth 和 minVisibleHeight 自动计算得到
minVisibleWidth // 当 padding 为空时有效，设置画布滚动时画布的最小可见宽度。
minVisibleHeight // 当 padding 为空时有效，设置画布滚动时画布的最小可见高度。
pageVisible // 是否分页，默认为 false。
pageBreak // 是否显示分页符，默认为 false。
pageWidth // 每一页的宽度，默认为画布容器宽度.
pageHeight // 每一页的高度，默认为画布容器高度。
autoResize // 是否自动扩充/缩小画布，默认为 true。开启后，移动节点/边时将自动计算需要的画布大小，当超出当前画布大小时，按照 pageWidth 和 pageHeight 自动扩充画布。反之，则自动缩小画布
autoResizeOptions {opt} // 自动扩展画布的选项
// autoResizeOptions 支持的选项
interface {
  minWidth?: number  // 画布最小宽度
  minHeight?: number // 画布最小高度
  maxWidth?: number  // 画布最大宽度
  maxHeight?: number // 画布最大高度
  border?: number    // 距离画布边缘多少位置时触发自动扩展画布，例如设置为 `20` 表示当节点移动到距离画布边缘 `20px` 内时触发自动扩展画布。
}
scrollToPoint(opt) // 将 x 和 y 指定的点（相对于画布）滚动到视口中心，如果只指定了其中一个方向，则只滚动对应的方向 滚动后指定的点不一定位于视口中心，如指定的点位于画布的角落
```

### MiniMap 小地图【画布的options（配置项）】

启用 [Scroller](https://x6.antv.vision/zh/docs/api/graph/scroller) 后可开启小地图，小地图是完整画布的预览，支持通过平移缩放小地图的视口来平移缩放画布 scroller  和 minimap 结合使用 

```javascript
const graph = new Graph({
  scroller: {
    enabled: true,
  },
  minimap: {
    enabled: true,
    container: minimapContainer, // minimapContainer 指定渲染小地图容器
  }
})
width // 小地图的宽度，默认为 300。
height // 小地图的高度，默认为 200
padding // 小地图容器的 padding 边距，默认为 10
scalable // 是否可缩放，默认为 true。
minScale // 最小缩放比例，默认为 0.01
maxScale // 最大缩放比例，默认为 16
graphOptions {opt} // 创建小地图 Graph 的选项，默认为 null
```

### History 撤销/重做 【画布的options（配置项）】

撤销/重做，默认禁用。创建画布时，通过以下配置开启画布撤销/重做能力。

```javascript
const graph = new Graph({
  history: true,
})

// 等同于
const graph = new Graph({
  history: {
    enabled: true,
  },
})

// 是否重做
if (graph.isHistoryEnabled()) {
  // 启用历史状态
  graph.disableHistory()
} else {
  // 禁用历史状态
  graph.enableHistory()
}

undo(opt) // 撤销。options 将被传递到事件回调中
undoAndCancel(opt) // 撤销，并且不添加到重做队列中，所以这个被撤销的命令不能被重做。options 将被传递到事件回调中
redo(opt) // 重做。options 将被传递到事件回调中。
canUndo() // 是否可以撤销
canRedo() // 是否可以重做
cleanHistory(opt) // 清空历史状态。options 将被传递到事件回调中
isHistoryEnabled() // 是否启用了历史状态
```

### Clipboard 剪切板【画布的options（配置项）】

```javascript
const graph = new Graph({
  clipboard: true,
    
})

// 等同于
const graph = new Graph({
  clipboard: {
    enabled: true,
    useLocalStorage: true, // 开启 useLocalStorage 后，被复制的节点/边同时被保存到 localStorage 中，浏览器刷新或者关闭后重新打开，复制/粘贴也能正常工作
  }
})

if (graph.isClipboardEnabled()) {
  	// 启用剪贴板
    graph.disableClipboard()
} else {
  	// 禁用剪贴板
    graph.enableClipboard()
}
// 复制节点/边。
graph.copy(cells: Cell[], options?: CopyOptions)
// 剪切节点/边。
graph.cut(cells: Cell[], options?: CopyOptions)
// 粘贴，返回粘贴到画布的节点/边。
graph.paste(options?: PasteOptions, targetGraph?: Graph)
```

### Keyboard 键盘快捷键【画布的options（配置项）】

```javascript
const graph = new Graph({
  keyboard: true,
})

// 等同于
const graph = new Graph({
  keyboard: {
    enabled: true,
  },
})

if (graph.isKeyboardEnabled()) {
    // 禁用键盘事件。
    graph.disableKeyboard()
} else {
   // 启用键盘事件。
   graph.enableKeyboard()
}

// 绑定键盘快捷键
bindKey(
  keys: string | string[], 
  callback: (e: KeyboardEvent) => void, 
  action?: 'keypress' | 'keydown' | 'keyup',
): this

// 解绑键盘快捷键
unbindKey(
  keys: string | string[], 
  action?: 'keypress' | 'keydown' | 'keyup',
): this
```

### connecting【画布的options（配置项）】

配置全局的连线规则

```javascript
const graph = new Graph({
  connecting: {
    snap: true,
  }
})

// 等价于
const graph = new Graph({
  connecting: {
    snap: {
      radius: 50,
    },
  }
})

allowBlank // 是否允许连接到画布空白位置的点，默认为 true
allowMulti // 是否允许在相同的起始节点和终止之间创建多条边，默认为 true。当设置为 false 时，在起始和终止节点之间只允许创建一条边，当设置为 'withPort' 时，在起始和终止节点的相同链接桩之间只允许创建一条边（即，起始和终止节点之间可以创建多条边，但必须要要链接在不同的链接桩上）
allowLoop // 是否允许创建循环连线，即边的起始节点和终止节点为同一节点，默认为 true。
allowNode // 是否允许边链接到节点（非节点上的链接桩），默认为 true。
allowEdge // 是否允许边链接到另一个边，默认为 true
allowPort // 是否允许边链接到链接桩，默认为 true。
highlight // 拖动边时，是否高亮显示所有可用的连接桩或节点，默认值为 false。
connectionPoint // 指定连接点，默认值为 boundary。
sourceConnectionPoint // 连接源的连接点。
targetConnectionPoint // 连接目标的连接点。
```

