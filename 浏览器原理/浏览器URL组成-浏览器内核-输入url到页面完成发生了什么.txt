【浏览器内核】
1、webkit(v8引擎)：chrome    opera    safari   国产浏览器和手机端
2、gecko: firefox(火狐浏览器)
3、trident: IE浏览器（IE的最新版本是edge）

【关于URL的几个重要概念】
一个完整的URL包括协议【http/https】、服务器地址(主机)【www.xxx.com/cn】、端口【8080】、路径【/xx】；
例子：http://www.baidu.com:8080/user/search.html?a=1&b=2#results
hash：URL的锚部分，包括开头的哈希（#）号，例如："#results"。
host：URL的主机端口部分，例如："http://www.baidu.com:8080";
hostname：URL的主机名部分，例如："http://www.baidu.com";
href：文档的URL的完整文本，不同于其他的指定URL的一部分Location属性。将这个属性设置为一个新的URL将导致浏览器读取并显示心得URL内容。把这个值直接赋值给Location对象将设置这个属性，把一个Location对象用作一个字符串将使用这个属性的值。
pathname：URL的路径名部分，例如"/user/search.html"; 
port：URL的端口部分，例如"8080";
protocol：URL协议部分，包括尾部的冒号，例如"http:";
search：URL的查询部分，包括开头的问号，例如"?a=1&b=2"。

【输入url到页面完成发生了什么】
（1）DNS解析：将域名解析为ip地址
（2）建立TCP连接： 客户端浏览器与WEB服务器建立TCP(传输控制协议)连接，三次握手【浏览器-->服务器，服务器-->浏览器，浏览器-->服务器】
（3）发送请求：请求报文 http协议的通信内容
（4）响应请求：响应报文
（5）渲染页面：dom树、css树、合并渲染树计算布局 绘制；
（6）断开连接【非持久性】： TCP四次挥手。【浏览器-->服务器，服务器-->浏览器，服务器-->浏览器，浏览器-->服务器】
http对应于应用层，Tcp协议对应于传输层，http协议是在Tcp协议之上建立的；
