# web安全

### 浏览器解析顺序

html parser  --->  css parser --->  js parser

### 浏览器解码顺序

html Decoding---> URL  Decoding --->  js Decoding

### session与cookie的区别

1. cookie的数据保存在客户端浏览器，session保存在服务器
2. 服务端保存状态机制需要在客户端做标记，所以session可能借助cookie机制
3. cookie通常用于客户端保存用户的登录状态

# xss概念

XSS原称为CSS(Cross-Site Scripting)，因为和层叠样式表(Cascading Style Sheets)重名，所以改称为XSS(X一般有未知的含义，还有扩展的含义)。XSS攻击涉及到三方：攻击者，用户，web server。用户是通过浏览器来访问web server上的网页，XSS攻击就是攻击者通过各种办法，在用户访问的网页中插入自己的脚本，让其在用户访问网页时在其浏览器中进行执行。攻击者通过插入的脚本的执行，来获得用户的信息，比如cookie，发送到攻击者自己的网站(跨站了)。所以称为**跨站脚本攻击**。XSS可以分为反射型XSS和持久性XSS，还有DOM Based XSS。(一句话，XSS就是在用户的浏览器中执行攻击者自己定制的脚本。)

# xss 分类

### 存储型（持久型）

XSS恶意代码**存储在web server中**，这样，每一个访问特定网页的用户，都会被攻击。

留言板功能：

![image-20210714223030023](C:\Users\Administrator.WIN-MHB8ELOJK80\AppData\Roaming\Typora\typora-user-images\image-20210714223030023.png)

### 反射型

内容直接读取并且反射展示在页面上

如：get请求的参数

### DOM型

可以当做反射型的一种

地址栏：location.hash     document.write('xxx')

### 其他xss类型

1. mXSS（突变型xss）
2. UXSS（通用型xss）
3. Flash XSS
4. UTF-7 XSS
5. MHTML XSS
6. CSS XSS
7. VBScript XSS

### xss 发生的原因

对用户输入没有严格控制而直接输出到页面

对非预期输入的信任

### xss的危害

盗取各类用户账号

窃取数据

非法转账

挂马（恶意代码）....

### xss防范

xss盲打：盲打只是一种惯称的说法，就是不知道后台不知道有没有xss存在的情况下，不顾一切的输入xss代码在留言啊，feedback啊之类的地方，尽可能多的尝试xss的语句与语句的存在方式，就叫盲打（测试xss）

XSS蠕虫：是指一种具有自我传播能力的XSS攻击，杀伤力很大。引发XSS蠕虫的条件比较高，需要在用户之间发生交互行为的页面，这样才能形成有效的传播。一般要同时结合反射型XSS和存储型XSS

### XSS防御的总体思路

1、对输入(和URL参数)进行过滤，对输出进行编码

也就是对提交的所有内容进行过滤，对url中的参数进行过滤，过滤掉会导致脚本执行的相关内容；然后对动态输出到页面的内容进行html编码，使脚本无法在浏览器中执行。虽然对输入过滤可以被绕过，但是也还是会拦截很大一部分的XSS攻击。

2、对输入和URL参数进行过滤(白名单和黑名单)

3、对输出进行编码

在输出数据之前对潜在的威胁的字符进行编码、转义是防御XSS攻击十分有效的措施。如果使用好的话，理论上是可以防御住所有的XSS攻击的。

对所有要动态输出到页面的内容，通通进行相关的编码和转义。当然转义是按照其输出的上下文环境来决定如何转义的

# CSRF

CSRF（Cross-site request forgery），中文名称：跨站请求伪造；

HTML CSRF：通过html元素发起CSRF请求

JSON HiJacking：构造自定义的回调函数

Flash CSRF：通过flash来实现跨域请求

![image-20210718145126332](C:\Users\Administrator.WIN-MHB8ELOJK80\AppData\Roaming\Typora\typora-user-images\image-20210718145126332.png)

### CSRF 与 xss 的区别

XSS： 利用对用户输入的不严谨然后执行js语句

CSRF：通过伪造受信任用户发送请求

### CSRF可以做什么

攻击者盗用了你的身份，以你的名义发送恶意请求。CSRF能够做的事情包括：以你名义发送邮件，发消息，盗取你的账号，甚至于购买商品，虚拟货币转账......造成的问题包括：个人隐私泄露以及财产安全

### CSRF的防御

1、Cookie Hashing(所有表单都包含同一个伪随机值)  在表单里增加Hash值，以认证这确实是用户发送的请求 --->  然后在服务器端进行Hash值验证

现在一般的CSRF防御也都在服务端进行！！！

2、One-Time Tokens(不同的表单包含一个不同的伪随机值)

3、总结：通过 referer【验证HTTP Referer】、token 【通过自定义token】或者 验证码 来检测用户提交。尽量不要在页面的链接中暴露用户隐私信息。对于用户修改删除等操作最好都使用post 操作 。避免全站通用的cookie，严格设置cookie的域！

# 点击劫持

### 概念

点击劫持是一种视觉欺骗的攻击手段。攻击者将需要攻击的网站通过 iframe 嵌套的方式嵌入自己的网页中，并将 iframe 设置为透明，在页面中透出一个按钮诱导用户点击。

### 如何防御

X-FRAME-OPTIONS：X-FRAME-OPTIONS是一个 HTTP 响应头，在现代浏览器有一个很好的支持。这个 HTTP 响应头 就是为了防御用 iframe 嵌套的点击劫持攻击

### JavaScript 防御

```javascript
<head>
  <style id="click-event">
    html {
      display: none !important;
    }
  </style>
</head>
<body>
  <script>
    if (self == top) {
      var style = document.getElementById('click-event')
      document.body.removeChild(style)
    } else {
      top.location = self.location
    }
  </script>
</body>
```

当通过 iframe 的方式加载页面时，攻击者的网页直接不显示所有内容了

# SQL注入

### 概念

SQL注入是一种常见的Web安全漏洞，攻击者利用这个漏洞，可以访问或修改数据，或者利用潜在的数据库漏洞进行攻击。

### 如何防御

- **严格限制Web应用的数据库的操作权限**，给此用户提供仅仅能够满足其工作的最低权限，从而最大限度的减少注入攻击对数据库的危害
- **后端代码检查输入的数据是否符合预期**，严格限制变量的类型，例如使用正则表达式进行一些匹配处理。
- **对进入数据库的特殊字符（'，"，，<，>，&，\*，; 等）进行转义处理，或编码转换**。基本上所有的后端语言都有对字符串进行转义处理的方法，比如 lodash 的 lodash._escapehtmlchar 库。
- **所有的查询语句建议使用数据库提供的参数化查询接口**，参数化的语句使用参数而不是将用户输入变量嵌入到 SQL 语句中，即不要直接拼接 SQL 语句。例如 Node.js 中的 mysqljs 库的 query 方法中的 ? 占位参数