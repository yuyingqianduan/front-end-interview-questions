### HTML5和CSS3核心

### css预处理器

#### less （个人移动端用的比较多一些）

中文文档：https://less.bootcss.com/#%E6%A6%82%E8%A7%88

安装：在 Node.js 环境中使用 Less 

```less
npm install -g less`
`> lessc styles.less styles.css
```

在浏览器环境中使用 Less ：

```js
<link rel="stylesheet/less" type="text/css" href="styles.less" />`
`<script src="//cdnjs.cloudflare.com/ajax/libs/less.js/3.11.1/less.min.js" ></script>
```

`less`使用`@`符号来标识变量，可支持兼并css书写规则

导入：@import` `"themes/themes.less";

```scss
// 最基本的嵌套就不提了
// 混合
.bordered {
  border-top: dotted 1px black;
  border-bottom: solid 2px black;
}
#menu a {
  color: #111;
  .bordered();
}
// & 表示当前选择器的父级 （和下面的sass一致）
.clearfix {
  display: block;
  zoom: 1;

  &:after {
    content: " ";
    display: block;
    font-size: 0;
    height: 0;
    clear: both;
    visibility: hidden;
  }
}
// calc() 特例
@var: 50vh/2;
width: calc(50% + (@var - 20px));  // 结果是 calc(50% + (25vh - 20px))

// 作用域
@var: red;
#page {
  @var: white;
  #header {
    color: @var; // white
  }
}
// 函数
@color: #FF8000;
@width:1.0;
.mycolor{
color: @color;
 width: percentage(@width);
}
// 编译后
.mycolor {
  color: #FF8000;
  width: 100%;
}
```



#### sass （个人pc用的多一些）

中文文档：https://www.sass.hk/docs/

安装： npm install sass -g

`sass`使用`$`符号来标识变量，可支持兼并css书写规则

导入：@import` `"themes/themes.scss";

```scss
// 最基本的嵌套就不提了
// 父选择器的标识符 & （和上面的less一样）
article a {
  color: blue;
  &:hover { color: red }
}
// 混合器 
@mixin round-eg {
  -moz-border-radius: 5px;
  -webkit-border-radius: 5px;
  border-radius: 5px;
}
test {
  background-color: green;
  border: 2px solid #000;
  @include round-eg;
}
// 混合器传参
@mixin view-colors($normal, $hover, $visited) {
  color: $normal;
  &:hover { color: $hover; }
  &:visited { color: $visited; }
}

a {
  @include view-colors(blue, red, green);
}

// Sass最终生成的是：
a { color: blue; }
a:hover { color: red; }
a:visited { color: green; }

// 继承
// 通过选择器继承继承样式
.test {
  border: 1px solid red;
  background-color: #fdd;
}
.new-tag {
  @extend .test;
  border-width: 3px;
}
```

#### stylus （个人比较讨厌这样的写法不习惯，写习惯了py的人估计喜欢）

中文文档：https://www.stylus-lang.cn/

安装： npm install stylus -g

`stylus`使用`$`符号来标识变量

```stylus
// 变量
$font-size = 14px
body {
  font: $font-size sans-serif;
}
// 编译为：
body {
  font: 14px sans-serif;
}
// ...blabla更多的参考官网文档就好了
```

