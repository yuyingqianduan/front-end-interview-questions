/*
 * @Description: vscode自带注释
 * @Version: 2.0
 * @Autor: lhl
 * @Date: 2020-11-07 14:55:15
 * @LastEditors: lhl
 * @LastEditTime: 2020-11-07 19:30:11
 */
let a: string;
a = 'hello,ts'

function add (a:number,b:number){
    return a + b
}

add(1,2)

// 1.基础类型 string number 基类
let tsStr:string = '字符串类型';
let tsNum:number = 5;
let tsBoolean:boolean = false;

// 对象标注
let obj:{x:number,y:number} = {
    x: 1,
    y: 2
}

// 数组类型
let arr: string[] = [];
arr.push('只能push对应类型的值') //正确
// arr.push(1) 错误

let arr2:Array<number> = []
arr2.push(1)

// 元祖类型 源目标个数限定 不能push或者操作不是定义的类型的
let arr3:[number,string] = [1,'ss']
// arr3.push(false) 报错

// 数字类型枚举
enum HTTP_CODE {
    OK = 200,
    NOT_FOUN = 404,
    NOT_LOGIN = 401
}

console.log(HTTP_CODE.OK)

// 无值类型 执行了return 但是没有具体的值返回
function fn():void{
    
}

// Never类型 不会执行return
function fin():never{
    throw new Error('Error Message')
}

let aa: string ;
a = fin()

// 任何类型 不推荐实在不知道时候标注
let bb:any;
bb = '1';
bb = 1;

// ts3.0 unknow类型  any的安全模式
// unknow只能赋给unknow或者 any

// 函数类型 手写一个 forEach
function forEach(data: number[], callback:(key:number,value:number) => void){
    for(let i:number = 0; i<data.length; i++){
        callback(i,data[i]);
    }
}

let testArr = [1,2];
forEach(testArr,(key,value) => {
    console.log(key,value)
})
