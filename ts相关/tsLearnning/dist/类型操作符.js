/*
 * @Description: 类型操作符
 * @Version: 2.0
 * @Autor: lhl
 * @Date: 2020-11-07 20:04:30
 * @LastEditors: lhl
 * @LastEditTime: 2020-11-07 20:44:45
 */
var colors = {
    color1: 'red',
    color2: 'blud'
};
// typeof 跟的值
var clorlObj = typeof colors;
// in for in 遍历
// 类型保护 typeof  instanceof
function toUpperCase(str) {
    if (typeof str === 'string') {
        str.toUpperCase();
    }
    else {
        str.push();
    }
}
