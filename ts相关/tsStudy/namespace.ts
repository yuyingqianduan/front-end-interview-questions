/*
 * @Description: ts命名空间 命名空间一个最明确的目的就是解决重名问题
 * @Version: 2.0
 * @Autor: lhl
 * @Date: 2021-05-02 21:03:38
 * @LastEditors: lhl
 * @LastEditTime: 2021-05-02 21:19:51
 */
namespace nameTest{
    export class People{
        public name:string = 'alien'
        setName(){
          console.log('我是alien')
        }
    }
}

namespace nameTest2{
    export class People{
        public name:string = 'bob'
        setName(){
          console.log('我是bob')
        }
    }
}

let people1:nameTest.People   = new nameTest.People()
let people2:nameTest.People   = new nameTest2.People()
people1.setName() // 我是alien
