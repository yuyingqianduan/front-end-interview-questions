# react基础到高阶积累

### react的三个特点

- 声明式
- 组件化
- 一次学习随处编写

### react开发前置准备

安装 vscode编辑器 http://vscode.bianjiqi.net/

安装nodejs http://nodejs.cn/

创建项目 npx create-react-app  [项目名]

cd [项目名]

启动项目 npm  start

```javascript
import React from 'react'
import './index.css'
import App from './App'
// react 17
// import ReactDOM from 'react-dom';
import reportWebVitals from './reportWebVitals'

// react 17
// ReactDOM.render(
//   <React.StrictMode>
//     <App />
//   </React.StrictMode>,
//   document.getElementById('root')
// );

// react 18 现在默认是 18版本了
import { createRoot } from 'react-dom/client'
const container = document.getElementById('root')
const root = createRoot(container)
root.render(<App tab="home" />)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals()
```

### 插件 

rcc 【react - class - component】

rfc 【react - function - component】

### 基础语法

#### onClick 点击事件

onClick={ this.methodsName.bind(this) }  【bind绑定this】

onClick={ ()=> this.methodsName() }     【箭头函数绑定this】

onClick={ this.methodsName }     【this.state外绑定this】

this.state= {

}

this.methodsName = this.methodsName.bind(this)

#### onClick 点击事件 传参

this.handleclick.bind(this，要传的参数单个或者多个)

handleclick(传过来的参数，event)

onClick={ ()=> this.methodsName(要传的参数单个或者多个) }

#### jsx渲染

```javascript
import './App.css'
function App() {
	const obj = { name: 'bob', age: 20 }
	// eslint-disable-next-line no-undef
	const bigInt = BigInt(123)
	const symbol = Symbol('10')
	const arr = [1, 2, 3]
	const jsxDom = (
		<div>
			nihao<span>我是jsx</span>
		</div>
	)
	console.log(bigInt, symbol)
	// 得到结论是 直接渲染的基础数据类型只有数字和字符串
	return (
		<div className="App">
			<h1>渲染数字{1}</h1>
			<h1>渲染字符串{'abc'}</h1>
			<h1>渲染布尔值true：{true}</h1>
			<h1>渲染布尔值false:{false}</h1>
			<h1>渲染null:{null}</h1>
			<h1>渲染undefined:{undefined}</h1>
			{/* 直接渲染对象报错 */}
			{/* <h1>渲染Object:{obj}</h1> */}
			<h1>渲染bigInt:{bigInt}</h1>
			<h1>渲染symbol:{symbol}</h1>
			<h1>渲染对象属性:{obj.name}</h1>
			{arr.map(item => {
				return <h1 key={item}>{item}</h1>
			})}
			<h1>渲染jxs:{jsxDom}</h1>
		</div>
	)
}
export default App
```

#### jsx本质

```javascript
// jsx本质
import { jsx as _jsx } from 'react/jsx-runtime'
function APP1() {
	return _jsx('h1', {
		children: 'hello，jsx',
	})
}
// 编译出 <h1>hello，jsx</h1>
```

### 函数式组件和类组件

#### 函数式组件



#### 类组件

#### 函数式组件和类组件的区别

语法上的区别：函数式组件本质是一个函数，接受props返回一个react元素即可，类组件需要React.Component 并且需要render返回react元素

调用方式的区别：函数式组件可以直接调用返回一个新的react元素，类组件在调用时候需要创建一个实例然后通过调用实例里的render方法来返回一个react元素

状态管理：函数式组件没有状态管理（hook出现之前），类组件有

生命周期：函数式组件没有生命周期（hook出现之前），类组件有

### React事件绑定

基础语法：on事件类型 = {事件处理函数}

const  handleChange  = () => { }

eg：onClick = { handleChange  }

#### React事件绑定原理

react中的事件都是合成事件，不像原生事件注册哪里就哪里触发，react16版本事件的冒泡都是统一在document上处理的，react17是将事件处理添加到渲染react树的根DOM容器中，原因是一旦你想要局部使用react ，那么react中的事件会影响全局（比如微前端）

阻止冒泡后 onClick事件不生效了

```javascript
import React, { Component } from 'react'

export default class EventTest extends Component {
	componentDidMount() {
		// 不建议操作 dom 为了示范事件原理
		document.getElementById('btn').addEventListener('click', e => {
			// 阻止冒泡
			e.stopPropagation()
		})
	}
	render() {
		return (
			<div>
				<h1>EventTest</h1>
				<button
					id="btn"
					onClick={() => {
						console.log(111)
					}}
				>
					点击事件
				</button>
			</div>
		)
	}
}
```

### React中的状态

state 是类的属性

state只能通过setState 进行修改

#### 父子组件传值

**子组件中只能使用不能修改父组件传递过来的数据！！！如果非要修改，那就父组件传递一个方法给子组件，子组件使用这个方法来改变父组件中的数据。这就是react的单项数据流（父组件传递给子组件，不可以在子组件中改变）**

### 生命周期（v16 v17 v18）

![img](https://img2022.cnblogs.com/blog/1186521/202204/1186521-20220409143223121-1796707195.png)

![img](https://img2022.cnblogs.com/blog/1186521/202204/1186521-20220409142237094-1387120349.png)

**<u>生命周期顺序Mounting 挂载顺序(初始化)</u>**

- constructor(props)：初始化 state 和 props 数据
- 新增 static getDerivedStateFromProps(nextProps, prevState)：组件初始化和被更新时调用用来替代 componentWillMount() 在组件即将被挂载到页面时执行(16.3已废弃)
- render()：渲染组件 [返回虚拟dom最终渲染成真实dom]
- componentDidMount()：在组件被挂载到页面后执行，只在挂载时执行一次

**<u>Updation 更新顺序(更新)</u>**

- static getDerivedStateFromProps(nextProps, prevState)：组件初始化和被更新时调用
- shouldComponentUpdate(nextProps, nextState)：在组件被更新之前执行 (return true 更新 , return false 不更新)
- render()： 渲染页面static getSnapshotBeforeUpdate(prevProps, prevState)
- componentDidUpdate()：state或props更新后调用

**<u>Unmounting 卸载(销毁)</u>**

- componentWillUnmount()：在组件即将被页面剔除时执行

  还可使用 ReactDOM.unmountComponentAtNode(DOM节点)

**<u>注意：</u>**除了render函数，其他所有的生命周期函数都可以没有，react中获取真实dom有两种方法，一种是 react 的ref，一种是 ReactDOM.findDomNode(DOM节点)

**<u>废弃的生命周期</u>**

[React](https://so.csdn.net/so/search?q=React&spm=1001.2101.3001.7020) 16之后有三个生命周期被废弃：

- componentWillMount
- componentWillReceiveProps
- componentWillUpdate

废除了不是不让使用,会有警告,在17.x版本中,最好是加上UNSAFE_前缀

### React Hooks (v16.8往后的版本可用)

#### hooks解决的痛点

- 组件直接复用状态逻辑困难
- 复杂组件变得难以理解
- 难以理解的class（代码冗余、this指向等）

#### hooks的好处

- 复用业务逻辑
- 实现业务关注点分离
- 解决class存在代码冗余的问题

#### 常用的hooks

##### useState

```javascript
语法：const [state, setState] = useState(initState)
const [num, setNum] = useState(0)
```

对比 class 组件

```javascript
this.state = {}
this.setState({})
```

class组件中this.setState更新是state是合并， useState中setState是替换

##### useEffect

```javascript
	// useEffect 作用模拟类组件的生命周期
	// 语法：useEffect(fn,[deps])   deps 依赖项
	useEffect(() => {
		console.log('第二个参数没有等同于componentDidMount 和 componentDidUpdate 不停的在调用')
        return () => {
			// 取消订阅 清除副作用 定时器 ....
            console.log('componentWillUnmount ');
		}
	})

	useEffect(() => {
		console.log('第二个参数是空数组等同于componentDidMount,只执行一次')
        return () => {
			// 取消订阅 清除副作用 定时器 ....
            console.log('componentWillUnmount ');
		}
	}, [])

	useEffect(() => {
		console.log('第二个参数是非空数组只有数组中依赖项发生变化时候才执行')
        return () => {
			// 取消订阅 清除副作用 定时器 ....
            console.log('componentWillUnmount ');
		}
	}, [deps])
```

##### useLayoutEffect 

```javascript
// useLayoutEffect 强制useeffect的执行为同步，并且先执行useLayoutEffect内部的函数
// 语法：useLayoutEffect(fn,[deps])   deps 依赖项
useLayoutEffect(() => {
	console.log('useLayoutEffect componentWillUnmount 无依赖')
})

useLayoutEffect(() => {
	console.log('useLayoutEffect componentWillUnmount 空数组')
}, [])

useLayoutEffect(() => {
	console.log('useLayoutEffect componentWillUnmount 依赖项')
}, [deps])

// useLayoutEffect 和 componentDidMount 等价
```

**useEffect 和 useLayoutEffect 的区别**

- useEffect在浏览器渲染完成后执行
- `useEffect` 是异步执行的，而`useLayoutEffect`是同步执行的
- useLayoutEffect在在DOM结构更新后、渲染前执行，相当于有一个防抖效果
- useLayoutEffect总是比useEffect先执行
- useLayoutEffect里面的任务最好影响了Layout（布局）
- 应先使用useEffect，若出问题再尝试useLayoutEffect；
- useLayoutEffect与componentDidMount，componentDidUpdate的调用阶段是一样的
- 针对服务端渲染：useEffect和useLayoutEffect都无法在JS代码加载完成之前执行

##### useContext

```javascript
import React, { useState, useContext } from 'react'
// React.createContext() // 创建一个用于共享数据的上下文
let themeContext = React.createContext(null)
export default function HookTest() {
	const [num, setNum] = useState(0)
	return (
		<div>
			<h1>HookTest</h1>
			<h3>{num}</h3>
			<button onClick={() => setNum(num + 1)}>加1</button>
			<themeContext.Provider value={{ background: 'red', color: 'blue' }}>
				<TabBar />
			</themeContext.Provider>
		</div>
	)
}

function TabBar(porps) {
	return (
		<div>
			<h1>bar</h1>
			<TabButton />
		</div>
	)
}

function TabButton(porps) {
	// useContext 跨组件传值
	const value = useContext(themeContext)
	console.log(value, 'value')
	return (
		<div>
			<button style={{ color: value.color }}>TabButton</button>
		</div>
	)
}
```

##### useRef

```javascript
import React, { useRef } from 'react'
// react-use 一个很好用的 React Hooks 库
import { useFullscreen, useGeolocation, useToggle } from 'react-use'
export default function HookTest() {
	return (
		<div>
			<h1>HookTest</h1>
			{/* react-use 一个很好用的React Hooks库 */}
			<GetGeoLocal />
			<GetFullScreen />
		</div>
	)
}

// 获取位置
function GetGeoLocal(props) {
	let state = useGeolocation()
	return (
		<div>
			<h1>{JSON.stringify(state, null, 2)}</h1>
		</div>
	)
}

// 全屏
function GetFullScreen(props) {
    // useRef 使用
	const ref = useRef(null)
	const [show, toggle] = useToggle(false)
	useFullscreen(ref, show, { onClose: () => toggle(false) })
	return (
		<div ref={ref} style={{ background: 'red' }}>
			<button onClick={() => toggle()}>点击全屏</button>
		</div>
	)
}

const Test = () => {
    const inputRef = useRef();
    return (
        <>
            <input ref={inputRef} />
            <button onClick={() => inputRef.current.focus()}>focus</button>
        </>
    );
}
export default Test
```

类组件 使用 React.createRef()  createRef 每次渲染都会返回一个新的引用，而useRef每次都会返回相同的引用

##### useMemo

```javascript
// useMemo(回调函数，[依赖值])
useMemo(() => {
    // 做一些事情
},[value]);
```

##### useCallback

```javascript
// useCallback(回调函数，[依赖值])
const handleClick = useCallback(()=> {
    // 做一些事
}, [value]);
```

##### useImperativeHandle 

##### useReducer  纯函数在 redux  react-redux中演示

### 高阶组件（HOC）

组件设计的目的：保证组件功能的单一性，本质是一个函数，函数接受一个组件，返回一个新组件

高阶组件是一个函数，并不是组件，HOC 自身不是 React API 的一部分，它是一种基于 React 的组合特性而形成的 **设计模式**

高阶组件是 React 中用于复用组件逻辑的一种高级技巧

```javascript
const EnhancedComponent = HOC(WrappedComponent);
```

```javascript
import React, { Component } from 'react'
class HocTest extends Component {
	render() {
		return (
			<div>
				<h1>HocTest</h1>
				<p>{this.props.name}</p>
				<p>{this.props.age}</p>
			</div>
		)
	}
}

const hightHocComp = Comp => {
	// 加工处理
	const newComp = props => {
		return <Comp {...props} name="HOC高阶组件" age={20} />
	}
	// 返回新组件
	return newComp
}
// 使用高阶组件 支持链式调用 重写生命周期
export default hightHocComp(HocTest)
```

```javascript
function withLog (WrappedComponent, pageName) {
   return class extends React.Component {
     static displayName = `withLog(${getDisplayName(WrappedComponent)})`;
     componentDidMount() {
       // 用console.log来模拟日志打印 实际上这里一般会传到服务器保存
       console.log(pageName)；
     }
     render (){
       // 此处注意要把this.props继续透传下去
       return <WrappedComponent {...this.props} />;
     }
   }
}

function getDisplayName(WrappedComponent) {
  return WrappedComponent.displayName || WrappedComponent.name || 'Component';
}
```

#### HOC可以用来做什么

- 代码重用，逻辑和引导抽象
- 渲染劫持（Render Highjacking）
- state 抽象和操作
- props 操作

#### HOC实现方式

##### 属性代理

```javascript
function HOC(WrappedComponent) {
  const otherProps = {}
  return class NewComp extends React.Component {
    render() {
      return <WrappedComponent {...this.props} {...otherProps}/>
    }
  }
}
```

通过创建新组件来包裹原始组件, 把原始组件作为新组件的子组件渲染
功能: 可实现对原始组件的 props数据更新 和 组件模板更新

**优点：**

- 正常属性代理可以和业务组件低耦合，零耦合，对于条件渲染和props属性增强,只负责控制子组件渲染和传递额外的props就可以，所以无须知道，业务组件做了些什么。所以正向属性代理，更适合做一些开源项目的hoc，目前开源的HOC基本都是通过这个模式实现的。
- 同样适用于class声明组件，和function声明的组件。
- 可以完全隔离业务组件的渲染,相比反向继承，属性代理这种模式。可以完全控制业务组件渲染与否，可以避免反向继承带来一些副作用，比如生命周期的执行。
- 可以嵌套使用，多个hoc是可以嵌套使用的，而且一般不会限制包装HOC的先后顺序

**缺点：**

- 一般无法直接获取业务组件的状态，如果想要获取，需要ref获取组件实例。
- 无法直接继承静态属性。如果需要继承需要手动处理，或者引入第三方库

##### 反向继承

通过创建新组件继承自原始组件, 把原始组件作为父组件
功能: 可实现对原始组件的state状态数据更新 和 组件模板更新

```javascript
import React, { Component } from 'react'

class HocMode extends Component {
	constructor(props) {
		super(props)
		this.state = { count: 100 }
	}
	componentDidMount() {
		console.log('演示 反向继承 子组件 mount')
	}
	// 点击事件
	handleClick() {
		console.log('组件被点了')
	}
    // 显示的内容是 10 --- 使用的是包装里面的state
	render() {
		return (
			<div>
				<h1>
					{this.state.count}---{this.state.title}
				</h1>
				<p>我是被高阶组件包装的普通组件</p>
			</div>
		)
	}
}

const HOCComp = WrapComponent => {
	return class extends WrapComponent {
		constructor(props) {
			super(props)
			this.state = { count: 10, title: '使用的是包装里面的state' }
		}
		componentDidMount() {
			// 反向继承后,可以拿到原始组件的 props和 state数据 console.log先打印包装组件内的内容
			console.log('高阶组件-componentDidMount', this.props, this.state)
			this.handleClick()
		}
		render() {
			return (
				// 使用 super.render() 渲染父组件模板
				<div>
					<button onClick={this.handleClick}>点击高阶组件</button>
					<div>{super.render()}</div>
				</div>
			)
		}
	}
}
export default HOCComp(HocMode)
```

反向继承指的是使用一个函数接受一个组件作为参数传入，并返回一个继承了该传入组件的类组件，且在返回组件的 render() 方法中返回 super.render() 方法

装饰组件直接传入

super.render( ) 调用

**优点：**

- 方便获取组件内部状态，比如state，props ,生命周期,绑定的事件函数等
- es6继承可以良好继承静态属性，无须对静态属性和方法进行额外的处理。

**缺点：**

- 无状态组件无法使用。
- 和被包装的组件强耦合，需要知道被包装的组件的内部状态，具体是做什么
- 如果多个反向继承hoc嵌套在一起，当前状态会覆盖上一个状态。这样带来的隐患是非常大的，比如说有多个componentDidMount，当前componentDidMount会覆盖上一个componentDidMount。这样副作用串联起来，影响很大

#### 高阶组件的约定

- 使用命名来方便调试
- 高阶组件必须是个纯函数(相同输入必须返回相同的结果) 这样才能保证组件的可复用性
- props 保持一致
- 不能在函数式（无状态）组件上使用 ref 属性，因为它没有实例（关注React.forwardRef）
- 不要以任何方式改变原始组件WrappedComponent
- 透传不相关 props 属性给被包裹的组件 WrappedComponent
- 不要在 render()方法中使用高阶组件
- 使用 compose 组合高阶组件

### redux And  react-redux

![img](https://img2022.cnblogs.com/blog/1186521/202204/1186521-20220417195818954-246109198.png)

```
npm install redux -S
npm install react-redux -S
```

redux

```javascript
// store/index
import { createStore } from 'redux'
/**
 * 这是一个 reducer，形式为 (state, action) => state 的纯函数。
 * 描述了 action 如何把 state 转变成下一个 state。
 *
 * state 的形式取决于你，可以是基本类型、数组、对象、
 * 甚至是 Immutable.js 生成的数据结构。惟一的要点是
 * 当 state 变化时需要返回全新的对象，而不是修改传入的参数。
 *
 * 下面例子使用 `switch` 语句和字符串来做判断，但你可以写帮助类(helper)
 * 根据不同的约定（如方法映射）来判断，只要适用你的项目即可。
 */
function counter(state = 10, action) {
	switch (action.type) {
		case 'INCREMENT':
			return state + 1
		case 'DECREMENT':
			return state - 1
		default:
			return state
	}
}

// 创建 Redux store 来存放应用的状态。
// API 是 { subscribe, dispatch, getState }。
let store = createStore(counter)

export default store
```

```javascript
import React from 'react'
import './index.css'
import App from './App'
// react 17
import ReactDOM from 'react-dom'
import store from './store'

// react 17
function render() {
	ReactDOM.render(
		<React.StrictMode>
			<App />
		</React.StrictMode>,
		document.getElementById('root')
	)
}
render() //  先执行一次 render
// 订阅一次 监听dom更新 不然视图不会更新
store.subscribe(render)
```

```javascript
import React from 'react'
import store from '../store'

export default function ReduxTest() {
	const state = store.getState()
	console.log(state)
	return (
		<div>
			<hr />
			<h1>ReduxTest</h1>
			<p>{state}</p>
			<button onClick={() => store.dispatch({ type: 'INCREMENT' })}>加+1</button>
			<button onClick={() => store.dispatch({ type: 'DECREMENT' })}>减-1</button>
		</div>
	)
}
```

react-redux  解决手动订阅的问题 store.subscribe()

```javascript
// index.js
import React from 'react'
import './index.css'
import App from './App'

// react 17
import ReactDOM from 'react-dom'
import reportWebVitals from './reportWebVitals'
import store from './store'
import { Provider } from 'react-redux'

// react 17
ReactDOM.render(
	<Provider store={store}>
		<App />
	</Provider>,
	document.getElementById('root')
)
```

```javascript
// store/index
import { createStore } from 'redux'
import counter from '../reducer/counter'
// 创建 Redux store 来存放应用的状态。
// API 是 { subscribe, dispatch, getState }。
let store = createStore(counter)

export default store
```

```javascript
// reducer/counter
function counter(state = 10, action) {
	switch (action.type) {
		case 'INCREMENT':
			return state + 1
		case 'DECREMENT':
			return state - 1
		default:
			return state
	}
}

// 处理状态
export const mapStateToProps = state => {
	return {
		// counter 就是上面的 reducer
		// 多个 reducer (combineReducers) 时候需要 state.xxx 标识 单个就返回 state
		count: state,
	}
}

// 处理事件
export const mapDispatchToProps = dispatch => {
	return {
		increment() {
			dispatch({ type: 'INCREMENT' })
		},
		decrement() {
			dispatch({ type: 'DECREMENT' })
		},
	}
}
export default counter
```

```javascript
// 测试组件
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { mapStateToProps, mapDispatchToProps } from '../reducer/counter'

class ReactReduxTest extends Component {
	render() {
		const { count, increment, decrement } = this.props
		return (
			<div>
				<hr />
				<h1>ReactReduxTest</h1>
				<p>{count}</p>
				<button onClick={() => increment()}>加+1</button>
				<button onClick={() => decrement()}>减-1</button>
			</div>
		)
	}
}
// 典型的高阶函数
export default connect(mapStateToProps, mapDispatchToProps)(ReactReduxTest)
```

#### connect 实现原理

#### Provider 实现原理
