/*
 * @Description: ts学习  运行编译：tsc xx文件名
 * @Version: 2.0
 * @Autor: lhl
 * @Date: 2021-05-02 16:13:25
 * @LastEditors: lhl
 * @LastEditTime: 2021-05-02 17:34:16
 */
// 基础类型
// 字符串
var hello = "hello word 哈哈";
console.log(hello);
// 布尔
var isBoolean = true;
function add(a, b) {
    return a + b;
}
console.log(add(1, 2));
// 数组 元祖（必须要声明类型）
var list1 = [1, 2, 3, 4];
var list2 = [1, 2, 3, 4];
var list3 = [1, 2, 3, 4];
var list4 = [1, "abc", true];
var list5 = ['1', 2];
// 对象 object
var obj = {
    age: 18,
    sex: "男"
};
// console.log(obj.name) // 定义 Object 还是会有问题
var obj2 = {
    age: 18,
    sex: "男"
};
var Point = /** @class */ (function () {
    function Point(x, y) {
        var _this = this;
        this.drawPoint = function () {
            console.log("x:", _this.x, "y:", _this.y);
        };
        this.getDistances = function (p) {
            return Math.pow(p.x - _this.X, 2) + Math.pow(p.y - _this.y, 2);
        };
        this.x = x;
        this.y = y;
    }
    return Point;
}());
var point = new Point(2, 3); // 对象实例
point.drawPoint();
// 联合类型
var count;
// count = true // 报错
count = '1';
count = 2;
// 字面量类型 {literal}
var selectType;
// 枚举类型 Enum
var Color;
(function (Color) {
    Color[Color["red"] = 0] = "red";
    Color[Color["block"] = 1] = "block";
    Color[Color["blue"] = 2] = "blue";
})(Color || (Color = {}));
var color = Color.red;
console.log(color, 'color'); // 0
var Color2;
(function (Color2) {
    Color2[Color2["red"] = 5] = "red";
    Color2[Color2["block"] = 6] = "block";
    Color2[Color2["blue"] = 7] = "blue";
})(Color2 || (Color2 = {}));
var color2 = Color2.block;
console.log(color2, 'color2'); // 6
//类型适配（断言）
var msg;
msg = "hello ts";
var str1 = msg.endsWith("s");
var str2 = msg.endsWith("s");
// 函数类型
var sum1 = function (x, y) {
    return x + y;
};
var sum2 = function (x, y) {
    return x + y;
};
sum1(1); // 可选
sum2(1, 2); // 必传
// any 和 unknown
var randomVal1 = 666;
randomVal1 = true;
randomVal1 = "sytsf";
randomVal1 = {};
// randomVal1()
// 确认了变量类型后才可以使用
var randomVal2 = 666;
randomVal2 = true;
randomVal2 = "sytsf";
randomVal2 = {};
if (typeof randomVal2 === 'function') {
    randomVal2();
}
// void 、 undefined 、never
function print() {
    console.log("balalala");
}
console.log(print(), '看看打印了啥'); // undefined
function returnUndefined() {
    console.log(666);
    return;
}
console.log(returnUndefined(), '看看又打印了啥'); // undefined
// 永远无法保证完成的状态
function throwError(message, erroorCode) {
    throw {
        message: message,
        erroorCode: erroorCode
    };
}
throwError('not found', 404);
function whileLoop() {
    while (true) {
        console.log("object");
    }
}
