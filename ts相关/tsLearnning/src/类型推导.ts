/*
 * @Description: 类型推导
 * @Version: 2.0
 * @Autor: lhl
 * @Date: 2020-11-07 19:49:15
 * @LastEditors: lhl
 * @LastEditTime: 2020-11-07 20:03:13
 */
let age = 20;

function user(age = 10){

}

// 断言
let img = document.getElementById('img') as HTMLElement