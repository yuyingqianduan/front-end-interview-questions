/*
 * @Description: 类型推导
 * @Version: 2.0
 * @Autor: lhl
 * @Date: 2020-11-07 19:49:15
 * @LastEditors: lhl
 * @LastEditTime: 2020-11-07 20:03:13
 */
var age = 20;
function user(age) {
    if (age === void 0) { age = 10; }
}
// 断言
var img = document.getElementById('img');
