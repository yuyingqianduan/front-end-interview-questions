# 什么是Virtual DOM

Virtual DOM(虚拟DOM)，是由普通的JS对象来描述DOM对象，因为不是真实的DOM对象，所以叫Virtual DOM

虚拟DOM（Virtual DOM）是对DOM的JS抽象表示，它们是JS对象，能够描述DOM结构和关系。应用的各种状态变化会作用于虚拟DOM，最终映射到DOM上

真实DOM成员

可以使用Virtual DOM来描述真实DOM

<u>**虚拟dom优点**</u>

虚拟DOM轻量、快速：当它们发生变化时通过新旧虚拟DOM比对可以得到最小DOM操作量，从

而提升性能

```javascript
{
  sel: 'div',
  data: { },
  children: undefined,
  text: 'hello world',
  ele: undefined,
  key: undefined
}
```

# 虚拟DOM的作用

1.维护视图和状态的关系

2.复杂视图情况下提升渲染性能

3.除了渲染dom还可以实现ssr、原生应用、小程序等

# 为什么使用Virtual DOM

1. 手动操作DOM比较麻烦，还需要考虑浏览器兼容性问题，虽然有jQuery等库简化DOM操作，但是随着项目的复杂DOM操作复杂提升
2. 为了简化DOM的复杂操作于是出现了各种MVVM框架，MVVM框架解决了视图和状态的同步问题
3. 为了简化视图的操作我们可以使用模板引擎，但是模板引擎没有解决跟踪状态变化的问题，于是Virtual DOM出现了
4. Virtual DOM的好处是当状态改变时不需要立即更新DOM，只需要创建一个虚拟树来描述DOM，Virtual DOM内部将弄清楚如何有效(diff)的更新DOM

# Snabbdom基本使用(Vue 2.x内部使用的Virtual DOM就是改造的Snabbdom)

打包工具使用parcel

Snabbdom参考文档：https://github.com/snabbdom/snabbdom

### 创建项目(window下命令)

mkdir  Snabbdom-test

cd  Snabbdom-test

npm init -y

cnpm i parcel-bundler -S

cnpm i snabbdom -S

### 配置package.json的scripts

```javascript
  "scripts": {
    "dev": "parcel index.html --open",
    "build": "parcel build index.html"
  },
```

### 看源码必备的快捷键

点击属性或者方法 + f12  or  ctrl + 鼠标左键 回到变量或者函数定义的地方

node_modules 搜索插件名直接点node_modules 文件夹输入对应插件名字即可跳转对应的依赖项

# Snabbdom使用案例

```javascript
import { h, init, thunk } from "snabbdom";
console.log(h, init, thunk, 'snabbdom')
// init()是一个高阶函数返回path()
// h()返回虚拟节点VNode
// thunk()是一种优化策略，可以在处理不可变数据时候使用

// patch函数 对比两个vnode的差异更新到真实 dom
let patch = init([])

// 第一个参数：标签 + 选择器
// 第二个参数：如果是字符串的话就是内容
let vnode = h('div#wrap.content', 'hello world')

let app = document.querySelector('#app')

// 第一个参数：可以说dom元素，内部会把dom转换成 VNode
// 第二个参数：VNode
// 返回值： VNode
let oldVnode = patch(app,vnode)

// 更新

// 单个元素更新
// vnode = h('div', 'hello world，你被更新了！！！')

// 多个节点元素更新
vnode = h('div#test', [
  h('h1', '哈哈哈'),
  h('p','胖胖胖')
])

// 对比差异
patch(oldVnode, vnode)

// 清空页面元素 --  patch(oldVnode,h('!'))
```

```javascript
// 导入模块
// 注册模块
// 使用 h() 函数的第二个参数传入模块需要的数据（对象）
import { init, h,   styleModule,
  eventListenersModule, } from 'snabbdom'
// import style from 'snabbdom/modules/style'
// import eventlistteners from 'snabbdom/modules/eventlistteners'

// 注册模块
let patch = init([
  styleModule,
  eventListenersModule
])

// 使用 h() 函数的第二个参数传入模块需要的数据（对象）
let vnode = h('div', {
  style: {
    fontSize: 16,
    backgroundColor: 'red'
  },
  on: {
    click: eventHandler
  }
}, [
  h('h1', '这是h1标签'),
  h('h3', '这是h3标签')
])

function eventHandler () {
  console.log('被点了')
}

let app = document.querySelector('#app')

patch(app,vnode)
```

# npm 由三个独立的部分组成

- 网站
- 注册表（registry）
- 命令行工具 (CLI)

参考文档：https://www.npmjs.cn/getting-started/what-is-npm/

安装：安装nodejs即可使用相关命令

nodejs网站：http://nodejs.cn/

查看npm版本：npm -v

更新命令：`npm install npm@latest -g ` 或者 `npm install npm@next -g`

nvm（node）版本管理ios：https://github.com/nvm-sh/nvm/blob/master/README.md#installation

nvm（node）版本管理window：https://github.com/coreybutler/nvm-windows

生成一个package.json文件命令：npm init  or  npm init  -y

# 如何安装包(安装到全局后面加 -g)

npm install <package1>  <package2> .... -S (--save)

npm install <package1>  <package2> .... -D (--dev --save)

# 如何更新本地安装的包（更新全局的也后面多加一个 -g）

1. 在 `package.json` 文件所在的目录中执行 `npm update` 命令。
2. 执行 `npm outdated` 命令。不应该有任何输出

# 如何卸载本地安装的包（卸载全局的也后面多加一个 -g）

如需删除 node_modules 目录下面的包（package）

npm uninstall <package>

如需从 package.json 文件中删除依赖，需要在命令后添加参数 --save:

npm uninstall --save  <package>

# vue2源码分析（2.6.10）

源码地址：https://github.com/vuejs/vue

git下载：git clone https://github.com/vuejs/vue.git

### vue\src\platforms\web\entry-runtime-with-compiler.js

渲染优先级：**render > template > el**

```javascript
// 入口文件，覆盖 $mount 执行模板解析和编译工作
const mount = Vue.prototype.$mount;
Vue.prototype.$mount = function (
  el?: string | Element,
  hydrating?: boolean
): Component {
  el = el && query(el);

  /* istanbul ignore if */
  if (el === document.body || el === document.documentElement) {
    process.env.NODE_ENV !== "production" &&
      warn(
        `Do not mount Vue to <html> or <body> - mount to normal elements instead.`
      );
    return this;
  }
  // 优先级：render > template > el
  const options = this.$options;
  // resolve template/el and convert to render function
  if (!options.render) {
    let template = options.template;
    if (template) {
      if (typeof template === "string") {
        if (template.charAt(0) === "#") {
          template = idToTemplate(template);
          /* istanbul ignore if */
          if (process.env.NODE_ENV !== "production" && !template) {
            warn(
              `Template element not found or is empty: ${options.template}`,
              this
            );
          }
        }
      } else if (template.nodeType) {
        template = template.innerHTML;
      } else {
        if (process.env.NODE_ENV !== "production") {
          warn("invalid template option:" + template, this);
        }
        return this;
      }
    } else if (el) {
      template = getOuterHTML(el);
    }
    if (template) {
      /* istanbul ignore if */
      if (process.env.NODE_ENV !== "production" && config.performance && mark) {
        mark("compile");
      }

      const { render, staticRenderFns } = compileToFunctions(
        template,
        {
          outputSourceRange: process.env.NODE_ENV !== "production",
          shouldDecodeNewlines,
          shouldDecodeNewlinesForHref,
          delimiters: options.delimiters,
          comments: options.comments,
        },
        this
      );
      options.render = render;
      options.staticRenderFns = staticRenderFns;

      /* istanbul ignore if */
      if (process.env.NODE_ENV !== "production" && config.performance && mark) {
        mark("compile end");
        measure(`vue ${this._name} compile`, "compile", "compile end");
      }
    }
  }
  // 执行挂载
  return mount.call(this, el, hydrating);
};
```

### vue\src\platforms\web\runtime\index.js

定义 $mount 方法

```javascript
// diff算法的关键
Vue.prototype.__patch__ = inBrowser ? patch : noop;

// public mount method
// 定义 $mount 方法
Vue.prototype.$mount = function (
  el?: string | Element,
  hydrating?: boolean
): Component {
  el = el && inBrowser ? query(el) : undefined;
  return mountComponent(this, el, hydrating);
};
```

### vue\src\core\index.js

定义全局方法

```javascript
initGlobalAPI(Vue)
```

```javascript
Vue.set = set 
Vue.delete = del 
Vue.nextTick = nextTick 
initUse(Vue) // 实现Vue.use函数 
initMixin(Vue) // 实现Vue.mixin函数 
initExtend(Vue) // 实现Vue.extend函数 
initAssetRegisters(Vue) //注册实现Vue.component/directive/filter
```

### **core/instance/index.js**

Vue构造函数定义

定义Vue实例API

```javascript
function Vue(options) {
  // 构造函数仅执行了_init
  this._init(options);
}
initMixin(Vue); // 实现init函数
stateMixin(Vue); // 状态相关api $data,$props,$set,$delete,$watch
eventsMixin(Vue); //事件相关api $on,$once,$off,$emit lifecycleMixin(Vue) // 生命周期ap_update,$forceUpdate,$destroy
renderMixin(Vue); // 渲染api _render,$nextTick
```

### vue\src\core\instance\init.js

```javascript
    initLifecycle(vm); 
	//定义 $parent,$refs,$children,$root 等
    initEvents(vm); // 对父组件传入事件添加监听
    initRender(vm); // 声明 $slots $createElement
    callHook(vm, "beforeCreate"); // 调用beforeCreate
    initInjections(vm); // 注入数据
    initState(vm); // 数据初始化 响应式 包括props、methods、		data、computed和watch
    initProvide(vm); // 提供数据
    callHook(vm, "created"); // 调用 created
```

可以看出初始化执行了beforeCreate、created两个生命钩子

### vue\src\core\instance\lifecycle.js

执行挂载，获取vdom并转换为dom

update() 执行更新，将传入vdom转换为dom，初始化时执行的是dom创建操作

update负责更新dom，转换vnode为dom

### vue\src\core\instance\render.js

渲染组件，获取vdom

### vue\src\core\observer\index.js

observe方法返回一个Observer实例

Observer对象根据数据类型执行对应的响应化操作

defineReactive定义对象属性的getter/setter，getter负责添加依赖，setter负责通知更新

### vue\src\core\observer\dep.js

Dep负责管理一组Watcher，包括watcher实例的增删及通知更新

### vue\src\core\observer\watcher.js

Watcher解析一个表达式并收集依赖，当数值变化时触发回调函数，常用于$watch API和指令中。相关API： $watcher，每个组件也会有对应的Watcher，数值变化会触发其update函数导致重新渲染

dep.notify()之后watcher执行更新，执行入队操作

```javascript
 */
export default class Watcher {
  constructor () {}
  /**
   * Evaluate the getter, and re-collect dependencies.
   */
  get() {}
  /**
   * Add a dependency to this directive.
   */
  addDep(dep: Dep) {}
  /**
   * Clean up for dependency collection.
   */
  cleanupDeps() {}
  /**
   * Subscriber interface.
   * Will be called when a dependency changes.
   */
  update() {}

  /**
   * Scheduler job interface.
   * Will be called by the scheduler.
   */
  run() {}
  /**
   * Evaluate the value of the watcher.
   * This only gets called for lazy watchers.
   */
  evaluats() {}
  /**
   * Depend on all deps collected by this watcher.
   */
  depend() {}
  /**
   * Remove self from all dependencies' subscriber list.
   */
  teardown() {}
}

```

### vue\src\core\observer\array.js

为数组原型中的7个可以改变内容的方法定义拦截器

Observer中覆盖数组原型

```javascript
if (Array.isArray(value)) {
    if (hasProto) {
        // 替换数组原型
        protoAugment(value, arrayMethods)
    } else {
        copyAugment(value, arrayMethods, arrayKeys)
    }
    this.observeArray(value)
}
```

### vue\src\core\instance\render.js

生成虚拟dom

watcher.run() => componentUpdate() => render() => update() => patch()

### vue\src\core\vdom\patch.js（DIFF关键）

new VNode不存在就删；

old VNode不存在就增；

都存在就执行diffff执行更新

**patchVnode**：比较两个VNode，包括三种类型操作：**属性更新、文本更新、子节点更新**

1. 新老节点**均有children**子节点，则对子节点进行diff操作，调用updateChildren

2. 如果**老节点没有子节点而新节点有子节点**，先清空老节点的文本内容，然后为其新增子节点。

3. 当**新节点没有子节点而老节点有子节点**的时候，则移除该节点的所有子节点。

4. 当**新老节点都无子节点**的时候，只是文本的替换

**updateChildren**：主要作用是用一种较高效的方式比对新旧两个VNode的children得出最小操作补丁。执行一个双循环是传统方式；

![image-20210522170023936](C:\Users\Administrator.WIN-MHB8ELOJK80\AppData\Roaming\Typora\typora-user-images\image-20210522170023936.png)

在新老两组VNode节点的左右头尾两侧都有一个变量标记，在**遍历过程中这几个变量都会向中间靠拢**。 

当**oldStartIdx > oldEndIdx**或者**newStartIdx > newEndIdx**时结束循环

**遍历规则**

首先，oldStartVnode、oldEndVnode与newStartVnode、newEndVnode**两两交叉比较**，共有4种比较

方法。

1. 当 oldStartVnode和newStartVnode 或者 oldEndVnode和newEndVnode 满足sameVnode，直接将该VNode节点进行patchVnode即可，不需再遍历就完成了一次循环
2. 如果oldStartVnode与newEndVnode满足sameVnode。说明oldStartVnode已经跑到了oldEndVnode后面去了，进行patchVnode的同时还需要将真实DOM节点移动到oldEndVnode的后面
3. 如果oldEndVnode与newStartVnode满足sameVnode，说明oldEndVnode跑到了oldStartVnode的前面，进行patchVnode的同时要将oldEndVnode对应DOM移动到oldStartVnode对应DOM的前面
4. 如果以上情况均不符合，则在old VNode中找newStartVnode满足sameVnode的vnodeToMove，若存在执patchVnode，同时将vnodeToMove对应DOM移动到oldStartVnode对应的DOM的前面。
5. 当然也有可能newStartVnode在old VNode节点中找不到一致的key，或者是即便key相同却不是sameVnode，这个时候会调用createElm创建一个新的DOM节点
6. 当结束时oldStartIdx > oldEndIdx，这个时候旧的VNode节点已经遍历完了，但是新的节点还没有。说明了新的VNode节点实际上比老的VNode节点多，需要将剩下的VNode对应的DOM插入到真实DOM中，此时调用addVnodes（批量调用createElm接口）
7. 当结束时newStartIdx > newEndIdx时，说明新的VNode节点已经遍历完了，但是老的节点还有剩余，需要从文档中删 的节点删除

### **编译过程**

编译分为三步：解析、优化和生成;

### vue\src\compiler\parser\index.js

解析器将模板解析为抽象语法树(AST)，基于AST可以做优化或者代码生成工作

processIf用于处理v-if解析

processFor用于处理v-for指令

![image-20210522214722732](C:\Users\Administrator.WIN-MHB8ELOJK80\AppData\Roaming\Typora\typora-user-images\image-20210522214722732.png)

v-if，v-for这些指令只能在编译器阶段处理，如果我们要在render函数处理条件或循环只能使用 if 和 for

```javascript
Vue.component('comp', { props: ['foo'], render(h) { // 渲染内容跟foo的值挂钩，只能用if语句 
    if (this.foo=='foo') { 
        return h('div', 'foo') 
    }
    return h('div','bar') 
  }
})
```

### vue\src\compiler\codegen\index.js

将AST转换成渲染函数中的内容，即代码字符串

**<u>编译器作用：</u>**template => render()

### vue\src\compiler\index.js

标记静态子树的好处：

每次重新渲染，不需要为静态子树创建新节点

虚拟DOM中patch时，可以跳过静态子树

```javascript
export const createCompiler = createCompilerCreator(function baseCompile(
  template: string,
  options: CompilerOptions
): CompiledResult {
  // 解析 转换成对象 AST
  const ast = parse(template.trim(), options);
  if (options.optimize !== false) {
    // 优化：标记静态节点 diff时候直接跳过
    optimize(ast, options);
  }
  // 代码生成：转换ast为代码字符串
  const code = generate(ast, options);
  return {
    ast,
    render: code.render,
    staticRenderFns: code.staticRenderFns,
  }
})
```

