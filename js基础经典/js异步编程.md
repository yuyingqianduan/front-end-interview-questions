# JavaScript异步编程

js是一门单线程语言，同一个时间节点只能做一件事情【导致页面渲染和时间的执行受到影响】。一旦有http请求向服务器发送就会出现等待数据返回之前页面出现假死效果。

# 关于同步和异步

### 同步（阻塞）

js会严格按照单线程（从上到下、从左到右）执行代码逻辑，进行代码的解析和运行。

```javascript
var a = 1
var b = 2
var c = a + b
console.log(c) // 3
alert(111) // 典型的同步阻塞效果
```

### 异步（非阻塞）

异步代码是在同步的代码执行【有异步先挂起】完毕的基础上将异步的代码按照特定的执行顺序和规则运行

```javascript
var a = 1
var b = 2
var c = a + b
setTimeout(() => {
    console.log('异步')
},1000) // 如果是 0 依旧是挂起来后面执行
console.log(c) // 3
```

**总结：**JavaScript的运行顺序就是完全单线程的异步模型：同步在前，异步在后。所有的异步任务都要等待当前的同步任务执行完毕后才能执行。

# js线程的组成

JavaScript虽然是单线程的但是实际是多个线程（不是多线程）协助完成的。

比如：

1. GUI渲染线程
2. JavaScript引擎线程
3. 定时器触发线程
4. http请求
5. 其他线程

### 主线程

用来执行页面的渲染，JavaScript代码的运行，事件的触发等操作

### 工作线程

用来处理异步任务的执行实现非阻塞的运行模式

### js运行的大概模式

![img](https://img2022.cnblogs.com/blog/1186521/202205/1186521-20220522171853059-801497416.png)

队列先进先出（管道出入口），栈先进后出（压栈类似容器满了最上面的得先出去）

# 异步任务

异步任务又分宏任务和微任务

### 宏任务（(macro)task）

每次执行栈执行的代码就是一个宏任务（包括每次从事件队列中获取一个事件回调并放到执行栈中执行）。JS分为同步任务和异步任务,同步任务都在主线程上执行，形成一个执行栈

```javascript
script(整体代码)
setTimeout
setInterval
I/O
UI交互事件
postMessage
MessageChannel
setImmediate(Node.js 环境)
```

```javascript
// 宏任务1
document.addEventListener('click',() => {
     Promise.resolve().then(() => {
          console.log(1)
     })
     console.log(2)
})
// 宏任务2
document.addEventListener('click',() => {
     Promise.resolve().then(() => {
          console.log(3)
     })
     console.log(4)
})
// 输出 2 1 4 3
只有click事件比如 document.onclick  = () => { xxx } 才会覆盖 addEventListener是叠加注册的事件顺序触发
```

### 微任务（microtask）

microtask,可以理解是在当前 task 执行结束后立即执行的任务。也就是说，在当前task任务后，下一个task之前，在渲染之前。

```javascript
Promise.then
Object.observe
MutaionObserver
process.nextTick(Node.js 环境)
```

### 经典的图文关系

![img](https://img2022.cnblogs.com/blog/1186521/202205/1186521-20220522184100670-106886644.png)

![img](https://img2022.cnblogs.com/blog/1186521/202205/1186521-20220522184109314-749766897.png)

### 经典的输出面试题

![img](https://img2022.cnblogs.com/blog/1186521/202205/1186521-20220522184325398-240147454.png)

![img](https://img2022.cnblogs.com/blog/1186521/202205/1186521-20220522184330243-1831359816.png)

# 事件循环（Event Loop）

![img](https://img2020.cnblogs.com/blog/1186521/202102/1186521-20210204184151896-820410266.png)

事件循环分为两种,分别是**浏览器事件循环**和**node.js事件循环**

JavaScript 语言的一大特点就是单线程，也就是说，同一个时间只能做一件事。为了协调事件、用户交互、脚本、UI 渲染和网络处理等行为，防止主线程的不阻塞，Event Loop 的方案应用而生。Event Loop 包含两类：一类是基于 [Browsing Context](https://link.zhihu.com/?target=https%3A//html.spec.whatwg.org/multipage/browsers.html%23browsing-context)，一种是基于 [Worker](https://link.zhihu.com/?target=https%3A//www.w3.org/TR/workers/%23worker)。二者的运行是独立的，也就是说，每一个 JavaScript 运行的"线程环境"都有一个独立的 Event Loop，每一个 Web Worker 也有一个独立的 Event Loop。

**事件循环的本质：**

通过不断地循环取执行异步回调

基于事件驱动模式

至少包含了一个事件循环来判断当前的任务队列是否有新的任务

### 异步执行的运行机制

```javascript
（同步执行也是如此，因为它可以被视为没有异步任务的异步执行。）
（1）所有同步任务都在主线程上执行，形成一个执行栈（execution context stack）。 （2）主线程之外，还存在一个"任务队列"（task queue）。只要异步任务有了运行结果，就在"任务队列"之中放置一个事件。            
（3）一旦"执行栈"中的所有同步任务执行完毕，系统就会读取"任务队列"，看看里面有哪些事件。那些对应的异步任务，于是结束等待状态，进入执行栈，开始执行。
（4）主线程不断重复上面的第三步。只要主线程空了，就会去读取"任务队列"【事件的队列 or 消息的队列】，这就是JavaScript的运行机制。这个过程会不断重复
process.nextTick和setImmediate的一个重要区别：多个process.nextTick语句总是在当前"执行栈"一次执行完，多个setImmediate可能则需要多次loop才能执行完       
```

### 同步任务

在主线程上排队执行的任务，只有前一个任务执行完毕，才能执行后一个任务

### 异步任务

指的是，不进入主线程、而进入"任务队列"（task queue）的任务，只有"任务队列"通知主线程，某个异步任务可以执行了，该任务才会进入主线程执行。

### 特别的 requestAnimationFrame

`requestAnimationFrame`是HTML5版本新增的API方法

是浏览器用于定时循环操作的一个接口,类似于setTimeout,主要用途是按帧对网页进行重绘,让各种网页动 画效果(DOM动画、Canvas动画、SVG动画、WebGL动画)能够有一个统一的刷新机制,从而节省系统资源,提高系统性能,改善视觉 效果。”

默认情况下，requestAnimationFrame执行频率是1000/60,大概是16.67ms执一次

```jsx
window.requestAnimationFrame(fn)
window.cancelAnimationFrame(id)
```

停止 requestAnimationFrame

cancelAnimationFrame()接收一个参数 requestAnimationFrame默认返回一个id，cancelAnimationFrame只需要传入这个id就可以停止了

requestAnimationFrame 特点：

1. CPU节能
2. 流畅度
3. 函数节流

### 浏览器的进程和线程

浏览器是多进程的，浏览器每一个打开一个Tab页面(网页)都代表着创建一个独立的进程（至少需要四个，若页面有插件运行，则五个）。渲染进程（浏览器内核）是多线程的，也是浏览器的重点，因为页面的渲染，JS执行等都在这个进程内进行

1. GUI渲染线程

负责渲染浏览器界面,包括解析HTML，CSS，构建DOM树和RenderObject树，布局和绘制等。
当界面需要重绘（Repaint）或由于某种操作引发回流(reflow)时，该线程就会执行。
注意 : GUI渲染线程与JS引擎线程是互斥的

2. JS引擎线程

也称为JS内核，负责解析处理Javascript脚本，运行代码。（例如V8引擎）。
JS引擎一直等待并处理任务队列中任务。一个Tab页中无论什么时候都只有一个JS线程在运行JS程序

3. 定时触发器线程

setInterval和setTimeout所在线程。通过此线程计时完毕后，添加到事件队列中，等待JS引擎空闲后执行

4. 事件触发线程

当一个事件被触发时该线程会把事件添加到事件队列，等待JS引擎的处理
这些事件可来自JS引擎当前执行的代码块如setTimeOut、也可来自浏览器内核的其他线程如鼠标点击、AJAX异步请求等，但由于JS的单线程关系所有这些事件都得排队等待JS引擎处理

5. 异步http请求线程

在XMLHttpRequest连接后是通过浏览器新开一个线程请求。
将检测到状态变更时，如果设置有回调函数，异步线程就产生状态变更事件，将这个回调再放入事件队列中。再由JS引擎执行

### 定时器误差

（1）执行顺序

遇到一个定时器请求，开启定时器线程去计时
计时结束后将回调函数放入到任务队列（宏任务）
等待同步任务执行 waiting… ，在同步任务执行结束后可能还有微任务
同步任务执行结束后取出任务队列中的回调
（2）误差大小

定时器的误差值取决于同步任务的执行时间 + 微任务的执行时间
同步任务执行时间越长，定时器的误差越大

# Promise 【es6】

比如：间隔一秒执行一件事【setTimeout嵌套调用（回调地狱）http（ajax）请求也是】

缺点：可读性差，嵌套地狱、维护能力差

### promise如何解决异步控制的问题

```javascript
// 回调地狱变成如下的链式调用 【代码解析成从上到下从左到右解析控制异步流程】
new Promise() // 同步
    .then() // 异步
    .then() // 异步
    .then() // 异步
// 实例对象后是异步代码
```

### 回调函数 【本身是同步的】

```javascript
function test(fn){
    fn()
}

test(function(){
    console.log('我是回调函数')
})
// 回调函数结构不一一定是异步代码，但异步代码一定是回调函数的结构
```

### Promise的三个状态

**pending** 【等待中】

**fulfilled** 【已完成】

**rejected**【已拒绝】

游戏规则：同一个promise对象同一时间只能存在一种状态,且状态一旦改变就不能再变

如果初始化 new Promise() 没有给定 resolve reject 那就是一直在pending的状态

### Promise为什么能链式调用

```javascript
function MyPromise(){
  
}

MyPromise.protoType.then = function(){
    console.log('链式调用测试')
   	return this
    // return new MyPromise()
}

var p = new MyPromise()
var p1 = p.then()
var p2 = p1.then()
// 函数对象内返回对应新的实例 链式调用的基本原理实现
```

### Promise 中断链式调用

Promise 中断链式调用会触发一次catch的执行

**方法一：**

return Promise.reject('我是中断的原因1')

**方法二：**

return throw('我是中断的原因2')

### Promise 链式调用的特点

1. 只要有then()并且触发了resolve整个链式就会执行到尾，这个过程中第一个回调函数的参数resolve传入的值
2. 后续每个函数都可以使用return返回一个结果，如果没有返回结果的话下一个then中回调函数的参数就是 undefined
3. 如果返回结果是普通变量，那么这个值就是下一个then中回调函数的参数
4. 如果返回结果是一个Promise对象，那么这个Promise对象resolve的结果会变成下一次then中回调函数的参数（多个也是如此）
5. 如果then中传入的不是函数或者未传值，Promise链条并不会中断then的链式调用，并且在这之前最后一次的返回结果，会直接进入离它最近的正确的then中的回调函数做为参数

### Generrator函数 （异步函数过渡期）

```javascript
function * fnName(){
    yield ***
    yield ***
}
// 例子
function * test(){
    var a = yield 1
    var b = yield 2
    var c = a + b
    console.log(c)
} 
var generator = test()
console.log(generator,'generator')
var step1 = generator.next()
console.log(step1,'step1')
var step2 = generator.next()  
console.log(step2,'step2')
```

Generrator函数异步代码同步化（异步控制流程）

### ES7 中 Async 和 Await 

async 和 await 需要成双成对出现 async 可以单独使用 await 只能和 async 一起出现！ 

```javascript
async function test(){
	return 1
}
let res = test()
console.log(res,'res') // 打印出Promise对象

// 例子 
async function test1(){
    console.log(3)
	return 1
}
console.log(1)
test1()
console.log(2)
// 输出 1 3 2 因为async变成了Promise对象

// 例子2
async function test2(){
    console.log(3)
    var a = await 4
	return 1
}
console.log(1)
test2()
console.log(2)
// 解释 test2() ==> Promise 写法
console.log(1)
new Promise((resolve,reject) => {
    console.log(3)
    resolve(4)
}).then(res => {
    console.log(res)
})
console.log(2)
```

这样即可实现精准的异步控制流程

回调地狱（ES5）---> Promise链式调用（ES6） --->  Generrator分布执行（ES6） ---> async 和 await（ES7） 自动异步代码同步化机制。

### 手写简版Promise

```html
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Promise</title>
	</head>
	<body>
		<div>学习 Promise</div>
		<script src="myPromise.js"></script>
		<script>
			console.log('MyPromise - start')
			
			let p = new MyPromise((resolve,reject) => {
				resolve('我是MyPromise的值')
			})
			
			console.log(p,'MyPromise')
			
			p.then(res => {
				console.log(res,'链式调用，第一个then执行')
				return 123
			}).then(res => {
				console.log(res,'链式调用，第二个then执行')
				return MyPromise.resolve('第二个then执行的值')
			}).then(res => {
				console.log(res,'链式调用，第三个then执行')
			})
			
			var p1 = new MyPromise((resolve,reject) => {
				reject('错误的值')
			})
			p1.catch(function(err){
				console.log(err,'err执行')
				return 456
			}).then(res => {
				console.log(res,'catch执行后依旧可以正常then')
			})
			console.log(p1,'测试catch错误')
			
			console.log('MyPromise - end')
		</script>
	</body>
</html>
```

```javascript
function MyPromise(fn) {
	// 全用 es5 语法写
	var _this = this
	this.promiseState = 'pending'
	this.promiseResult = ''
	// 定义一个函数对象用来注册 then 中的 callback
	this.cb = null
	// 定义一个函数对象用来注册 catch 中的 callback
	this.catchCb = null
	// resolve 成功回调
	var resolve = function(val){
		// 更改 promise 的状态和值 只有 pending 时候才可变更
		if(_this.promiseState === 'pending'){
			console.log('源码resolve回调执行',val)
			_this.promiseState = 'fulfilled'
			_this.promiseResult = val
			// 如果 resolve 传的还是一个 Promise对象 或者某一个环节 return了一个 Promise对象
			if(val instanceof MyPromise){
				val.then(function(res){
					// 如果 then 的函数注册了
					if(_this.cb){
						_this.cb(res)
					}
				})
			} else {
				setTimeout(function(){
					console.log('源码处理 异步任务')
					if(_this.cb){
						_this.cb(val)
					}
				})
			}
		}
	}
	// reject 失败回调
	var reject = function(errorVal){
		console.log(errorVal,'源码errorVal')
		if(_this.promiseState === 'pending'){
			_this.promiseState = 'rejeted'
			_this.promiseResult = errorVal
			setTimeout(function(){
				// 如果 catch 的函数注册了
				if(_this.catchCb){
					_this.catchCb(errorVal)
				} else if (_this.cb){
					_this.cb(errorVal)
				} else {
					throw('源码cath is error')
				}
			})
		}
	}
	 
	// 传进来的  resolve reject回调
	if(fn){
		fn(resolve,reject)
	}
}

// Promise 的 resolve方法 返回一个新的 Promise 对象
MyPromise.resolve = function(val){
	return new MyPromise(function(resolve,reject){
		resolve(val)
	})
}

// Promise 的 reject方法 返回一个新的 Promise 对象
MyPromise.reject = function(errValue){
	return new MyPromise(function(resolve,reject){
		reject(errValue)
	})
}

// Promise 的 then方法 返回一个 Promise对象
MyPromise.prototype.then = function(callback){
	console.log('源码then执行')
	var _this = this
	return new MyPromise(function(resolve,reject){
		_this.cb = function(val){
			// 链式调用的场景第一个调用的可能不是catch 
			// 会借助 then将 catch 的信息往下传递
			if(_this.promiseState === 'rejected'){
				reject(val)
			} else {
				// 直接成功 then 的
				var result = callback(val)
				// then 之后 Promise.reject()的这种情况 中断 Promise
				if (result instanceof MyPromise && result.promiseState === 'rejected'){
					result.catch(function(errValue){
						reject(errValue)	
					})
				} else {
					// 正常的 再把值传下去
					resolve(result)
				}
			}
		}
	})
}

// Promise 的 catch方法 也返回一个 Promise对象
// then 是连着的 cath可能不是
MyPromise.prototype.catch = function(callback){
	console.log('源码catch执行')
	var _this = this
	return new MyPromise(function(resolve,reject){
		_this.catchCb = function(errValue){
			var res = callback(errValue)
			// 比如 有return 的值 还需继续正常往下走
			resolve(res)
		}
	})
}

// Promise 的 all 方法
MyPromise.all = function(promiseList){
	let promiseArr = []
	return new MyPromise(function(resolve,reject){
		promiseList.forEach((item,index) => { 
			item.then(function(res) {
				promiseArr[index] = res 
				var flag = promiseList.erery(e => {
					return e.promiseState === 'fulfilled'
				})
				// 是否所有的已完成
				if(flag){
					resolve(promiseArr)
				}
			}).catch(function(err){
				reject(err)
			})
		})
	})
}

// Promise 的 race 方法
MyPromise.race = function(promiseList){
	return new MyPromise(function(resolve,reject){
		promiseList.forEach((item,index) => { 
			item.then(function(res) {
				resolve(res)
			}).catch(function(err){
				reject(err)
			})
		})
	})
}
```

