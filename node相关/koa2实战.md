# koa2实战

### Restful API 

是一种互联网软件的架构原则

### 前端API工具

Apifox：https://www.apifox.cn/?utm_source=baidu_sem1

### postman工具测试API接口

工具下载：https://www.postman.com/

### 常见的Content-Type类型

`application/json`：JSON数据格式，现在非常流行的格式
 `application/x-www-form-urlencoded`：很常见的一种数据格式，post请求中通常默认是这个
 `multipart/form-data`：上传文件时我们需要用到这个格式
 `application/xml`：XML数据格式
 `text/html`：HTML格式
 `text/plain`：纯文本格式
 `image/png`：png图片格式

**application/x-www-form-urlencoded**

x-www-form-urlencoded

浏览器原生的form表单类型，或者说是表单默认的类型

post 将请求参数以key1=value1&key2=value2这种键值对的方式进行组织，并放入到请求体中，其中中文或某些特殊字符，如"/"、","、“:" 等会自动进行URL转码

```javascript
import QS from 'qs'
QS.stringify(data) // 参数处理
```

**multipart/form-data** 

form-data

form表单格式，一般需要上传文件的表单则用该类型，在请求实体里每个参数以------boundary开始，然后是附加信息和参数名，然后是空行，最后是参数内容。多个参数将会有多个boundary块。如果参数是文件会有特别的文件域。最后以------boundary–为结束标识

```javascript
const formData = new FormData();
formData.append('key', 'value');
```

**application/json**

post会将序列化后的json字符串直接塞进请求体中

```javascript
import QS from 'qs'
QS.stringify(data) // 参数处理
```

### http请求方式

GET：读取资源

POST：新建资源

PUT：更新资源

PATCH：资源数据部分更新

DELETE：删除资源

### 常见http状态码

**<u>1xx：相关信息</u>**

<u>**2xx：操作成功**</u>

<u>**3xx：重定向**</u>

| **300 （Multiple Choices）**    | **多种选择， 针对请求，服务器可执行多种操作。服务器可根据请求者 (user agent) 选择一项操作，或提供操作列表供请求者选择** |
| ------------------------------- | ------------------------------------------------------------ |
| **301  （Moved Permanently ）** | **永久移动，请求的网页已永久移动到新位置。服务器返回此响应（对 GET 或 HEAD 请求的响应）时，会自动将请求者转到新位置** |
| **302 （Moved Temporatily）**   | **类似于301，但是移动是临时的。服务器目前从不同位置的网页响应请求，但请求者应继续使用原有位置来进行以后的请求** |
| **303 （See Other）**           | **查看其它位置，请求者应当对不同的位置使用单独的 GET 请求来检索响应时，服务器返回此代码。即该状态码存在的最主要意义是为了处理POST请求重定向到GET请求的情况** |
| **304 （ Not Modified ）**      | **未修改，自从上次请求后，请求的网页未修改过。服务器返回此响应时，不会返回网页内容** |

**<u>4xx：客户端错误</u>**

| 400 (Bad Request)            | 服务器不理解客户端的请求，未做任何处理                       |
| ---------------------------- | ------------------------------------------------------------ |
| **401 (Unauthorized)**       | **用户未提供（没有通过）身份证凭据（比如token失效）**        |
| **403 (Forbidden)**          | **用户通过了身份验证但是没有访问资源权限（比如需要连接内网）** |
| **404 (Not Found)**          | **资源未找到或不可用（比如接口没部署或者请求路径错误）**     |
| **405 (Method Not Allowed)** | **用户通过身份证验证，但是所用的http方法不在它的权限内（比如get但是使用了post 或 方法参数类型与标准不一致）** |

<u>**5xx：服务器错误**</u>

| **500 (Internal Server Error)** | **服务器内部错误（一般后端排查，前端考虑请求头接收形式content-Type）** |
| ------------------------------- | ------------------------------------------------------------ |
| **501 (Not Implemented)**       | **尚未实施，或请求格式错误（前端一般考虑写的ajax中的type:"post/get"是否出错或者from表单中的method:"post/get"是否书写错误）** |
| **502 （Bad Gateway）**         | **错误网关， 服务器作为网关或代理，从上游服务器收到无效响应** |
| **503 （Service Unavailable）** | **服务不可用， 服务器目前无法使用（由于超载或停机维护）。通常，这只是暂时状态** |
| **504 （Gateway Timeout）**     | **网关超时，服务器作为网关或代理，但是没有及时从上游服务器收到请求** |

### koa2请求实体

get请求 delete请求

```javascript
ctx.query // /user?name=bob 

/:name // 请求地址 动态名字
ctx.params // /user/bob
```

post请求、 put请求

```javascript
ctx.request // 请求体
```

### 解决跨域

```javascript
const koa = require('koa')
const cors = require('koa2-cors')
const app = new koa()
app.use(cors({
  origin: function (ctx) {
    return '*';
  },
  exposeHeaders: ['WWW-Authenticate', 'Server-Authorization'],
  maxAge: 5,
  credentials: true,
  allowMethods: ['GET', 'POST', 'DELETE', 'PUT'],
  allowHeaders: ['Content-Type', 'Authorization', 'Accept'],
})) // 中间件 都是一个实体
```

### 文件上传

文档：https://github.com/expressjs/multer

```javascript
const koa = require('koa')
const cors = require('koa-multer')
const upload = multer({ dest: 'uploads/' })
const app = new koa()

app.post('/profile', upload.single('avatar'), function (req, res, next) {
  // req.file 是 `avatar` 文件的信息
  // req.body 将具有文本域数据，如果存在的话
})

app.post('/photos/upload', upload.array('photos', 12), function (req, res, next) {
  // req.files 是 `photos` 文件数组的信息
  // req.body 将具有文本域数据，如果存在的话
})

const cpUpload = upload.fields([{ name: 'avatar', maxCount: 1 }, { name: 'gallery', maxCount: 8 }])
app.post('/cool-profile', cpUpload, function (req, res, next) {
  // req.files 是一个对象 (String -> Array) 键是文件名，值是文件数组
  //
  // 例如：
  //  req.files['avatar'][0] -> File
  //  req.files['gallery'] -> Array
  //
  // req.body 将具有文本域数据，如果存在的话
})
```

### 表单验证

koa-bouncer

```javascript
const bouncer = require('koa-bouncer');
app.use(bouncer.middleware());

const val = async (ctx, next) => {
    ctx.validateBody('name')
    .required('请输入用户名')
    .isLength(6, 12, '用户名长度应该为6~12')
    .isString()
    .trim()
    next();
}


router.post('/', val, ctx => {
    console.log('POST /users');
    const { body: user } = ctx.request;
    user.id = users.length + 1; // 自增
    users.push(user);
    ctx.body = {
        ok: 1
    }
});
```

### 图形验证码

- captchapng
- ccap
- trek-captcha （举例）
- svg-captcha - 实现图片识别类验证码
- gm - 实现滑动解锁
- GraphicsMagick - 图片处理
- ImageMagick - 图片处理
- nodemailer - 发送邮件

```javascript
const svgCaptcha = require('svg-captcha');

router.get('/code', async (ctx) => {
    // 生成加法形式的验证码
    // var captcha = svgCaptcha.createMathExpr({
    //     fontSize:50,
    //     width:120,
    //     height:34,
    //     background:'#cc9966'
    // });
    
    // 生成验证码
    var captcha = svgCaptcha.create({
        size: 4,
        fontSize: 50,
        width: 120,
        height: 34,
        background: '#cc9966'
    });
    // 保存生成的验证码结果
    ctx.session.code = captcha.text
    // 设置响应头
    ctx.response.type = 'image/svg+xml';
    ctx.body = captcha.data;
});
```

### 开启Gzip 加快网页加载速度

koa-compress

```javascript
const koa = require('koa')
const compress = require('koa-compress')
const app = new koa()
const opt = {threshold: 2048}
app.use(compress(opt))
```

### 生成token

jsonwebtoken

```javascript
import * as crypto from 'crypto';
import * as jwt from 'jsonwebtoken';
async login(ctx){
 // 从数据库中查找对应用户
 const user = await userRespository.findOne({
  where: {
   name: user.name
  }
 });
    
 // 加严【盐】处理 【数据更安全】   
 const secret = 'my_app_secret';
    
 // 密码加密
 const psdMd5 = crypto
  .createHash('md5')
  .update(user.password)
  .digest('hex');
 // 比较密码的md5值是否一致 若一致则生成token并返回给前端
 if (user.password === psdMd5) {
  // 生成token
  token = jwt.sign(user, secret, { expiresIn: '1h' });
  //响应到前端
  ctx.body = {
   token
  }
 }
}
```

### 数据库

数据库可以定义为存储数据的库；而从广义上说，数据库是一个长期存储在计算机内、有组织的、可共享的、统一管理的大量数据的集合 

数据库可分为关系型数据库、非关系型数据库；而SQL则是关系型数据库，并且具有数据定义、数据操纵和数据控制功能，是关系代数和关系演算的结合，也是关系型数据库的标准语言

**sql** 

**mongoose  【MongoDB】【Key-Value数据库】**

**Redis 【Key-Value数据库】**