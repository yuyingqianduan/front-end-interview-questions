# vue服务端渲染

### 什么是服务器渲染（SSR）

vue.js是构建客户端应用框架，默认情况下可在浏览器输出vue组件，进行生成DOM 和 操作 DOM 同样也可以将同一个组件渲染为服务器端的 html 字符串，将他们发送到浏览器最后将这些静态标记激活为客户端上完全可交互的应用程序

服务器渲染的vue.js应用程序又叫 同构 或者 通用  程序大部分代码都可在服务器 和 客户端运行

### vue SSR现有的解决方案

Nuxt ： https://www.nuxtjs.cn/guide （推荐）

vue-server-renderer

prerender-spa-plugin（预渲染）

### 客户端/服务端渲染区别和原理

客户端渲染：vue通过虚拟dom在浏览器输出vue文件进行生成 dom 和 操作dom

服务端渲染：将一个组件渲染为服务器的html字符串（只有html结构）将它们发送到浏览器结合css、js将这些静态标记加上样式进行交互激活为客户端上完全可交互的应用程序

### SSR的优缺点

优点：更好的SEO 

缺点：开发条件有限（生命周期beforCreate、create生命周期服务端不执行，window、document、addEventListener事件绑定等不存在）对服务器要求比较高