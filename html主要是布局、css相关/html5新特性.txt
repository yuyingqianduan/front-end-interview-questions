html5新特性
1、语义化标签：header、nav、aside、section、main、footer等
2、新的表单输入类型： type="date、time、tel、number、search、email等"
3、媒体标签：audio和video
4、本地存储：localStorage、sessionStorage
5、Canvas绘图、SVG绘图
6、地理定位
function getLocation() {
       if (navigator.geolocation) {
              navigator.geolocation.getCurrentPosition(showPosition);
       } else {
              console.log('Geolocation is not supported by this browser')
       }
}
7、拖放API
<div draggable="true"></div>
8、Web Worker
Web Worker可以通过加载一个脚本文件，进而创建一个独立工作的线程，在主线程之外运行
// worker.js
onmessage =function (evt){
    var d = evt.data;//通过evt.data获得发送来的数据
    postMessage( d );//将获取到的数据发送会主线程
}
9、WebSocket
 WebSocket协议为web应用程序客户端和服务端之间提供了一种全双工通信机制。
（1）握手阶段采用HTTP协议，默认端口是80和443
（2）建立在TCP协议基础之上，和http协议同属于应用层
（3）可以发送文本，也可以发送二进制数据。
（4）没有同源限制，客户端可以与任意服务器通信。
（5）协议标识符是ws（如果加密，为wss），如 ws://localhost:8023

10、新增contenteditable属性 （任何dom节点只要加上contenteditable="true"就可以变得可编辑）