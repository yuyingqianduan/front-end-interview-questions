/*
 * @Description: 类型操作符
 * @Version: 2.0
 * @Autor: lhl
 * @Date: 2020-11-07 20:04:30
 * @LastEditors: lhl
 * @LastEditTime: 2020-11-07 20:44:45
 */
let colors = {
    color1: 'red',
    color2:'blud'
}

// typeof 跟的值
let clorlObj = typeof colors;

interface Person {
    age: number,
    name: string
}

// keyof跟的类型
type userInfo  = keyof Person

// in for in 遍历
 
// 类型保护 typeof  instanceof
function toUpperCase(str:string | string[]){
    if(typeof  str === 'string'){
        str.toUpperCase()
    }else {
        str.push() 
    }
}

