/*
 * @Description: ts命名空间 命名空间一个最明确的目的就是解决重名问题
 * @Version: 2.0
 * @Autor: lhl
 * @Date: 2021-05-02 21:03:38
 * @LastEditors: lhl
 * @LastEditTime: 2021-05-02 21:18:27
 */
var nameTest;
(function (nameTest) {
    var People = /** @class */ (function () {
        function People() {
            this.name = 'alien';
        }
        People.prototype.setName = function () {
            console.log('我是alien');
        };
        return People;
    }());
    nameTest.People = People;
})(nameTest || (nameTest = {}));
var nameTest2;
(function (nameTest2) {
    var People = /** @class */ (function () {
        function People() {
            this.name = 'bob';
        }
        People.prototype.setName = function () {
            console.log('我是bob');
        };
        return People;
    }());
    nameTest2.People = People;
})(nameTest2 || (nameTest2 = {}));
var people1 = new nameTest.People();
var people2 = new nameTest2.People();
people1.setName();
