# vue最佳实践

### 箭头函数和普通函数

```javascript
function test(){
	console.log(arguments, '普通函数') // Arguments(3) [1, 2, 3]
}

let a = () => {
	console.log(arguments,'箭头函数') //  arguments is not defined
}
test(1,2,3)
a(1,2,3)
```

```javascript
let b = (...rest) => {
	console.log(rest,'剩余参数') // [1, 2, 3] '剩余参数'
}
b(1,2,3)
```

箭头函数在作用域下没有arguments，箭头函数不绑定arguments，取而代之用rest参数…解决

箭头函数不能用于构造函数，不能使用new

在普通函数中，this总是指向调用它的对象，如果用作构造函数，this指向创建的对象实例

箭头函数this继承上层作用域（上下文的 this）

箭头函数不能使用new生成实例，因为箭头函数没有prototype,而construct在prototype里面

箭头函数内不能用yield且不能用作Generator函数，而普通函数可以

### 历史背景

angular 09年谷歌团队研发 凉凉了国内环境...基本主流react、vue了

react  Facebook团队 2013年用户体验好

vue 2014正式开源 上手快用户体验好 尤雨溪（国人框架）

### vue基础语法

原始语法、双向绑定

```javascript
<%= value %>     
{{value}}
```

```vue
<div id="app">
	<p>{{msg}}</p>
	<p>{{1+1}}</p>
	<p>{{ 1===2 ? '这是真的' : '这是假的'}}</p>
	<p>{{getTxt()}}</p>
	<p>{{msg.split('.').reverse().join('.')}}</p>
	<p v-html="txt"></p>
	<input type="text" v-model="inpVal">
	<p>双向绑定的值：{{inpVal}}</p>
	<p v-show="isShow">{{inpVal}} 原理 display属性 频繁切换的话v-show</p>
	<p v-if="isShow">v-if dom的创建和销毁</p>
	<p v-else>看不到我</p>
	<button @click="handleClick">点击事件</button>
	<a :href="goLick">属性绑定 v-bind :xxx属性 :src :class等</a>
	<input type="text" @keyup.up="handleKeyBoard">
	<p v-for="(item,index) in list" :key="item">无需外层可用包裹一层template{{item,index}} -- {{index}}</p>
	<p>{{people}}</p>
	<p>这个价格是：{{price | priceEnmu('￥') }}</p>
	<p>这个价格是：{{price | priceEnmu('$') }}</p>
</div>
<script src="https://cdn.bootcdn.net/ajax/libs/vue/2.6.2/vue.js"></script>
<script>
	let vm = new Vue({
		el: '#app', // 可以是 id 或者 dom
		data(){
			return {
				age: 28,
				name: 'bob',
				price: 20,
				msg: 'vue2.6',
				txt: '<span>666</span>',
				inpVal: '双向绑定',
				isShow: true,
				goLick: 'https://www.baidu.com/',
				list: [1,2,3,4,5,'循环时候索引不更改数据不操作数据的可用index其他不建议']
			}
		},
		computed:{
			people(){
				return `${this.name} 今年 ${this.age} 岁了`
			}
		},
		watch:{
			// 写法一
			// inpVal(newVal,oldVal){
			// 	console.log(newVal,oldVal)
			// }
			inpVal:{
				handler(newVal,oldVal){
					console.log(newVal,oldVal)
				},
				deep: true, // 如果是 对象 或者数组就要深度监听
				immediate: true // 初始化是否执行
			}
		},
		// 过滤器（作用比较小一般方法即可处理） 局部过滤器 or 全局过滤器 otherParams 第二个是参数
		filters:{
			priceEnmu(val, otherParams){
				// return `$ ${val}`
				return `${otherParams} ${val}`
			}
		},
		methods:{
			getTxt(){
				return 'vue2版本的方法渲染'
			},
			handleClick(){
				// @自定义事件名 如果是自定义事件的话
				console.log(666)
			},
			// 具体的参考官网
			handleKeyBoard(e){
				// 按键码 比如 up down left right 等
				// 还有一些修饰符 stop prevent native 等
				console.log(e,'e')
			}
		}
	})
	// 全局过滤器
	Vue.filter = ('priceEnmu1', (val, otherParams) => {
		return `${otherParams} ${val}`
	})
	console.log(vm) // 实例
	console.log(vm.$data)
	console.log(vm.msg)
</script>
```

### computed 和 watch 的区别

computed（计算属性）只有当被计算的值发生改变时候才会触发，并且具有缓存效果除非依赖的响应式属性变化才会重新计算了。默认只有getter方法

watch（监听属性）监听某一个值的变化做对应的操作 比如http请求接口

### 组件基础

组件：是对数据和方法的简单封装

按组件注册方式分类：可以分为`全局组件`和`局部组件`

按组件有无自己的状态分类：可以分为`函数式（无状态）组件`和`普通（无状态）组件`

按组件是否动态分类：可以分为`动态组件`和`普通（非动态）组件`

按组件是否异步分类：可以分为`异步组件`和`普通（非异步）组件`

按组件是否循环引用分类：可以分为`递归组件`和`普通（非递归）组件`

#### 全局组件（项目里多次用到的 > 3次的使用频率或者更高可以考虑作为全局组件）

比如常用的 dialog 组件，search 组件，toast 组件，message 组件，分页组件等

```javascript
// 注册
Vue.component("my-component-name", {
  /****/
});
// 使用
<my-component-name></my-component-name>
```

#### 局部组件（子组件）

一般情况下的组件都应该是局部组件，这样会极大的减少构建应用后的代码体积，但是对于频繁使用的组件就显得麻烦了，所以建议，组件使用频率低，组件比较大的时候注册为局部组件。比如 table 组件，chart 组件等

```javascript
// 注册
import ComponentA from "./ComponentA.vue";
export default {
  components: {
    ComponentA
  }
  // ...
};

// 使用
<component-a></component-a>
```

#### 函数式组件

一般是无状态 (没有响应式数据)的组件可以注册成函数式组件，当一个组件是一个函数式组件的时候，它没有管理任何状态，也没有监听任何传递给它的状态，也没有生命周期方法，没有this实例。只接受一些 props 的函数，渲染开销低性能得到优化（一般为纯展示数据类型组件）

```javascript
// 形式一 jsx模式
<script>
  export default {
    name: "FunctionComponent",
    functional: true,
    props: {
      title: {
        type: String,
        required: true
      }
    },
    render(h, { props }) {
      const { title } = props;
      return (
        <div>
          <p> 我是函数式组件</p>
          <p>props {title}</p>
        </div>
      );
    }
  };
</script>

// 形式二 template模式 
<template functional>
  <div>
    {{ props.title }}
  </div>
</template>
<script>
  export default {
    functional: true,
    props: {
      title: {
        default: "",
        type: String
      },
    }
  };
</script>

// 形式三 render模式 
Vue.component('funcCmop', {
    functional: true,
    props:{
        
    },
    render(createElement,context){
        const {props,children,slots,scopedSlots,data,listeners,injections,parent} = context
    }
})

// 使用 
<function-component title="函数式组件" />
```

#### 动态组件

一般是是需要组件之间切换的情况下，比如tab栏目或者同一个视图区域需要依据条件展示不同的数据或者不同的视图，动态切换组件的显示与隐藏

```javascript
// 结合缓存一起使用来保存状态 看情况是否需要，一般情况下需要（不会销毁组件也不会重新创建，会被缓存或者激活）
<keep-alive>
	<component :is='component' />
</keep-alive>

当组件被缓存的时候，会自动触发组件的 deactivated 生命周期函数
当组件被激活的时候，会自动触发组件的 activated 生命周期函数
当组件第一次被创建的时候，先触发 created，再触发 activated
之后被激活时只触发 activated，不再触发 created

keep-alive 组件的属性 （通过组件的name 去匹配）
include 用来指定：只有名称匹配的组件会被缓存。多个组件名之间使用英文的逗号分隔。
exclude 属性指定谁不被缓存，二者只能指定一个
```

#### 异步组件 (一般用来优化性能)

在大型应用中，我们可能需要将应用分割成小一些的代码块，并且只在需要的时候才从服务器加载一个模块。Vue 允许你以一个工厂函数的方式定义你的组件，这个工厂函数会异步解析你的组件定义。Vue 只有在这个组件需要被渲染的时候才会触发该工厂函数，且会把结果缓存起来供未来重渲染

比如 大家使用最多的 是在 Vue Router 里使用，异步组件结合 Webpack 的代码分割功能，可以轻松实现路由组件的懒加载。

```javascript
Vue.component('async-webpack-example', function (resolve) {
  // 这个特殊的 `require` 语法将会告诉 webpack
  // 自动将你的构建代码切割成多个包，这些包
  // 会通过 Ajax 请求加载
  require(['./my-async-component'], resolve)
})

Vue.component(
  'async-webpack-example',
  // 这个动态导入会返回一个 `Promise` 对象。
  () => import('./my-async-component')
)

// 当使用局部注册的时候，你也可以直接提供一个返回 Promise 的函数
new Vue({
  // ...
  components: {
    'my-component': () => import('./my-async-component')
  }
})

// 2.3.0+ 新增 处理加载状态
// 注意如果你希望在 Vue Router 的路由组件中使用上述语法的话，你必须使用 Vue Router 2.4.0+ 版本
const AsyncComponent = () => ({
  // 需要加载的组件 (应该是一个 `Promise` 对象)
  component: import('./MyComponent.vue'),
  // 异步组件加载时使用的组件
  loading: LoadingComponent,
  // 加载失败时使用的组件
  error: ErrorComponent,
  // 展示加载时组件的延时时间。默认值是 200 (毫秒)
  delay: 200,
  // 如果提供了超时时间且组件加载也超时了，
  // 则使用加载失败时使用的组件。默认值是：`Infinity`
  timeout: 3000
})
```

#### 递归组件

在组件的自身再调用组件的自身，比如多层级的相同数据结构时候 菜单栏 树组件等

```javascript
<transition name="fade">
	<NavItem v-for="item in list" :key="item.id" :navItem="item" />
</transition>

 name:'NavItem', // 每个组件的name值是为了帮助我们实现递归的
```

### data必须是一个函数

一个组件的data选项必须是一个函数因为每个实例需要维护一份被返回对象的独立拷贝避免污染数据源，源码执行 initData 函数时会将其作为工厂函数都会返回全新data对象 Object是引用数据类型，如果不用function 返回,每个组件的data 都是内存的同一个地址,一个数据改变了其他也改变了, javascipt只有函数构成作用域, data是一个函数时,每个组件实例都有自己的作用域,每个实例相互独立,不会相互影响。

### vue中el，template，render的优先级

```vue
<div id="app">
	{{msg}}
</div>

<script>
const app = new Vue({
    el: '#app',
    template: `<div>template txt</div>`,
    render: (h) => {
        return h('div', 'render txt')
    },
    data() {
        return {
            msg: 'vue'
        }
    }
})
// 页面显示 render txt
</script>

// render > template > el  
```

```javascript
/*src/platforms/web/entry-runtime-with-compiler.js*/
const mount = Vue.prototype.$mount
Vue.prototype.$mount = function (
  el?: string | Element,
  hydrating?: boolean
): Component {
  el = el && query(el)

  /* istanbul ignore if */
  if (el === document.body || el === document.documentElement) {
    process.env.NODE_ENV !== 'production' && warn(
      `Do not mount Vue to <html> or <body> - mount to normal elements instead.`
    )
    return this
  }

  const options = this.$options
  // resolve template/el and convert to render function
  // 从这里开始
  if (!options.render) {
    // 不存在 render 使用 template
    let template = options.template
    if (template) {
      if (typeof template === 'string') {
        if (template.charAt(0) === '#') {
          template = idToTemplate(template)
          /* istanbul ignore if */
          if (process.env.NODE_ENV !== 'production' && !template) {
            warn(
              `Template element not found or is empty: ${options.template}`,
              this
            )
          }
        }
      } else if (template.nodeType) {
        template = template.innerHTML
      } else {
        if (process.env.NODE_ENV !== 'production') {
          warn('invalid template option:' + template, this)
        }
        return this
      }
    } else if (el) {
      // 不存在 template 使用 el
      template = getOuterHTML(el)
    }
    if (template) {
      /* istanbul ignore if */
      if (process.env.NODE_ENV !== 'production' && config.performance && mark) {
        mark('compile')
      }
      // 最后转成render函数 添加到options上
      const { render, staticRenderFns } = compileToFunctions(template, {
        outputSourceRange: process.env.NODE_ENV !== 'production',
        shouldDecodeNewlines,
        shouldDecodeNewlinesForHref,
        delimiters: options.delimiters,
        comments: options.comments
      }, this)
      options.render = render
      options.staticRenderFns = staticRenderFns

      /* istanbul ignore if */
      if (process.env.NODE_ENV !== 'production' && config.performance && mark) {
        mark('compile end')
        measure(`vue ${this._name} compile`, 'compile', 'compile end')
      }
    }
  }
  return mount.call(this, el, hydrating)
}
```

### 组件通信（传值）

vm.$emit( event, arg )  触发当前实例上的事件

vm.$on( event, fn );   监听event事件后运行 fn；

#### props 父传子  &&   $emit  $on 子传父

```vue
<template>
  <div id="app">
    <SonComp :msg="sendToChildMsg" @handelCallBack="handleRecevieChildMsg" />
    <p>{{ recevieChildMsg }}</p>
  </div>
</template>

<script>
import SonComp from "./components/son.vue";
export default {
  name: "App",
  components: {
    SonComp,
  },
  data() {
    return {
      sendToChildMsg: "父组件传送给子组件的数据",
      recevieChildMsg: "",
    };
  },
  methods: {
    handleRecevieChildMsg(msg) {
      this.recevieChildMsg = msg;
    },
  },
};
</script>

// 子组件
<template>
  <div>
    <p>{{ msg }}</p>
    <button @click="handleSendFatherMsg">
      点击就可以把子组件的值传给父组件了
    </button>
  </div>
</template>

<script>
export default {
  name: "childComp",
  // props: ["msg"],
  props: {
    msg: {
      type: String, // 传过来的数据类型
      required: true, // 校验是否必传
      default: "", // 默认值
    },
  },
  data() {
    return {
      SendFatherMsg: "子组件传递给父组件的值",
    };
  },
  methods: {
    handleSendFatherMsg() {
      this.$emit("handelCallBack", this.SendFatherMsg);
    },
  },
};
</script>
```

#### ref 子组件通信

```vue
<template>
  <div>
    <refComp ref="childRef" />
    <p>{{ msg }}</p>
    <p @click="getChildTex">获取值</p>
  </div>
</template>

<script>
import refComp from "./refComp.vue";
export default {
  name: "FatherComp",
  components: {
    refComp,
  },
  data() {
    return {
      childRef: null,
      msg: "",
    };
  },
  mounted() {
    // ref只能在dom挂载完毕才能获取到
    this.msg = this.$refs.childRef.name;
  },
  methods: {
    getChildTex() {
      this.$nextTick(() => {
        // 通过 ref 调用子组件的方法
        this.$refs.childRef.getTex();
      });
    },
  },
};
</script>

// 子组件
<template>
  <div>ref 传值</div>
</template>

<script>
export default {
  name: "RefComp",
  data() {
    return {
      name: "bob",
    };
  },
  methods: {
    getTex() {
      alert(1);
    },
  },
};
</script>
```

#### this.$parent

```javascript
// 直接在子组件中使用this.$parent.XX，不需要做任何多余操作
this.$parent.xxx [属性 or 方法]
```

本地缓存传值 （localStorage 和 sessionStorage）

```javascript
localStorage.setItem(key,value) // 缓存指定key 或者 localStorage.key = value 
localStorage.getItem(key) // 获取指定key
localStorage.removeItem(key) // 删除指定key
localStorage.clear() // 清空所有
JSON.stringify() // 将json格式的数据（JavaScript 对象）转换成JSON格式的字符串
JSON.parse() // 将JSON格式的字符串转换成JSON对象进行处理
sessionStorage.setItem(key,value) // 缓存指定key 或者 sessionStorage.key = value 
sessionStorage.getItem(key) // 获取指定key
sessionStorage.removeItem(key) // 删除指定key
sessionStorage.clear() // 清空所有

// 异
sessionStorage仅在当前会话下生效，当你关闭页面或浏览器后你存储的sessionStorage数据会被清除
localStorage生命周期是永久，储存的信息是永远不会消失的，除非你自己主动清除localStorage信息。
（作用域维度）
localStorage只要在相同的协议、相同的主机名、相同的端口下，就能读取/修改到同一份localStorage数据
sessionStorage比localStorage更严苛一点，除了协议、主机名、端口外，还要求在同一窗口（也就是浏览器的标签页）下
不同页面或标签页间无法共享sessionStorage的信息。页面及标 签页仅指顶级窗口，如果一个标签页包含多个iframe标签且他们属于同源页面
// 同
共同点是它们的数据存储大小一般都在5mb
都是h5 API

// 额外的cookie 
浏览器一般只允许存放300个Cookie，每个域不超过50个cookie，每个Cookie的大小限制为4KB
```

#### bus传值

```javascript
// main.js
import Vue from "vue";
import App from "./App.vue";

Vue.config.productionTip = false;

// 事件总线 一般挂载在原型或者单独一个文件引用
Vue.prototype.$bus = new Vue();

new Vue({
  render: (h) => h(App),
}).$mount("#app");

// 父组件
<template>
  <div>
    <childOneVue />
    <childTwoVue />
  </div>
</template>

<script>
import childOneVue from "./childOne.vue";
import childTwoVue from "./childTwo.vue";
export default {
  name: "FatherComp",
  components: {
    childOneVue,
    childTwoVue,
  },
};
</script>

// 第一个子组件
<template>
  <div>
    <p>我是第一个子组件</p>
    <button @click="sendFirstChildMsg">发送给兄弟组件的信息</button>
  </div>
</template>

<script>
export default {
  data() {
    return {
      firstChildMsg: "第一个子组件的data值",
      sendToBrotherMsg: "这是给兄弟组件传的值",
    };
  },
  methods: {
    sendFirstChildMsg() {
      console.log(this.$bus, "this.$bus");
      this.$bus.$emit("handelBrotherMsg", this.sendToBrotherMsg);
    },
  },
};
</script>

// 第二个子组件
<template>
  <div>
    <p>我是第二个子组件</p>
    <P>{{ recevieBrotherMsg }}</P>
  </div>
</template>

<script>
export default {
  data() {
    return {
      firstChildMsg: "第二个子组件的data值",
      recevieBrotherMsg: "",
    };
  },
  mounted() {
    this.$bus.$on("handelBrotherMsg", (msg) => {
      this.recevieBrotherMsg = msg;
      console.log(msg, "接收来自兄弟组件的值");
    });
  },
};
</script>
```

#### $attrs、$listeners:父子组件传值

$attrs 包含了父作用域不作为prop被识别（且获取）的attribute绑定（class和style除外）。当一个组件没有生命任何prop时，这里会包含所有父作用域的绑定（class和style除外），并且可以通过v-bind="$attrs"传入内部组件–在创建高级别的组件时非常有用。

通俗地来说就是如果从父组件传过来的值，没有在子组件中被接收，那么这些值就会被存在$attrs对象中。

$listeners 包含了父作用域中的（不包含.native修饰器的）v-on事件监听器。它可以通过v-on="$listeners"传入内部组件–在创建更高层次的组件时非常有用

通俗地来说就是可以从孙子组件发送事件到父子组件中

```javascript
// 父组件
<template>
  <div>
    <attrsTestVue :name="name" :age="age" @event="customEvent" />
  </div>
</template>

<script>
import attrsTestVue from "./attrsTest.vue";
export default {
  name: "FatherComp",
  components: {
    attrsTestVue,
  },
  data() {
    return {
      name: "bob",
      age: 29,
    };
  },
  methods: {
    customEvent(msg) {
      console.log("从grandSon组件发送过来的数据", msg);
    },
  },
};
</script>

// 子组件
<template>
  <div>
    <p>vue使用$attrs传值</p>
    <p>子组件：{{ otherMsg }}</p>
    <grandSonVue v-bind="$attrs" v-on="$listeners" />
  </div>
</template>

<script>
import grandSonVue from "./grandSon.vue";
export default {
  name: "AttrsTest",
  components: { grandSonVue },
  data() {
    return {
      otherMsg: "",
    };
  },
  created() {
    console.log(this.$attrs, "额外的没使用props接收的值");
    this.otherMsg = this.$attrs
      ? `${this.$attrs.name} 今年 ${this.$attrs.age}岁了`
      : "";
  },
};
</script>

// 孙组件
<template>
  <div>
    <p>孙子组件：{{ grandsonReciveMsg }}</p>
    <button @click="handleClick">孙子组件向上发送事件</button>
  </div>
</template>

<script>
export default {
  name: "GrandSon",
  data() {
    return {
      grandsonReciveMsg: "",
    };
  },
  created() {
    console.log("grandson", this.$attrs);
    this.grandsonReciveMsg = this.$attrs
      ? `${this.$attrs.name} 今年 ${this.$attrs.age}岁了`
      : "";
  },
  methods: {
    handleClick() {
      this.$emit("event", this.grandsonReciveMsg);
    },
  },
};
</script>
// provide和inject只能从父组件向孙组件传值，不能从孙组件向父组件传值，这种情况就需要使用$attrs和$listeners来实现了
```

#### provide和inject：父子组件或者爷孙组件传值

```javascript
// 父组件
<template>
  <div>
    <p>父组件</p>
    <provideSonVue />
    <provideGrandSonVue />
  </div>
</template>

<script>
import provideSonVue from "./provideSon.vue";
import provideGrandSonVue from "./provideGrandSon.vue";
export default {
  name: "ProvideFather",
  components: { provideSonVue, provideGrandSonVue },
  data() {
    return {
      name: "vue inject自顶向下注入数据",
    };
  },
  provide() {
    return {
      name: this.name,
    };
  },
};
</script>

// 子组件
<template>
  <div>
    <p>子组件</p>
    <p>子组件接收来自父组件的值：{{ name }}</p>
  </div>
</template>

<script>
export default {
  name: "ProvideSon",
  inject: ["name"],
};
</script>

// 孙组件
<template>
  <div>
    <p>孙组件</p>
    <p>孙组件接收来自父组件的值：{{ name }}</p>
  </div>
</template>

<script>
export default {
  name: "ProvideGrandSon",
  inject: ["name"],
};
</script>
// 这种方法传递过来的数据是没有响应性的，当你改变父组件中的name时，子组件中接收的name并不会改变.
// 官方解释：provide 和 inject 绑定并不是可响应的。这是刻意为之的。然而，如果你传入了一个可监听的对象，那么其对象的 property 还是可响应的
// 如果需要变成一个响应式 方式一 直接传递对象 方式二 直接包装一下用方法
provide() {
    return {
      newName: () => this.name
    }
}

// 子组件
inject: ['newName'],
computed: {
   newNameTxt() {
     return this.newName()
   }
}

// 使用
<h2>{{ newNameTxt }}</h2> <!-- 推荐使用这种方法 -->
<h2>{{ newName() }}</h2>
```

#### 插槽传值（匿名插槽、具名插槽、作用域插槽）

插槽(Slot)是vue为组件的封装者提供的能力。允许开发者在封装组件时，把不确定、希望由用户指定的部分定义为插槽。

`v-slot` 指令自 Vue 2.6.0 起被引入，提供更好的支持 `slot` 和 `slot-scope` attribute 的 API 替代方案。`v-slot` 完整的由来参见这份 [RFC](https://github.com/vuejs/rfcs/blob/master/active-rfcs/0001-new-slot-syntax.md)。在接下来所有的 2.x 版本中 `slot` 和 `slot-scope` attribute 仍会被支持，但已经被官方废弃且不会出现在 Vue 3 中

定义插槽区域的语法

```javascript
<slot></slot>
```

- vue 官方规定，每一个slot插槽，都要有一个name名称

- 如果不指定，name的默认值是defalut

- 默认情况下，在使用组件的时候，提供的内容都会被填充到名字 为default的插槽中

- v-slot这个指令只能用在components和<template>中，不能直接用在元素标签上

- <template>是一个虚拟的标签，只是起到一个包裹的作用，不会被渲染为一个具体的元素，在页面中看不到真实的元素

```javascript
// 父组件
<template>
  <div>
    <p>插槽测试页面</p>
    <slotNoNameVue>
      <p>这里是封装组件放在匿名插槽里面的不确定的内容</p>
    </slotNoNameVue>
    <slotNameVue>
      <!-- v-slot 只能添加在 <template> 上 v-slot:default会渲染默认的插槽 -->
      <!-- v-slot template标签 语法： v-slot:插槽名称  或者是简写 #插槽名称-->
      <!-- slot 普通标签语法： name=插槽名称 -->
      <template #test1>
        <h1>这里是封装组件放在具名插槽里面的不确定的内容</h1>
      </template>
      <template v-slot:test>
        <h1>这里是封装组件放在具名插槽里面的不确定的内容</h1>
      </template>
      <template v-slot:default>
        <h1>这里是封装组件放在具名默认插槽里面的不确定的内容</h1>
        <div name="test2">这里是封装组件放在具名默认插槽里面的不确定的内容</div>
      </template>
    </slotNameVue>
    <slotScopeVue>
      <!-- 父级作用域中 v-slot 来定义我们提供的插槽 prop 的名字 -->
      <!-- 语法 v-slot:插槽名="变量名" or #插槽名="变量名" -->
      <template v-slot:default="slotProps">
        {{ slotProps.user.firstName }} {{ slotProps.user.lastName }}
      </template>
      <template v-slot:getname="slotProps">
        {{ slotProps.sex }}
      </template>
    </slotScopeVue>
  </div>
</template>

<script>
import slotNoNameVue from "./slotNoName.vue";
import slotNameVue from "./slotName.vue";
import slotScopeVue from "./slotScope.vue";
export default {
  name: "SortFather",
  components: {
    slotNoNameVue,
    slotNameVue,
    slotScopeVue,
  },
};
</script>

// 匿名插槽 普通插槽 默认插槽
<template>
  <div>
    <p>匿名插槽 普通插槽 默认插槽</p>
    <!-- 方式一 -->
    <slot></slot>
    <!-- 方式二 -->
    <slot name="default"></slot>
  </div>
</template>

<script>
export default {
  name: "SlotNoName",
};
</script>

// 具名插槽
<template>
  <div>
    <p>具名插槽</p>
    <slot name="test"></slot>
    <slot name="test1"></slot>
    <slot name="test2"></slot>
    <slot name="default"></slot>
  </div>
</template>

<script>
export default {
  name: "SlotName",
};
</script>

// 作用域插槽
<template>
  <div>
    <p>作用域插槽</p>
    <!-- 绑定在 <slot> 元素上的 attribute 被称为插槽 prop -->
    <slot :user="user">{{ user.firstName }} {{ user.lastName }}</slot>
    <slot name="getname" :sex="sex"> {{ sex }}</slot>
  </div>
</template>

<script>
export default {
  name: "SlotScope",
  data() {
    return {
      user: {
        firstName: "lan",
        lastName: "bobo",
      },
      sex: "男",
    };
  },
};
</script>
```

#### vuex传值（下面单独vuex章节讲解吧）

### 生命周期

### VueRouter

#### vue路由文档

https://v3.router.vuejs.org/zh/（3.x vue2之前）

https://router.vuejs.org/zh/installation.html（4.x vue3）

#### 安装

```javascript
npm install vue-router@3 （3.x）
npm install vue-router@4 （4.x）// vue3出来一般默认是4.x这个了
vue add router （Vue CLI）
yarn add vue-router/vue-router@4
```

```html
router-link

<!-- 使用 router-link 组件来导航. -->
<!-- 通过传入 `to` 属性指定链接. -->
<!-- <router-link> 默认会被渲染成一个 `<a>` 标签 -->
<router-link to="/xxx路由路径">Go to page</router-link>
```

```html
router-view

<!-- 路由出口 -->
<!-- 路由匹配到的组件将渲染在这里 -->
<router-view></router-view>
```

#### 路由实例源信息和监听路由

```javascript
created() {
   console.log(this.$router, "路由实例", this.$route, "路由源信息");
},
watch: {
   $route(to, from) {
      console.log(to, "去到哪里", from, "来自哪里");
   },
},
// beforeRouteUpdate 也可监听
```

#### 路由跳转

```javascript
goPage() {
// 编程式导航 常用的路由实例挂载在原型的方法 push go forward back replace currentRoute beforeEach afterEach addRoutes addRoute
 this.$router.push({
     name: "My",
     // params也可携带参数注意两者区别
     query: {
         desc: "路由携带参数",
     },
 });
},
// 取参 一般name和params搭配 path和query搭配
console.log(this.$route.query, "参数");
console.log(this.$route.params, "参数");
// router.addRoute 添加一条新路由规则。如果该路由规则有 name，并且已经存在一个与之相同的名字，则会覆盖它
// router.addRoutes 已废弃 使用 router.addRoute() 代替 动态添加更多的路由规则。参数必须是一个符合 routes 选项要求的数组。
// router.getRoutes 获取所有活跃的路由记录列表。注意只有文档中记录下来的 property 才被视为公共 API，避免使用任何其它 property
```

#### 路由模式（默认hash #开头的）

**abstract 模式**，支持所有 JavaScript 运行环境，如 Node.js 服务器端。**如果发现没有浏览器的 API，路由会自动强制进入这个模式**

**hash 模式**，使用 URL hash 值来作路由。支持所有浏览器，包括不支持 HTML5 History Api 的浏览器，原理就是使用window.onHashChange来监听hash值的改变，一旦发生变化就找出此hash值所匹配的组件，进而将组件渲染到页面中（带#的url页面丑）

**history模式**，这种模式充分利用 onpopstate `history.pushState` `history.replaceState`（正常的url美观）history路由中我们使用onpopstate事件函数来监听history路由的变化，但是popstate事件函数只能监听到history.go、forward、back的切换路由方式，但是它不能够监听到pushState添加历史记录

该模式需要后端配合配置：官方例子文档：https://v3.router.vuejs.org/zh/guide/essentials/history-mode.html#%E5%90%8E%E7%AB%AF%E9%85%8D%E7%BD%AE%E4%BE%8B%E5%AD%90

**history 模式**，这种模式充分利用 `history.pushState`（并且需要后端支持配置参考文档：https://v3.router.vuejs.org/zh/guide/essentials/history-mode.html#%E5%90%8E%E7%AB%AF%E9%85%8D%E7%BD%AE%E4%BE%8B%E5%AD%90）

设计模式：History API https://developer.mozilla.org/en-US/docs/Web/API/History_API

```javascript
const router = new VueRouter({
  mode: 'history' | 'hash' | 'abstract',
  routes: [...]
})
```

#### 常见路由报错

```javascript
// 降低版本 npm i vue-router@3.0 -S 路由配置文件加入
const routerPush = Router.prototype.push
Router.prototype.push = function push(location) {
  return routerPush.call(this, location).catch((error) => error)
}

// 捕获router.push异常
this.$router.push(route).catch((err) => {
  console.log('输出报错', err)
})

// 补齐router.push()的第三个参数
this.$router.push(
  route,
  () => {},
  (e) => {
    console.log('输出报错', e)
  }
)
```

#### 404路由配置

```javascript
// 路由配置文件 一般是写在最后一个路由配置
import NotFound from "@/page/404";
{
    path: "*",
    name: "404",
    component: NotFound,
 },
```

#### 高级匹配模式

`vue-router` 使用 [path-to-regexp (opens new window)](https://github.com/pillarjs/path-to-regexp/tree/v1.7.0)作为路径匹配引擎，支持很多高级的匹配模式

#### 子路由、嵌套路由和动态路由

`children` 配置就是像 `routes` 配置一样的路由配置数组，可以嵌套多层路由

```javascript
{
	path: '/user/:id',
	component: User,
	children: [
		// 当 /user/:id 匹配成功，
		// UserHome 会被渲染在 User 的 <router-view> 中
		{ path: '', component: UserHome }
		// ...其他子路由
	]
}
```

#### 命名视图

```javascript
// 在界面中拥有多个单独命名的视图，而不是只有一个单独的出口。如果 router-view 没有设置名字，那么默认为 default
<router-view></router-view>
<router-view name="a"></router-view>
<router-view name="b"></router-view>
// 一个视图使用一个组件渲染，因此对于同个路由，多个视图就需要多个组件。确保正确使用 components 配置 (带上 s)

const router = new VueRouter({
  routes: [
    {
      path: '/',
      components: {
        default: Foo,
        a: Bar,
        b: Baz
      }
    }
  ]
})
```

#### 重定向（redirect）和别名（alias）

```javascript
// 直接配置 redirect
{ path: '/a', redirect: '/b' }

// 使用一个方法，动态返回重定向目标
const router = new VueRouter({
  routes: [
    { path: '/a', redirect: to => {
      // 方法接收 目标路由 作为参数
      // return 重定向的 字符串路径/路径对象
    }}
  ]
})
// 别名
{ path: '/a', component: A, alias: '/b' }
```

#### 路由钩子和组件独享钩子

```javascript
// 全局路由守卫
router.beforeEach((to, from, next) => {
  /* 必须调用 `next` */
  // 进度条开始 权限处理
})

router.beforeResolve((to, from, next) => {
  /* 必须调用 `next` */
})

router.afterEach((to, from) => {
    // 进度条停止
})

// 组件独享钩子
beforeRouteEnter(to, from, next) {
    // 在渲染该组件的对应路由被 confirm 前调用
    // 不！能！获取组件实例 `this` 可传一个vm回调
    // 因为当守卫执行前，组件实例还没被创建
    next(vm => {
        // 通过 `vm` 访问组件实例
    })
},
    
beforeRouteUpdate(to, from, next) {
    // 在当前路由改变，但是该组件被复用时调用
    // 举例来说，对于一个带有动态参数的路径 /foo/:id，在 /foo/1 和 /foo/2 之间跳转的时候，
    // 由于会渲染同样的 Foo 组件，因此组件实例会被复用。而这个钩子就会在这个情况下被调用。
    // 可以访问组件实例 `this`
},

beforeRouteLeave(to, from, next) {
    // 导航离开该组件的对应路由时调用
    // 可以访问组件实例 `this`
    // 常用就是页面挽留操作
}
```

#### vue路由懒加载的三种方式（首屏组件一般不懒加载比如Home组件）

**vue异步组件**

**es提案的import()**

**webpack的require,ensure()**

```javascript
// vue异步组件 这种情况下一个组件生成一个js文件
component: resolve => require(['@/components/home'],resolve)
// es提案的import()
// 没有指定webpackChunkName，每个组件打包成一个js文件
// 指定了相同的webpackChunkName，会合并打包成一个js文件
const Home = () => import(/* webpackChunkName: 'test' */ '@/components/home')
const Index = () => import(/* webpackChunkName: 'test' */ '@/components/index')
// webpack的require,ensure()
// 多个路由指定相同的chunkName，会合并打包成一个js文件
component: r => require.ensure([], () => r(require('@/components/home')), 'demo')
component: r => require.ensure([], () => r(require('@/components/index')), 'demo')
```

### Vuex

视项目大小复杂程度是否需要使用，足够简单直接忽略一般都是需要的目前项目比如更方便操作登录态，组件之间共享共同状态数据等

![vuex](https://v3.vuex.vuejs.org/vuex.png)

#### 为什么需要用到vuex

随着项目的越来越复杂庞大组件的通信嵌套越复杂当**多个组件共享状态**时，单向数据流的简洁性很容易被破坏：

- 多个视图依赖于同一状态。
- 来自不同视图的行为需要变更同一状态

#### 文档

vuex3.0文档：https://v3.vuex.vuejs.org/zh/ （一般vue2项目）

vue4.x文档：https://vuex.vuejs.org/zh/ （一般vue3项目）

#### 安装

使用vuex之前请把这个依赖安装好 引入 入口js 比如 main.js

```javascript
// 将下列代码添加到你使用 Vuex 之前的一个地方
npm install es6-promise --save # npm
yarn add es6-promise # Yarn

// main.js
import 'es6-promise/auto'
```

```javascript
npm install vuex --save
yarn add vuex
// 现在一般默认是4.x版本了需要指定版本是 npm install vuex@3 --save

npm install vuex@next --save
yarn add vuex@next --save
```

#### 使用vuex的数据持久化

**在VUE项目中，由于是单页应用，vuex中的数据在页面刷新时就会被清除，所以我们要考虑怎样让vuex中的数据持久保存在浏览器中**

**利用浏览器的本地存储localStorage和sessionStorage**

1. vuex的state在localStorage或sessionStorage取值
2. 在mutations里面，定义的方法对vuex的状态操作的同时，对存储也做对应的操作

**利用vue-presistedstate插件**

1. 安装方式；**npm install vuex-persistedstate --save**
2. 引入：在store下的index.js中引入，然后使用插件plugins

```javascript
// store/index 默认写法会将vuex中的所有数据存储到Local Storage中
import createPersistedStatefrom 'vuex-persistedstate'
export default new Vuex.Store({
    // ...
    plugins: [persistedState()]
})

// 如果想存储到sessionStorage，又或者只想持久化某些数据的话，可以使用以下两个属性：storage和reducer
plugins: [createPersistedState({
    // 默认位置是 localStorage
    storage: window.sessionStorage,
    reducer(state) {
      console.log(state)
      return {
        	// 默认是全部缓存，在这里可以设置需要缓存的状态
	      	token: state.token
      }
    }
})],
state: {
   token: ''
},
mutations: {
    
},
```

**浏览器监听 + 本地存储**

```javascript
// APP.vue  监听页面刷新和关闭
created() {
    // 页面加载时，读取 sessionStorage 中的状态信息
    if (sessionStorage.getItem("store")) {
      this.$store.replaceState(
        Object.assign(
          {},
          this.$store.state,
          JSON.parse(sessionStorage.getItem("store"))
        )
      );
    }
    // 在页面刷新时，将vuex中的信息保存到sessionStorage里
    window.addEventListener("beforeunload", () => {
      sessionStorage.setItem("store", JSON.stringify(this.$store.state));
    });
},
    
// 关闭浏览器窗口的时候清空浏览器缓存在localStorage的数据
mounted(){
   window.onbeforeunload = function (e) {
         var storage = window.localStorage;
         storage.clear()
    }
}
```

#### 基础用法

```javascript
// store/index  并在main.js vue实例中挂载
import Vue from "vue";
import Vuex from "vuex";

import createLogger from "vuex/dist/logger"; // 修改日志

const debug = process.env.NODE_ENV !== "production"; // 开发环境中为true，否则为false

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    sex: "男",
    count: 2,
    title: "vuex学懂了么",
  },
  // 同步操作
  mutations: {
    SET_NUM(state, num) {
      state.count = state.count * num;
    },
    SET_TITLE(state, newTxt) {
      state.title = newTxt;
    },
  },
  getters: {
    sex: (state) => state.sex,
    title: (state) => state.title,
  },
  // 异步操作
  actions: {
    setNum({ commit }, num) {
      setTimeout(() => {
        // 提交 mutation
        commit("SET_NUM", num);
      }, 2000);
    },
    setTitle({ commit }, newTxt) {
      setTimeout(() => {
        commit("SET_TITLE", newTxt);
      }, 2000);
    },
  },
  // 放置模块 登录 用户 购物车啊等
  modules: {},
  plugins: [debug ? createLogger() : [], xxxx别的插件], // 开发环境下显示vuex的状态修改
});

export default store;
```

#### 组件中使用vuex

```vue
<template>
  <div>
    <p>vuex测试</p>
    <p>{{ num }}</p>
    <p>{{ newSex }}</p>
    <p>{{ sex }}</p>
    <p>{{ title }}</p>
    <button @click="add">
      操作后的值(mutations)：{{ $store.state.count }}
    </button>
    <button @click="addSync">
      操作后的值(actions)：{{ $store.state.count }}
    </button>
    <button @click="SET_TITLE('新的标题')">
      点击更改后的文案：{{ newTitle }}
    </button>
  </div>
</template>

<script>
import { mapState, mapGetters, mapMutations, mapActions } from "vuex";
export default {
  name: "vuexTest",
  data() {
    return {};
  },
  computed: {
    num() {
      return this.$store.state.count;
    },
    ...mapState({
      newSex: (state) => state.sex,
      newTitle: (state) => state.title,
      // 箭头函数可使代码更简练
    }),
    ...mapGetters(["sex", "title"]),
  },
  created() {},
  methods: {
    ...mapMutations(["SET_TITLE"]),
    ...mapActions(["setTitle"]),
    add() {
      this.$store.commit("SET_NUM", 2);
    },
    addSync() {
      this.$store.dispatch("setNum", 2);
    },
  },
};
</script>
```

vuex文件的拆分代码组织方式有很多种具体看实际情况和个人习惯定义；一般单独的state处理写成一个modules 公共的【贯穿整个项目使用的】state一个文件管理，vuex同样也作为组件比较方便的组件传值的一种快捷方便的方法推荐使用；

### Vue优化的技巧

1.使用函数式组件

2.懒加载图片/组件/异步组件/CDN

3.子组件拆分

4.v-show 和 v-if

5.keep-alive （缓存 需要的业务场景使用）

6.Deferred 延时分批渲染组件（大量组件渲染）

7.时间片切割技术

8.使用虚拟滚动组件（长列表）

9.非响应式数据的处理，Object.freeze()

10.Service Worker

### Element 使用经验

https://element.eleme.io/#/zh-CN/guide/design （element 对标 vue2）

https://element-plus.gitee.io/zh-CN/guide/design.html  （element-plus 对标 vue3）

```
vue add element （vue2）
```

```
vue add element-plus （vue3）
```

选择按需导入 会自动安装babel-plugin-component 并且生成一个src/plugins 覆盖App.vue组件

#### form 下面只有一个 input 时回车键刷新页面

```javascript
// 原因是触发了表单默认的提交行为，给el-form 加上@submit.native.prevent就行了
<el-form inline @submit.native.prevent>
  <el-form-item label="名称">
    <el-input
      v-model="name"
      :placeholder="输入订名称"
      clearable
      @keyup.enter.native="handleInput"
    />
  </el-form-item>
</el-form>
```

#### 表格固定列最后一行显示不全

```css
// 设置全局 表格固定列最后一行显示不全
.el-table__fixed-right {
  height: 100% !important;
}

// 全局设置 表头与内容错位
.el-table--scrollable-y .el-table__body-wrapper {
 overflow-y: overlay !important;
}

// table组件在ie下宽度不是100%
.el-table__header{
	width: 100% !important;
}
.el-table__body{
	width: 100% !important;
}

// table随着浏览器放缩在googel浏览器内放表头单元格和内容单元格错位问题
body .el-table th.gutter{
	display: table-cell!important;
}
```

#### 输入框用正则限制但绑定值未更新

```javascript
// 方法一
<el-input 
  v-model="form.num" 
  placeholder="请输入" 
  oninput="value=value.replace(/[^\d.]/g,'')" 
/>
      
// 方法一 输入中文后 v-model 会失效，下面的方式更好一点
<el-input 
  v-model="form.num" 
  placeholder="请输入" 
  @keyup.native="form.num=form.num.replace(/[^\d.]/g,'')"
/>
```

#### 去除type="number"输入框聚焦时的上下箭头

```css
/* 设置全局 */
.clear-number-input.el-input::-webkit-outer-spin-button,
.clear-number-input.el-input::-webkit-inner-spin-button {
  margin: 0;
  -webkit-appearance: none !important;
} 
.clear-number-input.el-input input[type="number"]::-webkit-outer-spin-button,
.clear-number-input.el-input input[type="number"]::-webkit-inner-spin-button {
  margin: 0;
  -webkit-appearance: none !important;
}
.clear-number-input.el-input {
  -moz-appearance: textfield;
} 
.clear-number-input.el-input input[type="number"] {
  -moz-appearance: textfield;
}

// 组件使用
<el-input type="number" class="clear-number-input" />
```

#### 只校验表单其中一个字段

```javascript
this.$refs['form'].validateField('mobile', valid => {
  if (valid) {
    // 发送验证码
  }
})
```

#### 弹窗重新打开时表单上次的校验信息未清除

```javascript
<el-dialog @close="onClose">
  <el-form ref="form">
  </el-form>
</el-dialog>

// 弹窗关闭时重置表单
onClose() {
  this.$refs['form'].resetFields()
}
```

#### 根据条件高亮行并去除默认hover颜色

```javascript
<el-table :row-class-name="tableRowHighLigt">
</el-table>

tableRowHighLigt({ row }) {
  return row.status === 2 ? 'highlight' : ''
}

// 设置全局
.el-table .highlight {
  background-color: #f2f2ff;
  &:hover > td {
    background-color: initial !important;
  }
  td {
    background-color: initial !important;
  }
}
```

#### el-popover 位置偏移问题

```javascript
// el-popover 里的内容是动态获取的，所以刚打开时位置正确，此时内容为空，等到获取数据渲染后 el-popover 内容盒子大小发生变化从而造成位置偏移
<el-popover ref="popover" placement="left" trigger="click">
</el-popover>

// 获取数据后 通过源码知道 el-popover 里有个 updatePopper 方法用于更新位置（文档里没有）贼坑
this.$nextTick(() => {
  this.$refs['popover'].updatePopper()
})

// 手动关闭气泡弹框popover的方法
this.$refs.popover.doclose()
```

#### el-dialog 的 destroy-on-close 属性设置无效

```javascript
// destroy-on-close 设置为 true 后发现弹窗关闭后 DOM 元素仍在，没有被销毁
// 解决办法：在 el-dialog 上添加 v-if
<el-dialog :visible.sync="visible" v-if="visible" destroy-on-close>
</el-dialog>
```

#### el-cascader 选择后需要点击空白处才能关闭

```javascript
// 级联选择器在设置为可选任意一级时，选定某个选项时需要手动点击空白处才能关闭
<el-cascader
  ref="cascader"
  @change="onChange"
/>

// 可在 change 事件触发时将其关闭
onChange() {
  this.$refs['cascader'].dropDownVisible = false
}
```

#### 使用图片查看器

el-image 组件是自带图片预览功能的，传入 preview-src-list 即可。但有时候我们不用 image 组件但又想预览大图  例如点击一个按钮时弹出一个图片查看器

使用 el-image-viewer，文档里并没有这个组件，但通过查看源码知道，该组件正是 el-image 里预览图片所用的。

```vue
<template>
  <div>
    <el-button @click="open">打开图片预览</el-button>
    <el-image-viewer
      v-if="show"
      :on-close="onClose"
      :url-list="urls"
      :initial-index="initialIndex"
    />
  </div>
</template>

<script>
import ElImageViewer from 'element-ui/packages/image/src/image-viewer'

export default {
  components: {
    ElImageViewer
  },

  data() {
    return {
      show: false,
      urls: ['https://www.nuxtjs.cn/avEUftE.png'],
      initialIndex: 0
    }
  },

  methods: {
    open() {
      this.show = true
    },

    onClose() {
      this.show = false
    }
  }
}
</script>
```

#### el-table 翻页序号连续

```javascript
// 方法一
<el-table-column
    label="序号"
    type="index"
    width="50"
    align="center">
      <template v-slot="{ $index }">
          <span>{{ $index + pageSize * ( currentPage - 1 ) + 1 }}</span>
      </template>
</el-table-column>

/*  
	$index 当前序号
	pageSize 每页显示的条数
	currentPage 当前页码
*/

// 方法二
<el-table-column
    label="序号"
    type="index"
    width="50"
    :index="tableIndex"
    align="center" />

// 文档中index的类型可以是Function(index)，这里绑定一个方法，将返回值赋给index, 即该行的索引
methods: {
    tableIndex(index) {
        return index + this.pageSize * ( this.currentPage - 1 ) + 1
    }
}
```

#### el-table el-pagination 分页页数从0开始

后端返回的接口数据是从第0页开始的，el-pagination 中当前页数默认是从1开始的，也就是点击el-pagination的第1页传递给后端的页数是0，点击第2页传递1，点击第3页传递2 ... ...

```javascript
<el-pagination
    background
    :current-page="currentPage"
    :page-size="pageSize" />
        
export default{
    data() {
        return {
            currentPage: 1, // 当前页数
            pageSize: 10, // 每页显示条目个数
        }
    },
    // 调用接口的时候把queryParams传递过去就可以了
    computed: {
        queryParams () {
            return {
                page: this.currentPage - 1,
                size: this.pageSize
            }
        }
    }
}
```

#### input实现远程搜索问题

```javascript
// 方案一 使用 el-autocomplete
<el-autocomplete v-model="value" 
	:fetch-suggestions="querySearchAsync" 
	@blur="searchText" 
	@select="handleSelect"></el-autocomplete>

props: {
	data: {
		type: Function,
		default:() => {}
	}
}
methods: {
	async querySearchAsync(queryString, cb) {
		const result = await this.data(queryString)
		cb(result)
	},
	handleSelect(item) {
		this.$emit('change',item)
	},
	searchText(el) {
		this.$emit('inputValue', el.target.value)
	}
}

// 方案二 使用 el-select
<el-select v-model="name" filterable remote reserve-keyword 
            :no-data-text="noDataText" 
            :remote-method="handleSearch" 
            :loading="loading"
	    clear @change="handleChange(product,index)">
	<el-option v-for="(item,index) in searchList" 
            :key="index" :label="item.name" :value="item.id"/>
</el-select>
data() {
    return {
		noDataText:'无匹配信息',
		loading:false,
		searchList: [],
		name: ''
    };
  },
  methods: {
	  handleChange(val) {
		this.name = val  
	  },
	  handleSearch(query){
		// 请求接口部分
		  this.loading=true;
		  setTimeout(() => {
			  this.loading = false;
			  this.searchList = res.data; // res为接口返回的数据
		  },200)
	  },
}
```

