# vue3基础及最佳实践

### 脚手架官方（vue-cli）

文档：https://cli.vuejs.org/zh/guide/installation.html

默认版本5了

#### 安装

```javascript
npm install -g @vue/cli
# OR
yarn global add @vue/cli
```

Vue CLI 4.x 需要 [Node.js](https://nodejs.org/) v8.9 或更高版本 (推荐 v10 以上)。你可以使用 [n](https://github.com/tj/n)，[nvm](https://github.com/creationix/nvm) 或 [nvm-windows](https://github.com/coreybutler/nvm-windows) 在同一台电脑中管理多个 Node 版本

```javascript
// 查看安装版本
vue --version
```

#### 脚手架升级

```javascript
npm update -g @vue/cli
# 或者
yarn global upgrade --latest @vue/cli
```

#### 旧版本脚手架卸载

```javascript
npm uninstall vue-cli -g
# 或者
yarn global remove vue-cli
```

#### 创建项目

```javascript
// projectName 项目名
vue create 【projectName】
```

举例搭建vue3 + ts + element plus的项目

1、Manually select features 【自定义】

2、把需要用到的比如Ts选项、路由、vuex、css预处理勾上

3、选择版本3.x

4、是否需要class形式创建组件 【N】

5、沿TypeScript使用Babel 【TS基础配置】【Y】

6、路由模式、预处理、后面选项一路回车（或者喜好来）

7、安装 element-plus：   npm install element-plus --save

element-plus 组件文档：https://element-plus.gitee.io/zh-CN/guide/installation.html#%E7%89%88%E6%9C%AC

#### element-plus UI完整引入（只和打包体积有关）

```javascript
// main.ts
import { createApp } from 'vue'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import App from './App.vue'

const app = createApp(App)

app.use(ElementPlus)
app.mount('#app')
```

#### element-plus UI按需引入（只和打包体积有关）

```javascript
npm install -D unplugin-vue-components unplugin-auto-import
```

```javascript
// webpack.config.js or vue.config.js
// 更改配置文件后重新跑项目即可
const { defineConfig } = require("@vue/cli-service");
// element-plus UI按需引入
const AutoImport = require("unplugin-auto-import/webpack");
const Components = require("unplugin-vue-components/webpack");
const { ElementPlusResolver } = require("unplugin-vue-components/resolvers");
module.exports = defineConfig({
  transpileDependencies: true,
  // webpack配置
  // 如果这个值是一个对象，则会通过 webpack-merge 合并到最终的配置中。
  // 如果这个值是一个函数，则会接收被解析的配置作为参数。该函数既可以修改配置并不返回任何东西，也可以返回一个被克隆或合并过的配置版本
  configureWebpack: {
    plugins: [
      AutoImport({
        resolvers: [ElementPlusResolver()],
      }),
      Components({
        resolvers: [ElementPlusResolver()],
      }),
    ],
  },
});

// 配置好对应的 main.ts 即可无需引用
// import ElementPlus from "element-plus";
// import "element-plus/dist/index.css";
// app.use(ElementPlus);
```

#### 快速生成vue2、vue3代码的 VSCode插件 vb快捷键

Vue VSCode Snippets

#### [eslint-plugin-vue](https://eslint.vuejs.org/)规则

https://eslint.vuejs.org/rules/multi-word-component-names.html

创建组件的是时候name需要配置下

```javascript
// 关闭组件命名规则 组件的name属性
// .eslintrc.js
"vue/multi-word-component-names": "off",
```

登录规则：密码的加密md5

![image-20220703204543780](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20220703204543780.png)