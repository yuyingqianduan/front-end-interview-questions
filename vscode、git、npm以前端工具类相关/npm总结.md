# npm总结

### 什么是npm

npm 由三个独立的部分组成：

- 网站
- 注册表（registry）
- 命令行工具 (CLI)

[*网站*](https://npmjs.com/) 是开发者查找包（package）、设置参数以及管理 npm 使用体验的主要途径。

***注册表*** 是一个巨大的数据库，保存了每个包（package）的信息。

[*CLI*](https://docs.npmjs.com/cli/npm) 通过命令行或终端运行。开发者通过 CLI 与 npm 打交道。

### 安装npm

从 Node.js 网站安装 npm 

node官网：https://nodejs.org/en/

查看ndoe安装版本 **node -v**

查看npm版本 **npm -v**

更新到最新版本【同步版本】 **npm install npm@latest -g**

安装最新的官方测试版npm **npm install npm@next -g**

更新 npm

### 在线npm包查询

文档：https://www.npmjs.com/package/npm

### 中文文档

中文文档：https://www.npmjs.cn/

### 镜像管理工具

nrm

```javascript
npm i nrm -g // 安装
```

nrm有自带默认配置，*为当前的配置    nrm ls  

npm -------- https://registry.npmjs.org/

 yarn ------- https://registry.yarnpkg.com/

 cnpm ------- http://r.cnpmjs.org/

 *taobao ----- https://registry.npm.taobao.org/

 nj --------- https://registry.nodejitsu.com/

 npmMirror -- https://skimdb.npmjs.com/registry/

切换当前源地址  nrm use taobao

删除源地址 nrm del taobao

### 直接安装cnpm 

npm install cnpm -g --registry=[https://registry.npm.taobao.org](https://links.jianshu.com/go?to=https%3A%2F%2Fregistry.npm.taobao.org%2F)

直接更改源地址     npm set registry https://registry.npm.taobao.org/

查看npm源地址     npm config list

删除源地址             npm config rm registry

### 安装方式区别

-g 表示全局安装，没有-g表示本地安装
i 是install 的简写（使用空格可多个安装）
**npm i module_name -S** 等价于 **npm install module_name --save**
**npm i module_name -D** 等价于 **npm install module_name --save-dev**
–save表示我们安装模块的时候，同时把它写到package.json 文件中。- - 这时打开package.json 文件，会看到多一个dependencies字段，里面包含安装的模块信息。
–save-dev 又是什么，它表示安装依模块的时候，把它写到package.json中，不过它写入的不是dependencies, 而是devDependencies中。
**devDependencies 和dependencies 有什么区别？**
dependencies: 是项目运行时的依赖，就是程序上线后仍然需要依赖。
devDependencies, 开发依赖，就是我们在开发过程中需要的依赖。比如babel, 它只负责转换es6+ 到es5， 转换完成后，我们只要转换后的代码，上线的时候，直接把转换后的代码部署上线，不需要bebal. 这就是开发依赖，只在开发时候起作用， 上线不需要。其实就是我们在使用webpack开发时，它配置文件里所有的依赖，都是开发依赖。

```javascript
// 一次性安装多个
npm install 模块1 模块2 模块3
// 安装开发时依赖包
npm install 模块名 --save-dev 对应package.json中的 devDependencies 
// 安装运行时依赖包
npm install 模块名 –save  ---> 对应package.json中的 dependencies
// 安装该模块到指定版本
npm install 模块名 @版本号
// 如果安装到最新版本可以使用以下命令
npm install 模块名@latest
// 卸载模块 删除 node_modules 目录下面的包（package）
npm uninstall 模块名
// 如需从 package.json 文件中删除依赖，需要在命令后添加参数 --save
npm uninstall --save 模块名
// 卸载全局安装的包
npm uninstall -g 模块名

// 查看该模块的所有发布版本
npm view 模块名versions
// 查看该模块的最新版本
npm view 模块名version
```

**创建 `package.json` 文件**

```javascript
npm init // 【手动填写一些配置信息，一路回车也可】
npm init -y // [生成的默认的package.json，即省去回车]
```

### 如何设置 .npmrc

项目下 .npmrc 文件的优先级最高，可以给每个项目配置不同的镜像，项目之间的配置互不影响。

在项目的根目录下新建 .npmrc 文件，在里面以 key=value 的格式进行配置。

```javascript
registry=https://registry.npm.taobao.org
@xxx:registry=https://npm.xx.com
// 以@xxx 开头的包从 registry=https://npm.xx.com 这里下载，其余全去淘宝镜像下载
// yarn 会读取.npmrc的配置文件，所以不必为yarn再设置一次
```

### 查看包信息

```javascript
npm info 包名
```

### 设置npm源

```javascript
npm config set registry= 镜像地址(https//registry.npm.taobao.org)
```

### 获取npm源

```javascript
npm config get registry
```

### 删除一些配置

```javascript
npm config delete registry
```

### 删除全局包的缓存

```javascript
npm cache clear --force -g // 删除全局包的缓存（慎重）
```

### 删除 node_modules

```javascript
npm install rimraf -g //  安装 rimraf
// 使用命令删除 
rimraf node_modules // 删除 node_modules 也可以删除其它文件夹或文件
```

其他 包管理工具 yarn、 pnpm 简单区别

### package.json主要属性

必须属性

package.json 中最重要的两个字段就是 name【项目的名称】 和 version【该项目包的版本号】**「主版本号. 次版本号. 修订号」**，它们都是必须的，如果没有，就无法正常执行 npm install 命令。npm 规定 package.json 文件是由名称和版本号作为唯一标识符的

如果某个版本的改动较大，并且不稳定，可能如法满足预期的兼容性需求，就需要发布先行版本，先行版本通过会加在版本号的后面，通过 “-” 号连接以点分隔的标识符和版本编译信息：内部版本（alpha）、公测版本（beta）和候选版本（rc，即 release candiate）

```json
{
    “name”: “debugger”,
    “version”: “1.0.0”,
    “description”: “”,
    “main”: “index.js”,
    “scripts”: {
    “test”: “echo “Error: no test specified” && exit 1”,
    “start”: “node test.js”// 自己加入的配置部分
    },
    “keywords”: [],
    “author”: “”,
    “license”: “ISC”
}
```

#### engines

engines 字段指明了该模块运行的平台，比如 Node 的某个版本或者浏览器

```json
{
  "engines": {
    "node": ">=12.18.3",
    "npm": ">7.0.0"
  }
}
```

#### private

```json
{
  "private": true
}
```

#### publishConfig

此字段包含各种设置，仅当从本地来源生成包时才考虑这些设置（通过 yarn pack 或像 yarn npm publish 这样的发布命令之一）。

publishConfig.access定义将程序包发布到 npm 注册表时要使用的程序包访问级别。 有效值是公开的并且是受限制的，但是受限制的通常需要注册付费计划（这取决于你使用的注册表）。

publishConfig.bin如果存在，则在打包打包以将其运送到远程注册表之前，清单中的顶级 bin 字段将被设置为此新值。 这不会修改真正的清单，只会修改存储在 tarball 中的清单。

publishConfig.browser与publishConfig.bin属性的原理相同； 生成工作空间 tarball 时，将使用此值代替顶级浏览器字段。

publishConfig.executableFiles默认情况下，出于可移植性的原因，在 bin 字段中列出的文件之外的文件都不会在结果包归档文件中标记为可执行文件。 executeFiles 字段使你可以声明必须设置了可执行标志（+ x）的其他字段，即使不能通过 bin 字段直接访问它们也是如此。

publishConfig.main与publishConfig.bin属性相同的原理； 生成工作空间 tarball 时，将使用此值代替顶级“ main”字段。

publishConfig.module与publishConfig.bin属性相同的原理； 生成工作空间 tarball 时，将使用此值代替顶级“ module”字段。

publishConfig.registry如果存在，当将包推送到远程位置时，将替换配置中定义的任何注册表

```json
{
  "publishConfig": {
    "access": "public",
    "bin": "./build/bin.js",
    "browser": "./build/browser.js",
    "executableFiles": ["./dist/shim.js"],
    "main": "./build/index.js",
    "module": "./build/index.mjs",
    "registry": "https://npm.pkg.github.com"
  }
}
```

#### unpkg

unpkg 是一个内容源自 npm 的全球快速 CDN

配置unpkg 字段后,发布到 npmjs.com 中的包会自动同步到 unpkg.com 上，一般为 umd 格式

```json
{
  "unpkg": "dist/antd.min.js"
}
```

#### browserslist

浏览器兼容版本

```json
{
  "browserslist": [
    "> 0.5%",
    "last 2 versions",
    "Firefox ESR",
    "not dead",
    "IE 11",
    "not IE 10"
  ]
}
```

#### homepage 

主页信息，一个表示项目首页的url，它是一个字符串。

#### bugs

bugs 一个表示接受问题反馈的url地址，也可以是email地址。一般是Github项目下的issues

```json
bugs:{url: https://github.com/vue.js/vue/issues}
```

#### repository

指明你的项目源代码仓库所在位置，这有助于其他人为你的项目贡献代码，对于组件库很有用，这个配置项 会直接在组件库的npm首页生效

```json
respository:{
  type:'git',
  url:'git+https://github.com/xxx.git'
}
```